/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package owl2_lib;

/**
 *
 */
public class owl_lib {

  private owl_lib(){
  }
  
  static public float gps2float(long c){
    boolean neg = (c&0x01)>0;
    c >>= 1;
    float d = c%10000;
    d /= 10000;
    c /= 10000;
    int g = (int)(c/60);
    c -= g*60;
    return (neg ? -1 : 1)*(g+(c+d)/60);
  }

  static public int bits2v_check(final int v){
    return v<<8;
  }

  static public float bits2vbat(final int v){
    return ((float)(v+273))/100;
  }

  static public int bits2temp(final int v){
    return (v&0xFF)-55;
  }

  static public float bits2k(final int v){
    int dig = (v>>28)&0x0F;
    if(dig==0){
      return v/1e12f;
    }
    float k = v&0x0FFFFFFF;
    k /= 1000000; // 6 digitos sao automaticos
    while(dig>0){
      k /= 10;
      dig--;
    }
    return k;
  }

  static public int k2bits(float k){
    int v;
    int dig = 0;
    if(k>0&&k<1){
      do{
        k *= 10;
        dig++;
      }while(k<1);
      k *= 1000000; // 6 digitos sao automaticos
      v = ((int)k)&0x0FFFFFFF;
    }else{
      v = 0;
    }
    return (dig<<28)|v;
  }

  static public float bits2fp(final int v){
    return (float)v/32768;
  }

  static public float bits2i_min(final int v){
    return (float)v/100.0f;
  }

  static public String getIdsOfPath(long p){
    String o = "[";
    long mask = 1;
    int id = 1;
    boolean any = false;
    while(mask>0){
      if((p&mask)>0){
        if(any){
          o += ',';
        }
        any = true;
        o = o+id;
      }
      mask <<= 1;
      id++;
    }
    o += ']';
    return o;

  }

  static public int sensor_getData(int offset, int datasize, final int pos, final byte[] ba){
    int r = 0;
    int end = 32-datasize;
    int bitstart = offset+datasize*pos;
    int target = (bitstart/8);
    bitstart %= 8;
    int mask = 0x80>>>bitstart;
    int b = ba[target++]&0xFF;
    while(datasize-->0){
      r = r>>>1;
      if((b&mask)>0){
        r |= 0x80000000;
      }else{
        r &= ~0x80000000;
      }
      mask >>>= 1;
      if(mask==0&&datasize>0){
        mask = 0x80;
        b = ba[target++]&0xFF;
      }
    }
    r >>>= end;
    return r;
  }

}
