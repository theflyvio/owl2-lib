/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.persistent;

/**
 *
 * @author Usuario
 */
public interface IPersistentHandler {

  int get_timezone();

  void write_cache(final IPersistentRecord e);

}
