/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.persistent;

import java.io.IOException;
import java.sql.SQLException;
import owl2_lib.db.TDB;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 * @author Usuario
 */
public interface IPersistentRecord {

  byte getKind();

  boolean checkStatement();

  void configure(final TDB db) throws SQLException;

  boolean execute() throws SQLException;

  void save(final TXStreamWriter sw) throws IOException;

  String getDescription();

}
