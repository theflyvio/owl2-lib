/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib;

import java.io.IOException;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;
import x.etc.lib;

/**
 *
 * @author TheFlyvio
 */
final public class TCoordinates {

  final public float latitude;
  final public float longitude;

  public TCoordinates(final float latitude_, final float longitude_){
    latitude = latitude_;
    longitude = longitude_;
  }

  public boolean equals(final TCoordinates coo){
    return coo!=null&&coo.latitude==latitude&&coo.longitude==longitude;
  }

  public String asText(){
    return Float.toString(latitude)+","+Float.toString(longitude);
  }

  public double distanceTo(final TCoordinates coo){
    return lib.gpsDistance(latitude, longitude, coo.latitude, coo.longitude);
  }

  public void save(final TXStreamWriter sw) throws IOException{
    sw.writeByte((byte)1);
    sw.writeFloat(latitude);
    sw.writeFloat(longitude);
  }

  public static TCoordinates load(final TXStreamReader sr) throws IOException{
    final int v = sr.readByte();
    final float latitude = sr.readFloat();
    final float longitude = sr.readFloat();
    return new TCoordinates(latitude, longitude);
  }

}
