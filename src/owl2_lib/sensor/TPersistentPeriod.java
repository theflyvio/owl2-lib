/*
 *
 *
 *
 */
package owl2_lib.sensor;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import owl2_lib.db.TDB;
import owl2_lib.db.TDBSQL;
import owl2_lib.persistent.IPersistentRecord;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 * @author TheFlyvio
 *
 * v_invalida nao tem o dado after, somento o dado da tensao after que acabou de cair enquanto tinha corrente
 *
 */
public class TPersistentPeriod implements IPersistentRecord {

  //static private TDBSQL sqlDelete;
  static private TDBSQL sqlSelect;
  static private TDBSQL sqlInsert;

  final public int timezone_min;
  final public long sensor_sn;
  final public ZonedDateTime server_utc;
  final public ZonedDateTime server_local;
  final public ZonedDateTime period_start_utc;
  final public ZonedDateTime period_start_local;
  final public int period_size;
  final public int period;
  final public float vbat;
  final public float i_min;
  final public float i_med;
  final public float i_max;
  final public int i_dig_min;
  final public int i_dig_med;
  final public int i_dig_max;
  final public float v_min;
  final public float v_med;
  final public float v_max;
  final public int v_dig_min;
  final public int v_dig_med;
  final public int v_dig_max;
  final public float fp_min;
  final public float fp_med;
  final public float fp_max;
  final public int t_min;
  final public int t_med;
  final public int t_max;
  final public int v_dig;
  final public boolean pre_presence;
  final public boolean pos_presence;
  final public int vbat_charging;
  final public float energia_total;
  final public float energia_ativa;
  final public float energia_reativa;

  public TPersistentPeriod(final long sensor_sn_,
                           final int timezone_min_,
                           final ZonedDateTime server_utc_,
                           final ZonedDateTime period_start_utc_,
                           final int period_size_,
                           final int period_,
                           final float vbat_,
                           final float i_min_,
                           final float i_med_,
                           final float i_max_,
                           final int i_dig_min_,
                           final int i_dig_med_,
                           final int i_dig_max_,
                           final float v_min_,
                           final float v_med_,
                           final float v_max_,
                           final int v_dig_min_,
                           final int v_dig_med_,
                           final int v_dig_max_,
                           final float fp_min_,
                           final float fp_med_,
                           final float fp_max_,
                           final int t_min_,
                           final int t_med_,
                           final int t_max_,
                           final int v_dig_,
                           final boolean pre_presence_,
                           final boolean pos_presence_,
                           final int vbat_charging_,
                           final float energia_total_,
                           final float energia_ativa_,
                           final float energia_reativa_){
    sensor_sn = sensor_sn_;
    timezone_min = timezone_min_;
    server_utc = server_utc_;
    server_local = server_utc.plusMinutes(timezone_min_);
    period_start_utc = period_start_utc_;
    period_start_local = period_start_utc.plusMinutes(timezone_min_);
    period_size = period_size_;
    period = period_;
    vbat = vbat_;
    i_min = i_min_;
    i_med = i_med_;
    i_max = i_max_;
    i_dig_min = i_dig_min_;
    i_dig_med = i_dig_med_;
    i_dig_max = i_dig_max_;
    v_min = v_min_;
    v_med = v_med_;
    v_dig_max = v_dig_max_;
    v_dig_min = v_dig_min_;
    v_dig_med = v_dig_med_;
    v_max = v_max_;
    fp_min = fp_min_;
    fp_med = fp_med_;
    fp_max = fp_max_;
    t_min = t_min_;
    t_med = t_med_;
    t_max = t_max_;
    v_dig = v_dig_;
    pre_presence = pre_presence_;
    pos_presence = pos_presence_;
    vbat_charging = vbat_charging_;
    energia_total = energia_total_;
    energia_ativa = energia_ativa_;
    energia_reativa = energia_reativa_;
  }

  public TPersistentPeriod(final long sensor_sn_,
                           final int timezone_min_,
                           final TPeriod p){
    this(sensor_sn_,
         timezone_min_,
         ZonedDateTime.now(ZoneOffset.UTC),
         ZonedDateTime.of(p.utc.zdt.getYear(), p.utc.zdt.getMonthValue(), p.utc.zdt.getDayOfMonth(), (p.period*p.period_size)/60, (p.period*p.period_size)%60, 0, 0, ZoneOffset.UTC),
         p.period_size,
         p.period,
         p.vbat,
         p.i.min.get_value(),
         p.i.med.get_value(),
         p.i.max.get_value(),
         (int)p.i.min.get_digital(),
         (int)p.i.med.get_digital(),
         (int)p.i.max.get_digital(),
         p.v.min.get_value(),
         p.v.med.get_value(),
         p.v.max.get_value(),
         (int)p.v.min.get_digital(),
         (int)p.v.med.get_digital(),
         (int)p.v.max.get_digital(),
         (int)p.fp.min.get_value(),
         (int)p.fp.med.get_value(),
         (int)p.fp.max.get_value(),
         (int)p.t.min.get_value(),
         (int)p.t.med.get_value(),
         (int)p.t.max.get_value(),
         p.cag_v_digital,
         p.pre_presence,
         p.pos_presence,
         p.vbat_charging,
         p.energia_total,
         p.energia_ativa,
         p.energia_reativa);
  }

  @Override
  public String getDescription(){
    final StringBuilder sb = new StringBuilder();
    sb.append("sn=");
    sb.append(sensor_sn);
    sb.append(" periodo=");
    sb.append(period);
    sb.append("/");
    sb.append(period_size);
    sb.append(" vbat=");
    sb.append(vbat);
    sb.append(" i=");
    sb.append(i_min);
    sb.append(";");
    sb.append(i_med);
    sb.append(";");
    sb.append(i_max);
    sb.append(" i_d=");
    sb.append(i_dig_min);
    sb.append(";");
    sb.append(i_dig_med);
    sb.append(";");
    sb.append(i_dig_max);
    sb.append(" v=");
    sb.append(v_min);
    sb.append(";");
    sb.append(v_med);
    sb.append(";");
    sb.append(v_max);
    sb.append(" v_d=");
    sb.append(v_dig_min);
    sb.append(";");
    sb.append(v_dig_med);
    sb.append(";");
    sb.append(v_dig_max);
    sb.append(" fp=");
    sb.append(fp_min);
    sb.append(";");
    sb.append(fp_med);
    sb.append(";");
    sb.append(fp_max);
    sb.append(" t=");
    sb.append(t_min);
    sb.append(";");
    sb.append(t_med);
    sb.append(";");
    sb.append(t_max);
    sb.append(" ");
    sb.append(period_start_utc);
    return sb.toString();
  }

  @Override
  public byte getKind(){
    return (byte)'p';
  }

  @Override
  public void configure(final TDB db) throws SQLException{
    //if(sqlDelete==null){
    //  sqlDelete = new TDBSQL(db);
    //}
    if(sqlSelect==null){
      sqlSelect = new TDBSQL(db);
    }
    //
    if(sqlInsert==null){
      sqlInsert = new TDBSQL(db);
    }
    sqlSelect.addConnectionPartner(sqlInsert);
    sqlSelect.setSQL("select id from gw_periods where sensor_sn=? and period_size=? and period=? and period_start_utc=?");
    sqlInsert.setSQL("insert into gw_periods (sensor_sn,server_utc,server_local,period_start_utc,period_start_local,period_size,period,vbat,i_min,i_med,i_max,v_min,v_med,v_max,fp_min,fp_med,fp_max,t_min,t_med,t_max,i_dig_min,i_dig_med,i_dig_max,v_dig,pre_presence,pos_presence,vbat_charging,v_value_min,v_value_med,v_value_max,energia_total,energia_ativa,energia_reativa,processed) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'N')");
  }

  @Override
  public boolean checkStatement(){
    return sqlSelect.checkStatement(); // sqlDelete.checkStatement(); //&&sqlInsert.checkStatement();
  }

  @Override
  public boolean execute() throws SQLException{
//    if(sensor_sn<203||sensor_sn>205){
//      return true;
//    }
    if(sqlSelect.startTransaction()){
      sqlSelect.setLong(1, sensor_sn);
      sqlSelect.setInt(2, period_size);
      sqlSelect.setInt(3, period);
      sqlSelect.setTimestamp(4, period_start_utc.toInstant());
      final ResultSet rs = sqlSelect.query();
      if(rs!=null){
        final boolean hasdata = rs.next();
        rs.close();
        if(hasdata){
          // já existe, ignora
          sqlSelect.rollback();
          return true;
        }else{
          sqlInsert.setLong(1, sensor_sn);
          sqlInsert.setTimestamp(2, server_utc.toInstant());
          sqlInsert.setTimestamp(3, server_local.toInstant());
          sqlInsert.setTimestamp(4, period_start_utc.toInstant());
          sqlInsert.setTimestamp(5, period_start_local.toInstant());
          sqlInsert.setInt(6, period_size);
          sqlInsert.setInt(7, period);
          sqlInsert.setFloat(8, vbat);
          sqlInsert.setFloat(9, i_min);
          sqlInsert.setFloat(10, i_med);
          sqlInsert.setFloat(11, i_max);
          sqlInsert.setInt(12, v_dig_min);
          sqlInsert.setInt(13, v_dig_med);
          sqlInsert.setInt(14, v_dig_max);
          sqlInsert.setFloat(15, fp_min);
          sqlInsert.setFloat(16, fp_med);
          sqlInsert.setFloat(17, fp_max);
          sqlInsert.setInt(18, t_min);
          sqlInsert.setInt(19, t_med);
          sqlInsert.setInt(20, t_max);
          sqlInsert.setInt(21, i_dig_min);
          sqlInsert.setInt(22, i_dig_med);
          sqlInsert.setInt(23, i_dig_max);
          sqlInsert.setInt(24, v_dig);
          sqlInsert.setChar(25, pre_presence ? 'Y' : 'N');
          sqlInsert.setChar(26, pos_presence ? 'Y' : 'N');
          sqlInsert.setInt(27, vbat_charging);
          sqlInsert.setFloat(28, v_min);
          sqlInsert.setFloat(29, v_med);
          sqlInsert.setFloat(30, v_max);
          sqlInsert.setFloat(31, energia_total);
          sqlInsert.setFloat(32, energia_ativa);
          sqlInsert.setFloat(33, energia_reativa);
          if(sqlInsert.execute()){
            return sqlSelect.commit();
          }
        }
      }
      sqlSelect.rollback();
    }
    return false;
  }

  @Override
  public void save(final TXStreamWriter sw) throws IOException{
    sw.writeInt(1);
    //
    sw.writeInt(timezone_min);
    sw.writeLong(sensor_sn);
    sw.writeZonedDateTime(server_utc);
    sw.writeZonedDateTime(period_start_utc);
    sw.writeInt(period_size);
    sw.writeInt(period);
    sw.writeFloat(vbat);
    sw.writeFloat(i_min);
    sw.writeFloat(i_med);
    sw.writeFloat(i_max);
    sw.writeInt(i_dig_min);
    sw.writeInt(i_dig_med);
    sw.writeInt(i_dig_max);
    sw.writeFloat(v_min);
    sw.writeFloat(v_med);
    sw.writeFloat(v_max);
    sw.writeInt(v_dig_min);
    sw.writeInt(v_dig_med);
    sw.writeInt(v_dig_max);
    sw.writeFloat(fp_min);
    sw.writeFloat(fp_med);
    sw.writeFloat(fp_max);
    sw.writeInt(t_min);
    sw.writeInt(t_med);
    sw.writeInt(t_max);
    sw.writeInt(v_dig);
    sw.writeBoolean(pre_presence);
    sw.writeBoolean(pos_presence);
    sw.writeInt(vbat_charging);
    sw.writeFloat(energia_total);
    sw.writeFloat(energia_ativa);
    sw.writeFloat(energia_reativa);
  }

  static public TPersistentPeriod load(final TXStreamReader sr) throws IOException{
    int v = sr.readInt();
    //
    final int timezone_min = sr.readInt();
    final long sensor_sn = sr.readLong();
    final ZonedDateTime server_utc = sr.readZonedDateTime();
    final ZonedDateTime period_start_utc = sr.readZonedDateTime();
    final int period_size = sr.readInt();
    final int period = sr.readInt();
    final float vbat = sr.readFloat();
    final float i_min = sr.readFloat();
    final float i_med = sr.readFloat();
    final float i_max = sr.readFloat();
    final int i_dig_min = sr.readInt();
    final int i_dig_med = sr.readInt();
    final int i_dig_max = sr.readInt();
    final int v_min = sr.readInt();
    final int v_med = sr.readInt();
    final int v_max = sr.readInt();
    final int v_dig_min = sr.readInt();
    final int v_dig_med = sr.readInt();
    final int v_dig_max = sr.readInt();
    final float fp_min = sr.readFloat();
    final float fp_med = sr.readFloat();
    final float fp_max = sr.readFloat();
    final int t_min = sr.readInt();
    final int t_med = sr.readInt();
    final int t_max = sr.readInt();
    final int v_dig = sr.readInt();
    final boolean pre_presence = sr.readBoolean();
    final boolean pos_presence = sr.readBoolean();
    final int vbat_charging = sr.readInt();
    final float energia_total = sr.readFloat();
    final float energia_ativa = sr.readFloat();
    final float energia_reativa = sr.readFloat();
    return new TPersistentPeriod(sensor_sn,
                                 timezone_min,
                                 server_utc,
                                 period_start_utc,
                                 period_size,
                                 period,
                                 vbat,
                                 i_min,
                                 i_med,
                                 i_max,
                                 i_dig_min,
                                 i_dig_med,
                                 i_dig_max,
                                 v_min,
                                 v_med,
                                 v_max,
                                 v_dig_min,
                                 v_dig_med,
                                 v_dig_max,
                                 fp_min,
                                 fp_med,
                                 fp_max,
                                 t_min,
                                 t_med,
                                 t_max,
                                 v_dig,
                                 pre_presence,
                                 pos_presence,
                                 vbat_charging,
                                 energia_total,
                                 energia_ativa,
                                 energia_reativa);
  }

}
