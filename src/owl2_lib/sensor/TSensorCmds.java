/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import java.io.IOException;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
final public class TSensorCmds {

  private int mux_tryout = 0; // quantas tentativas de envio desse comando
  private int req_config_tryout = 0; // 1=requisita configuracoes
  private int clear_flash_tryout = 0; // 1=requisita configuracoes
  //
  private int mux;
  private int pot;
  private int mux_pot_samples;

  public TSensorCmds(){
    clear(); // na criacao nao há save, fica com os valores default para forçar 1 envio
  }

  final public void clear(){
    mux_tryout = 0;
    req_config_tryout = 0;
    clear_flash_tryout = 0;
  }

  public boolean has_mux_pot(){
    return mux_tryout>0;
  }

  public void mux_pot_sent(){
    if(mux_tryout>0){
      mux_tryout--;
    }
  }

  public void mux_pot_cancel(){
    mux_tryout = 0;
  }

  public void set_mux_pot(final int vmux, final int vpot, final int vsamples){
    mux = vmux;
    pot = vpot;
    mux_pot_samples = vsamples;
    mux_tryout = 3;
  }

  public int get_mux(){
    return mux;
  }

  public int get_pot(){
    return pot;
  }

  public int get_mux_pot_samples(){
    return mux_pot_samples;
  }

  public boolean has_req_config(){
    return req_config_tryout>0;
  }

  public void req_config_sent(){
    if(req_config_tryout>0){
      req_config_tryout--;
    }
  }

  public void set_req_config(){
    req_config_tryout = 1;
  }

  public boolean has_clear_flash(){
    return clear_flash_tryout>0;
  }

  public void clear_flash_sent(){
    if(clear_flash_tryout>0){
      clear_flash_tryout--;
    }
  }

  public void set_clear_flash(){
    clear_flash_tryout = 1;
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    //
    s.writeInt(mux);
    s.writeInt(pot);
    s.writeInt(mux_pot_samples);
    s.writeInt(mux_tryout);
    s.writeInt(req_config_tryout);
    s.writeInt(clear_flash_tryout);
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt(); // v
    // 1
    mux = s.readInt();
    pot = s.readInt();
    mux_pot_samples = s.readInt();
    mux_tryout = s.readInt();
    req_config_tryout = s.readInt();
    clear_flash_tryout = s.readInt();
  }

}
