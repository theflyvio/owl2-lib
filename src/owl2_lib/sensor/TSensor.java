/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import ab.etc.data.TUTC;
import java.io.IOException;
import owl2_lib.TCoordinates;
import owl2_lib.cmd.TCMD_r_a;
import owl2_lib.cmd.TCMD_r_p;
import owl2_lib.cmd.TCMD_r_s;
import owl2_lib.measure.IMeasureK;
import owl2_lib.measure.TMeasure;
import owl2_lib.persistent.IPersistentHandler;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
final public class TSensor {

  final private ILog log;
  final private IPersistentHandler persistent_handler;
  final public IMeasureK mk_i;
  final public IMeasureK mk_v;
  final public IMeasureK mk_fp;
  final public IMeasureK mk_t;
  //
  final public int sn;
  public ESensorType type;
  public float vbat; // ultima bateria recebida, pode ser de configuracao, de periodo, de leitura instantanea, etc
  public TUTC vbat_utc;
  public int fw_version;
  public TCoordinates coo;
  //
  final public TSensorInstall install;
  final public TSensorConfig config;
  final public TSensorReset reset;
  //
  final public TState i_state;
  final public TState v_state;
  final public TState dir_state;
  final public TState i_alarm;
  final public TAfundamento afundamento;
  //
  final public TPeriod period; // último período recebido
  public TUTC utc_instant;
  final public TMeasure i_instant;
  final public TMeasure v_instant;
  final public TMeasure fp_instant;
  final public TMeasure t_instant;
  //
  final public TSensorConfig target_config;
  final public TSensorCmds cmds;
  //
  public long last_req_config;

  public TSensor(final int sn_, final ESensorType type_, final IPersistentHandler persistent_handler_, final ILog log_){
    sn = sn_;
    type = type_;
    persistent_handler = persistent_handler_;
    log = log_;
    install = new TSensorInstall();
    config = new TSensorConfig();
    target_config = new TSensorConfig();
    reset = new TSensorReset();
    //
    mk_i = () -> install.k_i;
    mk_v = () -> install.k_v;
    mk_fp = () -> 1f/32768f;
    mk_t = () -> 1;
    //
    vbat_utc = new TUTC(0);
    coo = new TCoordinates(0, 0);
    i_state = new TState();
    v_state = new TState();
    dir_state = new TState();
    i_alarm = new TState();
    afundamento = new TAfundamento();
    //
    period = new TPeriod(this);
    i_instant = new TMeasure(mk_i);
    v_instant = new TMeasure(mk_v);
    fp_instant = new TMeasure(mk_fp);
    t_instant = new TMeasure(mk_t);
    //
    cmds = new TSensorCmds();
    //
  }

  public boolean is_configured(){
    return config.configured&&config.equals(target_config);
  }

  public void set_vbat(final float b, final TUTC utc){
    if(utc.compare_to(vbat_utc)>0){
      vbat = b;
      vbat_utc = utc;
    }
  }

  public void execute_cmd(final TCMD_r_a r){
    final String m = r.get_print();
    if(config.configured&&r.rtc.valid){
      //
      set_vbat(r.vbat, r.rtc.utc);
      // verifica os eventos ocorridos
      final boolean valid = r.presence_v||!r.presence_i; // tem tensao ou nao tem corrente
      // analise da corrente
      if(i_state.get_state()!=r.presence_i){
        persistent_handler.write_cache(new TPersistentEvent(sn,
                                                            persistent_handler.get_timezone(),
                                                            r.presence_i ? ESensorEvent.I_PRESENCA : ESensorEvent.I_FALTA,
                                                            i_state.get_state() ? 1 : 0,
                                                            i_state.get_utc(),
                                                            r.presence_i ? 1 : 0,
                                                            r.rtc.utc,
                                                            valid,
                                                            0, 0, 0, 0));
      }
      // analise da tensao
      if(v_state.get_state()!=r.presence_v){
        persistent_handler.write_cache(new TPersistentEvent(sn,
                                                            persistent_handler.get_timezone(),
                                                            r.presence_v ? ESensorEvent.V_PRESENCA : ESensorEvent.V_FALTA,
                                                            v_state.get_state() ? 1 : 0,
                                                            v_state.get_utc(),
                                                            r.presence_v ? 1 : 0,
                                                            r.rtc.utc,
                                                            valid,
                                                            0, 0, 0, 0));
      }
      // analise do afundamento
      if(r.afund>0&&r.afund<100
         &&!(r.rtc.hours==0&&r.rtc.minutes==0) // ignora 00:00
         &&(!afundamento.get_utc().equals(r.rtc.utc)||afundamento.get_magnitude()!=r.afund)){ // utc ou magnitude diferente
        // novo afundamento. tem tensao, é afundamento senão é curto permanente
        persistent_handler.write_cache(new TPersistentEvent(sn,
                                                            persistent_handler.get_timezone(),
                                                            r.presence_v ? ESensorEvent.AFUNDAMENTO : ESensorEvent.CURTO,
                                                            afundamento.get_magnitude(),
                                                            afundamento.get_utc(),
                                                            r.afund,
                                                            r.rtc.utc,
                                                            valid,
                                                            afundamento.get_ref1(),
                                                            afundamento.get_taxa1(),
                                                            afundamento.get_ref2(),
                                                            afundamento.get_taxa2()));
      }
      // analise do alarme de corrente
      if(i_alarm.get_state()!=r.i_alarm){
        // mudança no alarme
        persistent_handler.write_cache(new TPersistentEvent(sn,
                                                            persistent_handler.get_timezone(),
                                                            r.i_alarm ? ESensorEvent.I_ALARME_ON : ESensorEvent.I_ALARME_OFF,
                                                            i_alarm.get_state() ? 1 : 0,
                                                            i_alarm.get_utc(),
                                                            r.i_alarm ? 1 : 0,
                                                            r.rtc.utc,
                                                            valid,
                                                            0, 0, 0, 0));
      }
      // analise do alarme de direcao
      boolean after_dir = r.direction_i==0b00||r.direction_i==0b11;
      if(dir_state.get_state()!=after_dir){
        // mudança na direcao
        persistent_handler.write_cache(new TPersistentEvent(sn,
                                                            persistent_handler.get_timezone(),
                                                            after_dir ? ESensorEvent.I_DIRECAO_POS : ESensorEvent.I_DIRECAO_NEG,
                                                            dir_state.get_state() ? 1 : 0,
                                                            dir_state.get_utc(),
                                                            after_dir ? 1 : 0,
                                                            r.rtc.utc,
                                                            valid,
                                                            0, 0, 0, 0));
      }
      // atualiza valores
      i_alarm.set_state(r.i_alarm, r.rtc.utc);
      i_state.set_state(r.presence_i, r.rtc.utc);
      v_state.set_state(r.presence_v, r.rtc.utc);
      dir_state.set_state(r.direction_i==0b00||r.direction_i==0b11, r.rtc.utc);
      afundamento.set_afundamento(r.afund, r.i_inc_tx1, r.i_inc_tx2, r.i_inc_ref1, r.i_inc_ref2, r.rtc.utc);
      //
      log.print(this, m);
    }else{
      log.print(this, "warning (ignorado) "+m);
    }
  }

  public void execute_cmd(final TCMD_r_p r){
    final String m = r.get_print();
    if(config.configured){
      //
      period.read(r);
      //
      set_vbat(period.vbat, period.utc);
      //
      persistent_handler.write_cache(new TPersistentPeriod(sn, persistent_handler.get_timezone(), period));
      print(period.get_info());
    }else{
      print("warning (ignorado) "+m);
    }
  }

  public void execute_cmd(final TCMD_r_s s){
    fw_version = s.fw_version;
    vbat = s.vbat;
    v_state.set_state(s.presence_v, s.rtc.utc);
    i_state.set_state(s.presence_i, s.rtc.utc);
    dir_state.set_state(s.direction_i==0b00||s.direction_i==0b11, s.rtc.utc); // 00 ou 11 = atual(0b01)==instalacao(0b10)
    i_alarm.set_state(s.alarm, s.rtc.utc);
    //afundamento.set_afundamento(s.afund, s., sn, sn, sn, utc_);
    //
    config.read(s);
    install.read(s);
    reset.read(s);
    //
    i_instant.set_value(s.irms);
    v_instant.set_value(s.vrms);
    fp_instant.set_value(s.fp);
    t_instant.set_value(s.temp);
    //
    install.mux = s.cag_mux;
    install.pot = s.cag_pot;
    if(s.coo!=null){
      coo = s.coo;
    }
    //
    print(s.get_print());
  }

  public void print(final String msg){
    log.print(this, msg);
  }

  public String get_browser_text(){
    char v_p = v_state.get_state() ? '_' : i_state.get_state() ? '!' : '*';
    char i_p = i_state.get_state() ? i_alarm.get_state() ? '!' : '_' : '*';
    return String.format("%c%c%c%c sn:%07d %4.2f",
                         type==ESensorType.GPRS_MAIN ? 'h' : 'p',
                         v_p,
                         i_p,
                         dir_state.get_state() ? '+' : '-',
                         sn,
                         vbat);

  }

  public String get_info(){
    return "sn......: "+sn+" / "+type
           +"\ncoo.....: "+coo.asText()+" vbat="+vbat+" "+vbat_utc.date_time(true)
           +"\nfwver...: "+fw_version+" "+reset.get_info()
           +"\ninstall.:"+install.get_info()
           +"\npres v..: "+v_state.get_info()+" v_check="+config.v_check+" cag="+config.set_cag
           +"\npres i..: "+i_state.get_info()+" i_min="+config.i_min+" ade_offset="+config.ade_offset
           +"\ndir  i..: "+dir_state.get_info()+" loops  ="+config.direction_i_loops
           +"\nalarm i.: "+i_alarm.get_info()+" i_mult ="+config.i_mult
           +"\nafundam.: "+afundamento.get_info()+" curto_c="+config.curto_c
           +"\n"
           +"\ninstantâneos "+utc_instant.date_time(true)
           +"\nv.......: "+v_instant.get_info(true)
           +"\ni.......: "+i_instant.get_info(true)
           +"\nfp......: "+fp_instant.get_info(true)
           +"\nt.......: "+(int)t_instant.get_value()
           +"\n"
           +period.get_info()
           +"\n";
  }

  public void save(final TXStreamWriter s) throws Exception{
    s.writeInt(1);
    //
    s.writeInt(type.getId());
    s.writeFloat(vbat);
    s.writeLong(vbat_utc.toMs());
    s.writeInt(fw_version);
    coo.save(s);
    install.save(s);
    config.save(s);
    reset.save(s);
    i_state.save(s);
    v_state.save(s);
    dir_state.save(s);
    i_alarm.save(s);
    afundamento.save(s);
    period.save(s);
    s.writeLong(utc_instant.toMs());
    i_instant.save(s);
    v_instant.save(s);
    fp_instant.save(s);
    t_instant.save(s);
    target_config.save(s);
    cmds.save(s);
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt();
    //
    type = ESensorType.getById(s.readInt());
    vbat = s.readFloat();
    vbat_utc = new TUTC(s.readLong());
    fw_version = s.readInt();
    coo = TCoordinates.load(s);
    install.load(s);
    config.load(s);
    reset.load(s);
    i_state.load(s);
    v_state.load(s);
    dir_state.load(s);
    i_alarm.load(s);
    afundamento.load(s);
    period.load(s);
    utc_instant = new TUTC(s.readLong());
    i_instant.load(s);
    v_instant.load(s);
    fp_instant.load(s);
    t_instant.load(s);
    target_config.load(s);
    cmds.load(s);
  }

}
