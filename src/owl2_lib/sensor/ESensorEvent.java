/*
 *
 *
 *
 */
package owl2_lib.sensor;

/**
 *
 * @author user
 */
public enum ESensorEvent {
  NONE(0),
  V_FALTA(1),
  V_PRESENCA(2),
  V_INVALIDA(3),
  I_FALTA(4),
  I_PRESENCA(5),
  AFUNDAMENTO(6),
  CURTO(7),
  I_ALARME_ON(8),
  I_ALARME_OFF(9),
  V_VALIDA(10),
  I_DIRECAO_POS(11),
  I_DIRECAO_NEG(12),
  ;

  final public int id;

  private ESensorEvent(final int id_){
    id = id_;
  }

  static public ESensorEvent getById(final int id){
    for(final ESensorEvent v : values()){
      if(v.id==id){
        return v;
      }
    }
    return NONE;
  }

}
