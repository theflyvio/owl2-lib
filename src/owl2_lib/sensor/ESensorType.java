/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

/**
 *
 * @author TheFlyvio
 *
 * tipo de sensor
 *
 */
public enum ESensorType {
  NONE(1), //
  GPRS_MAIN(2),
  GPRS_SECONDARY(3);

  final private int id;

  private ESensorType(final int id_){
    id = id_;
  }

  public int getId(){
    return id;
  }

  static public ESensorType getById(final int id){
    for(final ESensorType c : values()){
      if(c.id==id){
        return c;
      }
    }
    return null;
  }

}
