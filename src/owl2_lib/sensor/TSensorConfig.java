/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import java.io.IOException;
import owl2_lib.cmd.TCMD_r_s;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
final public class TSensorConfig {

  final static public int v_check_default = 700000;
  final static public int curto_c_default = 80;
  final static public int i_mult_default = 10;
  final static public int set_cag_default = 1; // 3 = garante que nao bate e tenta uma vez (com 1)
  final static public int i_min_default = 50;
  final static public int direction_i_loops_default = 0;
  final static public int connect_spread_default = 1440;
  final static public int period_size_default = 1;
  final static public int ade_offset_default = 0;

  public boolean configured; // enquanto a primeira configuracao nao chegar, ignora dados

  public int set_cag;
  public int curto_c;
  public int direction_i_loops;
  public int period_size;
  public int connect_spread;
  public int v_check;
  public int i_mult;
  public int i_min;
  public int ade_offset;

  public TSensorConfig(){
    defaults();
  }

  final public void defaults(){
    set_cag = set_cag_default;
    curto_c = curto_c_default;
    direction_i_loops = direction_i_loops_default;
    period_size = period_size_default;
    connect_spread = connect_spread_default;
    v_check = v_check_default;
    i_mult = i_mult_default;
    i_min = i_min_default;
    ade_offset = ade_offset_default;
  }

  public void set(final TSensorConfig c){
    set_cag = c.set_cag;
    curto_c = c.curto_c;
    direction_i_loops = c.direction_i_loops;
    period_size = c.period_size;
    connect_spread = c.connect_spread;
    v_check = c.v_check;
    i_mult = c.i_mult;
    i_min = c.i_min;
    ade_offset = c.ade_offset;
  }

  public boolean equals(final TSensorConfig i){
    return v_check==i.v_check
           &&curto_c==i.curto_c
           &&i_mult==i.i_mult
           &&(set_cag==1&&i.set_cag==1)
           &&i_min==i.i_min
           &&ade_offset==i.ade_offset
           &&period_size==i.period_size
           &&direction_i_loops==i.direction_i_loops
           &&connect_spread==i.connect_spread;
  }

  public void read(final TCMD_r_s s){
    direction_i_loops = s.direction_iLoops;
    v_check = s.v_check;
    curto_c = s.curto_c;
    i_mult = s.i_mult;
    set_cag = s.cag_set;
    i_min = (int)(s.i_min*100);
    ade_offset = s.ade_offset;
    period_size = s.period_size;
    configured = true;
  }

  public String get_print(){
    final StringBuilder sb = new StringBuilder();
    sb.append("cag=").append(set_cag)
            .append(" curto_c=").append(curto_c)
            .append(" direction_i_loops=").append(direction_i_loops)
            .append(" period_size=").append(period_size)
            .append(" connect_spread=").append(connect_spread)
            .append(" v_check=").append(v_check)
            .append(" i_mult=").append(i_mult)
            .append(" i_min=").append(i_min)
            .append(" ade_offset=").append(ade_offset);
    return sb.toString();
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    //
    s.writeBoolean(configured);
    s.writeInt(set_cag);
    s.writeInt(curto_c);
    s.writeInt(direction_i_loops);
    s.writeInt(period_size);
    s.writeInt(connect_spread);
    s.writeInt(v_check);
    s.writeInt(i_mult);
    s.writeInt(i_min);
    s.writeInt(ade_offset);

  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt(); // v
    // 1
    configured = s.readBoolean();
    set_cag = s.readInt();
    curto_c = s.readInt();
    direction_i_loops = s.readInt();
    period_size = s.readInt();
    connect_spread = s.readInt();
    v_check = s.readInt();
    i_mult = s.readInt();
    i_min = s.readInt();
    ade_offset = s.readInt();
  }

}
