/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import java.io.IOException;
import owl2_lib.cmd.TCMD_r_s;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
final public class TSensorInstall {

  final static private String[] fase_list = {"?", "A", "B", "C"};

  public float k_i;
  public float k_v;
  //
  public int v; // tensao de instalacao
  public int v_dig; // leitura digital da tensao na instalacao
  //
  public int mux;
  public int pot;
  //
  // somente no gateway
  public int fase;
  public String poste;

  public String get_fase_info(){
    if(fase<0||fase>=fase_list.length){
      fase = 0;
    }
    return fase_list[fase];
  }

  public String get_info(){
    return "v="+v+" ("+v_dig+") mux="+mux+" pot="+pot+" k_v:"+k_v+" k_i:"+k_i+" fase:"+get_fase_info()+" poste:"+poste;
  }

  public void read(final TCMD_r_s s){
    k_i = s.k_i;
    k_v = s.k_v;
    v = s.install_v;
    v_dig = s.install_v_dig;
    mux = 0xFF;
    pot = 0xFF;
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    //
    s.writeFloat(k_i);
    s.writeFloat(k_v);
    s.writeInt(v);
    s.writeInt(v_dig);
    s.writeInt(mux);
    s.writeInt(pot);
    s.writeInt(fase);
    s.writeShortString(poste);
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt(); // v
    // 1
    k_i = s.readFloat();
    k_v = s.readFloat();
    v = s.readInt();
    v_dig = s.readInt();
    mux = s.readInt();
    pot = s.readInt();
    fase = s.readInt();
    poste = s.readShortString();
  }

}
