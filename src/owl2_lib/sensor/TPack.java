/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import owl2_lib.cmd.ECMD;
import owl2_lib.cmd.ICMD_Handler;
import owl2_lib.cmd.TCMD_RX;
import owl2_lib.cmd.TCMD_ack;
import owl2_lib.cmd.TCMD_c;
import owl2_lib.cmd.TCMD_j;
import owl2_lib.cmd.TCMD_l;
import owl2_lib.cmd.TCMD_r_a;
import owl2_lib.cmd.TCMD_r_p;
import owl2_lib.cmd.TCMD_r_s;
import owl2_lib.cmd.TCMD_u_k;
import owl2_lib.cmd.TCMD_u;
import x.etc.io.streams.TXStreamReader;
import x.etc.lib;

/**
 *
 * @author Usuario
 */
public class TPack {

  public int len;
  public int sn;
  public int section;
  public ECMD cmd;
  public int n;
  public int checksum_h;
  public int checksum_d;
  //
  public byte[] buffer;
  //
  /*
   * for_server
   *
   * header RX len=12
   * 0.... uchar start; /// 1 'g'
   * 1...2 uint len; ////// 2 len do conteudo APOS cmd
   * 3...6 ulong sn; ////// 4 sn do hub
   * 7...8 uint section; // 2 secao
   * 9.... uchar cmd; ///// 1 protocolo
   * 10.11 uint n; //////// 2 numero p/ ack
   * 12.13 uint checksum_h
   * 14.15 uint checksum_d
   *
   *
   * !for_server
   *
   * 0.... uchar start; // 1 'g'
   * 1...2 uint len; ///// 2 do conteudo APOS cmd somente header = 0
   * 3.... uchar cmd; //// 1 protocolo
   * 4...5 uint n; /////// 2 numero p/ ack
   * 6...7 checksum header
   * 8...9 checksum data
   *
   */
  final public boolean for_server;

  public TPack(final boolean for_server_){
    for_server = for_server_;
  }

  public void read_header(final byte[] header){
    if(for_server){
      len = lib.arrayToShort(header, 1, true);
      sn = lib.arrayToInt(header, 3, true);
      section = lib.arrayToShort(header, 7, true)&0xFFFF;
      cmd = ECMD.getById(header[9]);
      n = lib.arrayToShort(header, 10, true)&0xFFFF;
      checksum_h = lib.arrayToShort(header, 12, true)&0xFFFF;
      checksum_d = lib.arrayToShort(header, 14, true)&0xFFFF;
    }else{
      len = lib.arrayToShort(header, 1, true);
      cmd = ECMD.getById(header[3]);
      n = lib.arrayToShort(header, 4, true)&0xFFFF;
      checksum_h = lib.arrayToShort(header, 6, true)&0xFFFF;
      checksum_d = lib.arrayToShort(header, 8, true)&0xFFFF;
    }
    if(cmd==null){
      cmd = ECMD.UNKNOWN;
    }
    buffer = new byte[len];
  }

  public void read_body_and_execute(final ICMD_Handler handler) throws Exception{
    if(cmd!=ECMD.U_PING){
      handler.print("vai executar "+cmd);
    }
    switch(cmd){
      case G_ACK:
        read_ack(handler);
        break;

      case G_LOGIN:
        read_l(handler);
        break;

      case G_RECORD:
        read_r(handler);
        break;

      case G_PEER_DONE:
        read_peer_done(handler);
        break;

      case G_UPDATE:
        read_u(handler);
        break;

      case U_LOGIN:
        read_j(handler);
        break;

      case U_COMMAND:
        read_c(handler);
        break;

      case U_PING:
        break;

      default:
        handler.pack_invalid(this);
        break;
    }
  }

  private void read_ack(final ICMD_Handler handler) throws Exception{
    final TXStreamReader s = TXStreamReader.create(true, buffer);
    final TCMD_ack ack = new TCMD_ack();
    ack.read(s);
    //
    handler.execute_pack_ack(this, ack);
  }

  private void read_l(final ICMD_Handler handler) throws Exception{
    final TXStreamReader s = TXStreamReader.create(true, buffer);
    final TCMD_l l = new TCMD_l();
    l.read(s);
    //
    handler.execute_pack_l(this, l);
  }

  private void read_peer_done(final ICMD_Handler handler){
    handler.execute_pack_peer_done();
  }

  private void read_r(final ICMD_Handler handler){
    final TXStreamReader s = TXStreamReader.create(true, buffer);
    final TCMD_RX r = new TCMD_RX();
    handler.send_ack(n);
    try{
      boolean read_header = true;
      if(s.size()>10){ // na primeira vez tem que ter pelo menos o header
        do{
          if(read_header){
            r.read(s);
            read_header = handler.is_gprs_main(r.sn);
          }else{
            r.rec = (char)(s.readByte()&0xFF);
          }
          switch(r.rec){
            case 'a':
              final TCMD_r_a r_a = new TCMD_r_a(r);
              r_a.read(s);
              try{
                handler.execute_pack_r_a(this, r_a);
              }catch(final Exception ex){
                lib.unhandledException(ex);
                handler.print("exceção TGPRS_Pack.pack_r_a "+ex.getMessage());
              }
              break;

            case 'p':
              final TCMD_r_p r_p = new TCMD_r_p(r);
              r_p.read(s);
              try{
                handler.execute_pack_r_p(this, r_p);
              }catch(final Exception ex){
                lib.unhandledException(ex);
                handler.print("exceção TGPRS_Pack.pack_r_p "+ex.getMessage());
              }
              break;

            case 's':
              final TCMD_r_s r_s = new TCMD_r_s(r);
              r_s.read(s);
              try{
                handler.execute_pack_r_s(this, r_s);
              }catch(final Exception ex){
                lib.unhandledException(ex);
                handler.print("exceção TGPRS_Pack.pack_r_s "+ex.getMessage());
              }
              break;
          }
        }while(s.size()>(read_header ? 10 : 0));
      }
    }catch(final Exception ex){
      lib.unhandledException(ex);
      handler.print("exceção TGPRS_Pack "+ex.getMessage());
    }
    //
  }

  private void read_u(final ICMD_Handler handler){
    final TXStreamReader s = TXStreamReader.create(true, buffer);
    //send_ack(hub);
    try{
      final TCMD_u u = new TCMD_u(s);
      switch(u.sub){
        case 'k':
          final TCMD_u_k u_k = new TCMD_u_k(u);
          u_k.read(s);
          handler.execute_pack_u_k(this, u_k);
          break;

      }
    }catch(final Exception ex){
      lib.unhandledException(ex);
    }
  }

  private void read_j(final ICMD_Handler handler) throws Exception{
    final TXStreamReader s = TXStreamReader.create(true, buffer);
    final TCMD_j j = new TCMD_j();
    j.read(s);
    //
    handler.execute_pack_j(this, j);
  }

  private void read_c(final ICMD_Handler handler) throws Exception{
    final TXStreamReader s = TXStreamReader.create(true, buffer);
    final TCMD_c c = new TCMD_c();
    c.read(s);
    //
    handler.execute_pack_c(this, c);
  }

}
