/*
 *
 *
 *
 */
package owl2_lib.sensor;

import ab.etc.data.TUTC;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import owl2_lib.db.TDB;
import owl2_lib.db.TDBSQL;
import owl2_lib.persistent.IPersistentRecord;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;
import x.etc.lib;

/**
 *
 * @author TheFlyvio
 *
 * v_invalida nao tem o dado after, somento o dado da tensao after que acabou de cair enquanto tinha corrente
 *
 */
public class TPersistentEvent implements IPersistentRecord {

  static private TDBSQL sql;

  final public long sensor_sn;
  final public int timezone_min; // minutos de diferença dessa timezone
  final public ESensorEvent kind;
  final public int before_value;
  final public TUTC before_utc;
  final public int after_value;
  final public TUTC after_utc;
  final public boolean valid;
  //
  final public int after_ref1;
  final public int after_taxa1;
  final public int after_ref2;
  final public int after_taxa2;

  public TPersistentEvent(final long sensor_sn_,
                          final int timezone_min_,
                          final ESensorEvent kind_,
                          final int before_value_,
                          final TUTC before_utc_,
                          final int after_value_,
                          final TUTC after_utc_,
                          final boolean valid_,
                          final int after_ref1_,
                          final int after_taxa1_,
                          final int after_ref2_,
                          final int after_taxa2_){
    sensor_sn = sensor_sn_;
    timezone_min = timezone_min_;
    kind = kind_;
    before_value = before_value_;
    before_utc = before_utc_;
    after_value = after_value_;
    after_utc = after_utc_;
    valid = valid_;
    //
    after_ref1 = after_ref1_;
    after_taxa1 = after_taxa1_;
    after_ref2 = after_ref2_;
    after_taxa2 = after_taxa2_;
  }

  @Override
  public byte getKind(){
    return (byte)'e';
  }

  @Override
  public void configure(final TDB db) throws SQLException{
    if(sql==null){
      sql = new TDBSQL(db);
      sql.setSQL(
              "insert into gw_events "
              +"(sensor_sn,server_utc,server_local,event,before_utc,before_local,before_int,after_utc,after_local,after_int,valid,after_ref1,after_taxa1,after_ref2,after_taxa2,processed) values "
              +"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'F')");
    }
  }

  @Override
  public boolean checkStatement(){
    return sql.checkStatement();
  }

  @Override
  public boolean execute() throws SQLException{
    final Instant now = Instant.now(); // utc
    sql.setLong(1, sensor_sn); // 1 sensor_sn
    sql.setTimestamp(2, now); // 2 server_utc
    sql.setTimestamp(3, now.plus(timezone_min, ChronoUnit.MINUTES)); // 3 server_local OwlGatewayControl.config.getTimeZone_Minutes()
    sql.setString(4, kind.name()); // 4 event
    //
    final Instant before = before_utc.toInstant(); // createInstant(before_value);
    sql.setTimestamp(5, before); // 5 before_utc
    sql.setTimestamp(6, before.plus(timezone_min, ChronoUnit.MINUTES)); // 6 before_local
    sql.setInt(7, before_value);// 7 before_int
    //
    final Instant after = after_utc.toInstant();
    sql.setTimestamp(8, after); // 8 after_utc
    sql.setTimestamp(9, after.plus(timezone_min, ChronoUnit.MINUTES)); // 9 after_local
    sql.setInt(10, after_value); // 10 after_int
    //
    sql.setChar(11, valid ? 'T' : 'F');
    sql.setInt(12, after_ref1);
    sql.setInt(13, after_taxa1);
    sql.setInt(14, after_ref2);
    sql.setInt(15, after_taxa2);
    return sql.execute();
  }

  private String get_print(final TUTC utc, final int v){
    return lib.strzero(utc.zdt.getHour(), 2)+":"+lib.strzero(utc.zdt.getMinute(), 2)+"="+v;
  }

  private String get_print_before(){
    return get_print(before_utc, before_value);
  }

  private String get_print_after(){
    return get_print(before_utc, before_value);
  }

  @Override
  public String getDescription(){
    final String v = valid ? "" : " INVALID";
    switch(kind){
      case AFUNDAMENTO:
        return "AFUNDAMENTO.. "+get_print_before()+" => "+get_print_after()+" tx="+after_taxa1+":"+after_ref1+" / "+after_taxa2+":"+after_ref2;

      case CURTO:
        return "CURTO........ "+get_print_before()+" => "+get_print_after();

      case I_ALARME_OFF:
        return "I_ALARME OFF. "+get_print_before()+" => "+get_print_after();

      case I_ALARME_ON:
        return "I_ALARME ON.. "+get_print_before()+" => "+get_print_after();

      case I_DIRECAO_NEG:
        return "I_DIRECAO_NEG "+get_print_after();

      case I_DIRECAO_POS:
        return "I_DIRECAO_POS "+get_print_after();

      case I_FALTA:
        return "I_FALTA...... "+get_print_before()+" => "+get_print_after();

      case I_PRESENCA:
        return "I_PRESENCA... "+get_print_before()+" => "+get_print_after();

      case V_FALTA:
        return "V_FALTA...... "+get_print_before()+" => "+get_print_after();

      case V_INVALIDA:
        return "V_INVALIDA... "+get_print_after();

      case V_PRESENCA:
        return "V_PRESENCA... "+get_print_before()+" => "+get_print_after();

      case V_VALIDA:
        return "V_VALIDA..... "+get_print_after();

      default:
        return "UNKNOWN EVENT ";

    }
  }

  @Override
  public void save(final TXStreamWriter sw) throws IOException{
    sw.writeInt(1);
    //
    sw.writeLong(sensor_sn);
    sw.writeShort((short)kind.id);
    sw.writeBoolean(valid);
    sw.writeInt(before_value);
    sw.writeLong(before_utc.toMs());
    sw.writeInt(after_value);
    sw.writeLong(after_utc.toMs());
    sw.writeInt(timezone_min);
    sw.writeInt(after_ref1);
    sw.writeInt(after_taxa1);
    sw.writeInt(after_ref2);
    sw.writeInt(after_taxa2);
  }

  static public TPersistentEvent load(final TXStreamReader sr) throws IOException{
    int v = sr.readInt();
    //
    final long sensor_sn = sr.readLong();
    final ESensorEvent kind = ESensorEvent.getById(sr.readShort());
    final boolean valid = sr.readBoolean();
    final int before_value = sr.readInt();
    final TUTC before_utc = new TUTC(sr.readLong());
    final int after_value = sr.readInt();
    final TUTC after_utc = new TUTC(sr.readLong());
    final int timezone_min = sr.readInt();
    final int after_ref1 = sr.readInt();
    final int after_taxa1 = sr.readInt();
    final int after_ref2 = sr.readInt();
    final int after_taxa2 = sr.readInt();
    return new TPersistentEvent(sensor_sn, timezone_min, kind, before_value, before_utc, after_value, after_utc, valid, after_ref1, after_taxa1, after_ref2, after_taxa2);
  }

}
