/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import ab.etc.data.TUTC;
import java.io.IOException;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
public class TAfundamento {

  private int magnitude;
  private int taxa1;
  private int taxa2;
  private int ref1;
  private int ref2;
  private TUTC utc;

  public TAfundamento(){
    utc = new TUTC(0);
  }

  public void set_afundamento(final int magnitude_, final int taxa1_, final int taxa2_, final int ref1_, final int ref2_, final TUTC utc_){
    magnitude = magnitude_;
    taxa1 = taxa1_;
    taxa2 = taxa2_;
    ref1 = ref1_;
    ref2 = ref2_;
    utc = utc_;
  }

  public int get_magnitude(){
    return magnitude;
  }

  public int get_taxa1(){
    return taxa1;
  }

  public int get_taxa2(){
    return taxa2;
  }

  public int get_ref1(){
    return ref1;
  }

  public int get_ref2(){
    return ref2;
  }

  public TUTC get_utc(){
    return utc;
  }

  public String get_info(){
    return "mag:"+magnitude+" tx:"+taxa1+","+taxa2+" rf:"+ref1+","+ref2+" "+utc.date_time(true);
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    //
    s.writeInt(magnitude);
    s.writeInt(taxa1);
    s.writeInt(taxa2);
    s.writeInt(ref1);
    s.writeInt(ref2);
    s.writeLong(utc.toMs());
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt();
    //
    magnitude = s.readInt();
    taxa1 = s.readInt();
    taxa2 = s.readInt();
    ref1 = s.readInt();
    ref2 = s.readInt();
    utc = new TUTC(s.readLong());
  }

}
