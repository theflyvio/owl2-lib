/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import ab.etc.data.TUTC;
import java.io.IOException;
import owl2_lib.cmd.TCMD_r_s;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
public class TSensorReset {

  public int pcon0;
  public int module;
  public int event_n;
  public int step;
  public int interrupt;
  //
  public TUTC utc;
  public int count;
  public int motive;

  public TSensorReset(){
  }

  public void read(final TCMD_r_s s){
    pcon0 = s.reset_PCON0;
    module = s.reset_module;
    event_n = s.reset_event_n;
    step = s.reset_step;
    interrupt = s.reset_interrupt;
    count = s.reset_count;
    motive = s.reset_motive;
    utc = s.rtc.utc;
  }

  private String get_pcon0_info(final int v){
    String r = Integer.toBinaryString(v);
    if((v&0x80)>0){
      r += " STKOV";
    }
    if((v&0x40)>0){
      r += " STKUN";
    }
    if((v&0x20)==0){
      r += " WWDT";
    }
    if((v&0x10)==0){
      r += " WDT";
    }
    if((v&0x08)==0){
      r += " MCLR";
    }
    if((v&0x04)==0){
      r += " RES";
    }
    if((v&0x02)==0){
      r += " POR";
    }
    if((v&0x01)==0){
      r += " BOR";
    }
    return r+" ";
  }

  private String get_reset_motive_info(final int m){
    switch(m){
      case 1:
        return "UNKNOWN";

      case 2:
        return "NOT_ASKED";

      case 3:
        return "GPRS";

      case 4:
        return "UTC<00:04";

      case 5:
        return "UPDATE";

      default:
        return "WRONG_VALUE";

    }
  }

  public String get_info(){
    return " reset ("+count+") "+utc.date_time(true)+" motive: "+get_reset_motive_info(motive)+" PCON0:"+get_pcon0_info(pcon0)+" m:"+module+" e:"+event_n+" s:"+step+" i:"+interrupt;
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    //
    s.writeInt(pcon0);
    s.writeInt(module);
    s.writeInt(event_n);
    s.writeInt(step);
    s.writeInt(interrupt);
    s.writeLong(utc.toMs());
    s.writeInt(count);
    s.writeInt(motive);
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt(); // v
    // 1
    pcon0 = s.readInt();
    module = s.readInt();
    event_n = s.readInt();
    step = s.readInt();
    interrupt = s.readInt();
    utc = new TUTC(s.readLong());
    count = s.readInt();
    motive = s.readInt();
  }

}
