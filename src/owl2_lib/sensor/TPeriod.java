/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import ab.etc.data.TUTC;
import java.io.IOException;
import owl2_lib.cmd.TCMD_r_p;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;
import owl2_lib.measure.TMeasureMMM;

/**
 *
 */
final public class TPeriod {

  final public TSensor sensor;
  //
  public TUTC utc;
  public float vbat;
  public int vbat_charging;
  //
  public int period_size;
  public int period;
  //
  public boolean pre_presence;
  public boolean pos_presence;
  //
  final public TMeasureMMM i; // corrente
  final public TMeasureMMM v; // tensão
  final public TMeasureMMM fp; // fator de potencia
  final public TMeasureMMM t; // temperatura
  //
  public int cag_v_digital;
  //
  public float energia_total;
  public float energia_ativa;
  public float energia_reativa;

  public TPeriod(final TSensor sensor_){
    sensor = sensor_;
    i = new TMeasureMMM(sensor.mk_i);
    v = new TMeasureMMM(sensor.mk_i);
    fp = new TMeasureMMM(sensor.mk_i);
    t = new TMeasureMMM(sensor.mk_i);
  }

  final public void read(final TCMD_r_p r){
    final int tmins = r.period*r.period_size;
    utc = new TUTC(r.year, r.month, r.day, tmins/60, tmins%60, 00);
    vbat = r.vbat;
    vbat_charging = r.vbat_charging;
    //
    period_size = r.period_size;
    period = r.period;
    //
    pre_presence = r.pre_presence;
    pos_presence = r.pos_presence;
    //
    if(r.final_values){
      i.min.set_value(r.i_min);
      i.med.set_value(r.i_med);
      i.max.set_value(r.i_max);
      //
      v.min.set_value(r.v_min);
      v.med.set_value(r.v_med);
      v.max.set_value(r.v_max);
    }else{
      i.min.set_digital(r.i_min);
      i.med.set_digital(r.i_med);
      i.max.set_digital(r.i_max);
      //
      v.min.set_digital(r.v_min);
      v.med.set_digital(r.v_med);
      v.max.set_digital(r.v_max);
    }
    //
    fp.min.set_value(r.fp_min);
    fp.med.set_value(r.fp_med);
    fp.max.set_value(r.fp_max);
    //

    t.min.set_value(r.t_min);
    t.med.set_value(r.t_med);
    t.max.set_value(r.t_max);
    //
    cag_v_digital = r.v_dig;
    //
    energia_total = r.energia_total;
    energia_ativa = r.energia_ativa;
    energia_reativa = r.energia_reativa;
  }

  public String get_info(){
    return "\nperíodo "+period+"("+period_size+") pp="+(pre_presence ? 'Y' : 'N')+(pos_presence ? 'Y' : 'N')+" vbat="+vbat+"("+vbat_charging+") "+utc.date_time(true)
           +"\ni.......: "+i.get_info(false)
           +"\nv.......: "+v.get_info(false)+" ref="+cag_v_digital
           +"\nfp......: "+fp.get_info(false)
           +"\nt.......: "+t.get_info(false)
           +"\nenergia.: tot="+String.format("%10.2f", energia_total)+" ati="+String.format("%10.2f", energia_ativa)+" rea="+String.format("%10.2f", energia_reativa);
  }

  final public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    //
    s.writeLong(utc.toMs());
    s.writeFloat(vbat);
    s.writeInt(vbat_charging);
    s.writeInt(period_size);
    s.writeInt(period);
    s.writeBoolean(pre_presence);
    s.writeBoolean(pos_presence);
    s.writeInt(cag_v_digital);
    s.writeFloat(energia_ativa);
    s.writeFloat(energia_reativa);
    s.writeFloat(energia_total);
    i.save(s);
    v.save(s);
    fp.save(s);
    t.save(s);
  }

  final public void load(final TXStreamReader s) throws IOException{
    s.readInt();
    utc = new TUTC(s.readLong());
    vbat = s.readFloat();
    vbat_charging = s.readInt();
    period_size = s.readInt();
    pre_presence = s.readBoolean();
    pos_presence = s.readBoolean();
    period = s.readInt();
    cag_v_digital = s.readInt();
    energia_ativa = s.readFloat();
    energia_reativa = s.readFloat();
    energia_total = s.readFloat();
    i.load(s);
    v.load(s);
    fp.load(s);
    t.load(s);
  }

}
