/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import ab.etc.data.TUTC;
import java.io.IOException;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
public class TState {

  private boolean unknown;
  private TUTC utc;
  private boolean state;

  public TState(){
    unknown = true;
    utc = new TUTC(0);
  }

  public void set_state(final boolean s, final TUTC utc_){
    if(s!=state||unknown){
      unknown = false;
      state = s;
      utc = utc_;
    }
  }

  public boolean get_state(){
    return state;
  }

  public TUTC get_utc(){
    return utc;
  }

  public String get_info(){
    return unknown ? "?" : (state ? "S" : "N")+" "+utc.date(true)+" "+utc.time(true);
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    //
    s.writeBoolean(unknown);
    if(!unknown){
      s.writeLong(utc.toMs());
      s.writeBoolean(state);
    }
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt();
    //
    unknown = s.readBoolean();
    if(unknown){
      utc = new TUTC(0);
    }else{
      utc = new TUTC(s.readLong());
      state = s.readBoolean();
    }
  }

}
