/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.sensor;

import java.util.ArrayList;
import java.util.HashMap;
import owl2_lib.persistent.IPersistentHandler;

/**
 *
 */
public class TSensorList {

  final private TSensorList owner;
  final private ArrayList<TSensor> list;
  final private HashMap<Integer, TSensor> map;
  final private IPersistentHandler persistent_handler;
  final private ILog log;

  private TSensorList(final TSensorList owner_, final IPersistentHandler ph, final ILog log_){
    owner = owner_;
    persistent_handler = ph;
    log = log_;
    list = new ArrayList<>();
    map = new HashMap<>();
  }

  /*
   * somente a lista principal pode criar novos sensores
   * e tem persistencia
   */
  public TSensorList(final IPersistentHandler ph, final ILog log_){
    this(null, ph, log_);
  }

  /*
   * listas secundárias podem criar sensores a partir da lista principal
   * mas tem sua propria lista de sensores (que também estarão na principal)
   */
  public TSensorList(final TSensorList owner_){
    this(owner_, owner_.persistent_handler, owner_.log);
  }

  public void clear(){
    list.clear();
    map.clear();
  }

  public int count(){
    return list.size();
  }

  public TSensor get_by_index(final int index){
    return list.get(index);
  }

  public TSensor get_by_sn(final int sn){
    return map.get(sn);
  }

  public TSensor get_by_sn(final int sn, final ESensorType type, final boolean create){
    TSensor s = map.get(sn);
    if(s==null&&create){
      if(owner!=null){
        // se já existe na principal, recupera ou cria
        s = owner.get_by_sn(sn, type, true);
      }else{
        s = new TSensor(sn, type, persistent_handler, log);
      }
      if(s!=null){
        add(s);
      }
    }
    return s;
  }

  public void add(final TSensor s){
    if(map.get(s.sn)==null){
      list.add(s);
      map.put(s.sn, s);
    }
  }

  public void remove(final int sn){
    final TSensor s = map.remove(sn);
    if(s!=null){
      list.remove(s);
    }
  }
}
