/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cache;

import x.etc.io.file.TXFile;
import x.etc.lib;

/**
 *
 * @author Usuario
 */
public class TCache {

  final private TXFile file;
  //
  final byte[] nbuf;
  //
  private int writing;
  private int reading;
  private int readingLen;

  public TCache(final String filename){
    nbuf = new byte[4];
    file = new TXFile(filename);
  }

  public boolean isOpen(){
    return file.isOpen();
  }

  public boolean open(){
    if(file.create(false, false)){
      if(file.size()<1000){
        writing = reading = 1000;
        writeHeader();
      }else{
        readHeader();
      }
      return true;
    }
    return false;
  }

  public void close(){
    file.close();
  }

  private void writeInt(final int value){
    lib.intToArray(value, nbuf, 0, true);
    file.write(nbuf);
  }

  private int readInt(){
    file.read(nbuf);
    return lib.arrayToInt(nbuf, 0, true);
  }

  private void writeHeader(){
    file.position(0);
    writeInt(writing);
    writeInt(reading);
  }

  private void readHeader(){
    file.position(0);
    writing = readInt();
    reading = readInt();
  }

  public void write(final byte[] data){
    synchronized(file){
      file.position(writing);
      writeInt(data.length);
      file.write(data);
      writing += data.length;
      writeHeader();
    }
  }

  public boolean isDataAvailable(){
    synchronized(file){
      return reading<writing;
    }
  }

  /*
   *
   * obtem o primeiro bloco nao confirmado sem retira-lo do arquivo
   *
   */
  public byte[] read(){
    synchronized(file){
      if(reading>=writing){
        return null;
      }
      file.position(reading);
      readingLen = readInt();
      final byte[] data = new byte[readingLen];
      file.read(data);
      return data;
    }
  }

  /*
   *
   * confirma e retira o primeiro bloco
   *
   */
  public void confirm(){
    synchronized(file){
      reading += readingLen;
      if(reading>writing||(reading>100*1024*1024&&reading>=writing)){
        // bug > ou muito grande e vazio
        reading = writing = 1000;
      }
      writeHeader();
    }
  }

}
