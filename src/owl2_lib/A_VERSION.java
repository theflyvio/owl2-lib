package owl2_lib;

/**
 *
 * LIB
 *
 */
public class A_VERSION {

  final static public String app = "owl2_lib";
  final static public int version = 1;

  /*
   *
   * 21/10/2020 1
   *  .versao "serviço"
   *
   */

  static private boolean done;

  static public void check(final String checker, final int minimum){
    if(!done){
      done = true;
      System.out.println(app+".version="+version);
    }
    if(version<minimum){
      System.err.println(app+".version needed="+minimum+" ["+checker+"]");
      System.exit(1);
    }
  }

}
