/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.db;

import java.sql.Connection;
import java.sql.DriverManager;
import x.etc.lib;

/**
 *
 * @author Usuario
 */
public class TDB {

  final private Object lock;
  private String host;
  private String port;
  private String database;
  private String user;
  private String password;

  public TDB(){
    lock = new Object();
  }

  public void configure(final String host_, final String port_, final String database_, final String user_, final String password_){
    synchronized(lock){
      host = host_;
      port = port_;
      database = database_;
      user = user_;
      password = password_;
    }
  }

  public Connection createConnection(){
    try{
      synchronized(lock){
        return DriverManager.getConnection("jdbc:postgresql://"+host+":"+port+"/"+database+"?ApplicationName=OwlGateway", user, password);
      }
    }catch(final Exception ex){
      lib.unhandledException(ex);
    }
    return null;
  }

}
