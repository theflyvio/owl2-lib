/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import x.etc.lib;

/**
 *
 * @author Usuario
 */
public class TDBSQL {

  final static public Calendar tzUTC = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

  final private TDB db;
  private Connection connection;
  private String sqlText;
  private PreparedStatement statement;
  private ResultSet resultset;
  final private ArrayList<TDBSQL> listSql;
  private boolean connection_owner;

  public TDBSQL(final TDB db_){
    db = db_;
    listSql = new ArrayList<>();
    connection_owner = true;
  }

  private void checkConnection(){
    if(connection_owner&&connection==null){
      connection = db.createConnection();
      for(final TDBSQL s : listSql){
        addConnectionPartner(s);
      }
    }
  }

  public void addConnectionPartner(final TDBSQL s){
    s.connection_owner = false;
    s.connection = connection;
    if(listSql.indexOf(s)==-1){
      listSql.add(s);
    }
  }

  public void close(){
    closeConnection();
  }

  private void closeResultSet(){
    if(resultset!=null){
      try{
        resultset.close();
      }catch(final Exception ex){
        lib.unhandledException("TDBSQL closeResultSet", ex);
      }
      resultset = null;
    }
  }

  private void closeStatement(){
    if(statement!=null){
      closeResultSet();
      try{
        statement.close();
      }catch(final Exception ex){
        lib.unhandledException("TDBSQL closeStatement", ex);
      }
      statement = null;
    }
  }

  private void closeConnection(){
    if(connection!=null){
      closeStatement();
      try{
        connection.close();
      }catch(final Exception ex){
        lib.unhandledException("TDBSQL closeConnection", ex);
      }
      connection = null;
    }
  }

  public boolean checkStatement(){
    try{
      if(statement==null){
        checkConnection();
        if(connection!=null){
          try{
            statement = connection.prepareStatement(sqlText);
          }catch(final SQLException ex){
            closeConnection();
            lib.unhandledException(ex);
          }
        }
      }
      return statement!=null;
    }catch(final Exception ex){
      closeStatement();
      return false;
    }
  }

  public boolean startTransaction(){
    if(connection!=null){
      try{
        connection.setAutoCommit(false);
        return true;
      }catch(final Exception ex){
        lib.unhandledException(ex);
      }
    }
    return false;
  }

  public boolean commit(){
    if(connection!=null){
      try{
        connection.commit();
        return true;
      }catch(final SQLException ex){
        closeConnection();
        lib.unhandledException(ex);
      }
    }
    return false;
  }

  public boolean rollback(){
    if(connection!=null){
      try{
        connection.rollback();
        return true;
      }catch(final SQLException ex){
        closeConnection();
        lib.unhandledException(ex);
      }
    }
    return false;
  }

  public void setSQL(final String sql) throws SQLException{
    sqlText = sql;
    closeStatement();
    checkStatement();
  }

  public void setNULL(final int n) throws SQLException{
    //if(statement!=null){
    statement.setNull(n, java.sql.Types.NULL);
    //}
  }

  public void setString(final int n, final String value) throws SQLException{
    //if(statement!=null){
    statement.setString(n, value);
    //}
  }

  public void setChar(final int n, final char value) throws SQLException{
    //if(statement!=null){
    statement.setString(n, String.valueOf(value));
    //}
  }

  public void setInt(final int n, final int value) throws SQLException{
    //if(statement!=null){
    statement.setInt(n, value);
    //}
  }

  public void setLong(final int n, final long value) throws SQLException{
    //if(statement!=null){
    statement.setLong(n, value);
    //}
  }

  public void setFloat(final int n, final float value) throws SQLException{
    //if(statement!=null){
    statement.setFloat(n, value);
    //}
  }

  public void setTimestamp(final int n, final Timestamp value) throws SQLException{
    //if(statement!=null){
    statement.setTimestamp(n, value, tzUTC);
    //}
  }

  public void setTimestamp(final int n, final Instant value) throws SQLException{
    //if(statement!=null){
    statement.setTimestamp(n, new Timestamp(value.toEpochMilli()), tzUTC);
    //}
  }

  public boolean execute(){
    //if(statement!=null){
    try{
      statement.execute();
      return true;
    }catch(final Exception ex){
      closeConnection();
      lib.unhandledException(ex);
    }
    //}
    checkStatement();
    return false;
  }

  public ResultSet query(){
    try{
      return statement.executeQuery();
    }catch(final Exception ex){
      closeConnection();
      lib.unhandledException(ex);
      checkStatement();
      return null;
    }
  }
}
