/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.measure;

import java.io.IOException;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
public class TMeasureMMM {

  final public TMeasure min;
  final public TMeasure med;
  final public TMeasure max;

  public TMeasureMMM(final IMeasureK k_){
    min = new TMeasure(k_);
    med = new TMeasure(k_);
    max = new TMeasure(k_);
  }

  public String get_info(final boolean digital){
    return "min="+min.get_info(digital)+" med="+med.get_info(digital)+" max="+max.get_info(digital);
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    // 1
    min.save(s);
    med.save(s);
    max.save(s);
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt();
    // 1
    min.load(s);
    med.load(s);
    max.load(s);
  }
}
