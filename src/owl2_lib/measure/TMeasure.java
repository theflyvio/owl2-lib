/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.measure;

import java.io.IOException;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
public class TMeasure {

  final private IMeasureK k;
  private float value;

  public TMeasure(final IMeasureK k_){
    k = k_;
  }

  public float get_k(){
    return k.get_k();
  }

  public void set_value(final float value_){
    value = value_;
  }

  public float get_value(){
    return value;
  }

  public void set_digital(final float digital){
    value = digital*k.get_k();
  }

  public float get_digital(){
    return value/k.get_k();
  }

  public String get_info(final boolean digital){
    return String.format("%10.2f", value)+(digital ? " ["+String.format("%8d", (int)get_digital())+"]" : "");
  }

  public void save(final TXStreamWriter s) throws IOException{
    s.writeInt(1);
    // 1
    s.writeFloat(value);
  }

  public void load(final TXStreamReader s) throws IOException{
    s.readInt();
    // 1
    value = s.readFloat();
  }

}
