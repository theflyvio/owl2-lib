/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.update;

import java.util.ArrayList;
import java.util.Arrays;
import z_owl.EProgram;
import z_owl.cmd.TINTERFACE_UPDATE_FW;
import x.etc.io.file.TXFile;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 * @author Usuario
 */
public class TFirmware {

  private EProgram program;
  private int version;
  private byte[] data;
  private int checksum;
  private int partCount;

  public EProgram getProgram(){
    return program;
  }

  public int getVersion(){
    return version;
  }

  public int getChecksum(){
    return checksum;
  }

  /**
   * carrega o firmware a partir do conteudo do hex file
   *
   * @param ab       conteudo do arquivo.hex
   * @param version_ versao do firmware sendo carregado
   * @return sucesso
   */
  private boolean loadFromByteArray(final byte[] hexdata, final EProgram program_, final int version_){
    try{
      program = program_;
      version = version_;
      final THexLineList h = new THexLineList(hexdata);
      data = h.convert();
      partCount = (data.length-1)/TINTERFACE_UPDATE_FW.CMD_UPDATE_BUFFER+1;
      checksum = 0;
      for(final int b : data){
        checksum += b&0xFF;
      }
      return true;
    }catch(final Exception ex){
      return false;
    }
  }

  /**
   * carrega o firmware a partir do nome do arquivo
   *
   * @param fileName_
   * @param program_
   * @param version_
   * @return
   */
  public boolean loadFromFile(final String fileName_, final EProgram program_, final int version_){
    try (final TXFile file = new TXFile(fileName_)){
      if(file.open(false)){
        return loadFromByteArray(file.readFile(), program_, version_);
      }
    }catch(final Exception ex){
    }
    return false;
  }

  public int getPartCount(){
    return partCount;
  }

  public byte[] getPart(final int part){
    final int len;
    if(part==partCount){
      len = (data.length-1)%TINTERFACE_UPDATE_FW.CMD_UPDATE_BUFFER+1;
    }else{
      len = TINTERFACE_UPDATE_FW.CMD_UPDATE_BUFFER;
    }
    final int from = (part-1)*TINTERFACE_UPDATE_FW.CMD_UPDATE_BUFFER;
    return Arrays.copyOfRange(data, from, from+len);
  }

  /*
   * formato do arquivo de envio
   *
   * 001 len dos dados (len 0 reservado para formato de comando com estrutura diferente)
   * 003 endereço (HIGH MID LOW)
   * len dados
   *
   * len = 0, proximo byte:
   * 1 = fim do arquivo
   */
  static private class THexLineList {

    final byte[] data;
    final ArrayList<THexLine> h;

    public THexLineList(final byte[] data_){
      data = data_;
      h = new ArrayList<>();
    }

    public byte[] convert() throws Exception{
      int pos = 0;
      do{
        final THexLine l = new THexLine();
        pos = l.read(data, pos);
        if(l.b!=null){
          h.add(l);
        }
      }while(pos<data.length);
      return groupBy(254);
    }

    private byte[] groupBy(final int size) throws Exception{
      if(h.size()>0){
        int pos = 0;
        int offset = 0;
        // ajusta os enderecos
        while(pos<h.size()){
          final THexLine l = h.get(pos);
          if(l.type==0x04){
            offset = ((l.b[0]<<8|l.b[1])&0xFFFF)<<16;
            h.remove(pos);
          }else{
            l.address += offset;
            //l.adjustLine();
            if(l.address>=0x1C000){ //&&l.address<0x200000){ // ignoramos o bootloader
              h.remove(pos);
              System.out.println("Ignorando address "+Integer.toHexString(l.address));
            }else{
              pos++;
            }
          }
        }
        THexLine line = h.get(0);
        pos = 0;
        while(pos<h.size()-1){
          final THexLine next = h.get(pos+1);
          if(line.type!=0){
            throw new Exception("invalid register type "+line.type);
          }
          if(next.address==line.address+line.b.length){
            int max = size-line.b.length;
            max = max>next.b.length ? next.b.length : max;
            line.b = Arrays.copyOf(line.b, line.b.length+max);
            System.arraycopy(next.b, 0, line.b, line.b.length-max, max);
            next.b = Arrays.copyOfRange(next.b, max, next.b.length);
            if(next.b.length==0){
              h.remove(pos+1);
            }else{
              next.address += max;
            }
          }
          if(pos<h.size()-1&&(line.b.length==size||h.get(pos+1).address!=line.address+line.b.length)){
            pos++;
            line = h.get(pos);
          }
        }
      }
      final TXStreamWriter sw = TXStreamWriter.create();
      int i = 0;
      while(i<h.size()){
        final THexLine l = h.get(i++);
        sw.writeByte((byte)l.b.length);
        sw.writeByte((byte)(l.address>>16));
        sw.writeByte((byte)(l.address>>8));
        sw.writeByte((byte)(l.address));
        sw.writeByteArrayOnly(l.b);
      }
      // finalizacao
      sw.writeByte((byte)0);
      sw.writeByte((byte)1);
      return sw.getByteArray();
    }

    /*
     *
     */
    static private class THexLine {

      public int address; // 2
      public int type; ///// 1
      public byte[] b; // count + 1 checksum

      public int read(final byte[] data, int pos) throws Exception{
        while(pos<data.length&&data[pos]!=':'){
          if(data[pos]!=0x0A&&data[pos]!=0x0D){
            throw new Exception("invalid file format");
          }
          pos++;
        }
        if(pos<data.length){
          pos++;
          final int count = readData(data, pos, 2);
          pos += 2;
          address = readData(data, pos, 4);
          pos += 4;
          type = readData(data, pos, 2);
          pos += 2;
          int q = count;
          b = new byte[count];
          int i = 0;
          while(q-->0){
            b[i++] = (byte)readData(data, pos, 2);
            pos += 2;
          }
          pos += 2; // checksum
        }
        return pos;
      }

      public void adjustLine() throws Exception{
        if(address==0){
          if(b.length!=4){
            throw new Exception("primeira linha != 4");
          }else{
            final byte o[] = b;
            b = new byte[4];
            for(int i = 0; i<4; i++){
              b[i] = o[3-i];
            }
          }
        }else{
          if((b.length%2)!=0){
            b = Arrays.copyOf(b, b.length+1);
            b[b.length-1] = (byte)0xFF;
          }
          final byte o[] = b;
          b = new byte[o.length];
          for(int i = 0; i<o.length; i += 2){
            b[i] = o[i+1];
            b[i+1] = o[i];
          }
        }
      }

      private int readData(final byte[] data, int pos, int count){
        int r = 0;
        while(count-->0){
          r = r*16;
          if(data[pos]<='9'){
            r += data[pos++]-'0';
          }else if(data[pos]>='a'){
            r += data[pos++]-('a'-10);
          }else{
            r += data[pos++]-('A'-10);
          }
        }
        return r;
      }

    }

  }
}
