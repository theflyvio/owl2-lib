/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.update;

import java.awt.Component;
import javax.swing.JFileChooser;
import z_owl.EProgram;

/**
 *
 * @author TheFlyvio
 */
final public class TFirmwareUpdate {

  final private TFirmware firmware;
  //
  private String fileName;
  private byte[] partSentArray;
  private int partSent;

  public TFirmwareUpdate(final TFirmware fw){
    firmware = fw==null ? new TFirmware() : fw;
    clear();
  }

  /**
   * abre uma tela de selecao de arquivos para carga do firmware
   *
   * @param parent   tela "pai" do filechooser
   * @param program_
   * @param version_ versao do firmware
   * @return sucesso
   * @throws Exception
   */
  public boolean select(final Component parent, final EProgram program_, final int version_) throws Exception{
    final JFileChooser fileChooser = new JFileChooser();
    if(fileChooser.showOpenDialog(parent)==JFileChooser.APPROVE_OPTION){
      return loadFromFile(fileChooser.getSelectedFile().getAbsolutePath(), program_, version_);
    }
    return false;
  }

  /**
   * carrega o firmware a partir do nome do arquivo
   *
   * @param fileName_
   * @param program_
   * @param version_
   * @return
   */
  public boolean loadFromFile(final String fileName_, final EProgram program_, final int version_){
    fileName = fileName_;
    if(firmware.loadFromFile(fileName_, program_, version_)){
      clear();
      return true;
    }
    return false;
  }

  public void clear(){
    partSentArray = new byte[(getPartCount()-1)/8+1];
    partSent = 0;
  }

  public String getFileName(){
    return fileName;
  }

  public EProgram getProgram(){
    return firmware.getProgram();
  }

  public int getVersion(){
    return firmware.getVersion();
  }

  public int getChecksum(){
    return firmware.getChecksum();
  }

  public byte[] getCheckData(){
    return partSentArray;
  }

  /**
   * seta a lista de partes ja recebidas, para reenvio de partes faltantes
   * bit 1 = enviado
   *
   * @param partSentArray_
   */
  public void setPartSentArray(final byte[] partSentArray_){
    partSentArray = partSentArray_;
    partSent = 0;
  }

  /**
   * qual a ultima parte enviada
   *
   * @return
   */
  public int getPartSent(){
    return partSent;
  }

  /**
   * quantas partes tem o firmware
   * baseado no pacote de tamanho TINTERFACE_UPDATE_FW.CMD_UPDATE_BUFFER
   *
   * @return
   */
  public int getPartCount(){
    return firmware.getPartCount();
  }

  /**
   * informa se a parte indicada ja foi enviada
   * bit 1 = enviado
   *
   * @param n
   * @return
   */
  private boolean isPartSent(final int n){
    final int pbyte = (n-1)/8;
    final int pbit = (n-1)%8;
    return (partSentArray[pbyte]&(0x01<<pbit))>0;
  }

  /**
   * marca todas as partes como enviadas
   */
  public void setAllSent(){
    for(int i = 1; i<=getPartCount(); i++){
      setPartSent(i);
    }
  }

  /**
   * marca uma parte como enviada
   * bit 1 = enviado
   *
   * @param n = numero da parte iniciando em 1
   */
  public void setPartSent(final int n){
    final int pbyte = (n-1)/8;
    final int pbit = (n-1)%8;
    partSentArray[pbyte] |= 0x01<<pbit;
  }

  public byte[] getData(final boolean next){
    if(partSent<=getPartCount()&&next){
      if(partSent==0){
        partSent = 1;
      }
      while(partSent<=getPartCount()&&isPartSent(partSent)){
        partSent++;
      }
    }
    if(partSent>getPartCount()){
      return new byte[0];
    }
    setPartSent(partSent);
    return firmware.getPart(partSent);
  }

  public byte[] getDataNotSent(){
    partSent = 1;
    while(partSent<=getPartCount()&&isPartSent(partSent)){
      partSent++;
    }
    if(partSent>getPartCount()){
      return null;
    }
    return firmware.getPart(partSent);
  }
}
