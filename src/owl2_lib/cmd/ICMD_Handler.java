/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import owl2_lib.IPeer;
import owl2_lib.sensor.TPack;

/**
 *
 * @author Usuario
 */
public interface ICMD_Handler {

  void print(final String msg);

  int get_sn();

  boolean is_gprs_main(int sn);

  boolean is_remote_control();

  void peer_connected(final IPeer peer_);

  void peer_disconnected(final IPeer peer_);

  void pack_invalid(final TPack p);

  void send_ack(final int n);

  void send_ping();

  void execute_pack_peer_done();

  void execute_pack_c(final TPack p, final TCMD_c c);

  void execute_pack_j(final TPack p, final TCMD_j c);

  void execute_pack_l(final TPack p, final TCMD_l c);

  void execute_pack_ack(final TPack p, final TCMD_ack c);

  void execute_pack_r_a(final TPack p, final TCMD_r_a c);

  void execute_pack_r_p(final TPack p, final TCMD_r_p c);

  void execute_pack_r_s(final TPack p, final TCMD_r_s c);

  void execute_pack_u_k(final TPack p, final TCMD_u_k c);

  void on_exception(final String msg, final Exception ex);

}
