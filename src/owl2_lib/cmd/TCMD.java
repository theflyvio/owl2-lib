/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import ab.etc.convert.convert;
import owl2_lib.TCoordinates;
import owl2_lib.owl_lib;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 * @author Usuario
 */
abstract public class TCMD {

  static public int read_2(final TXStreamReader s) throws Exception{
    return convert.to_short(s.readByteArray(2), 0, true)&0xFFFF;
  }

  static public float read_2_float(final TXStreamReader s) throws Exception{
    return convert.to_short(s.readByteArray(2), 0, true);
  }

  static public int read_4(final TXStreamReader s) throws Exception{
    return convert.to_int(s.readByteArray(4), 0, true);
  }

  static public float read_4_float(final TXStreamReader s) throws Exception{
    return convert.to_int(s.readByteArray(4), 0, true);
  }

  static public TCoordinates read_coo(final TXStreamReader s) throws Exception{
    return new TCoordinates(owl_lib.gps2float(read_4(s)), owl_lib.gps2float(read_4(s)));
  }

  /*
   * header TX
   * 0.... uchar start; // 1 'g' ou 'u'
   * 1...2 uint len; ///// 2 do conteudo APOS n somente header = 0
   * 3.... uchar cmd; //// 1 protocolo
   * 4...5 uint n; /////// 2 numero p/ ack
   * 6...7 uint checksum_h
   * 8...9 uint checksum_d
   */
  static public void write_header_sensor(final TXStreamWriter s, final ECMD cmd, final int n) throws Exception{
    s.writeByte(cmd.get_type());
    s.writeShort((short)0); // len
    s.writeByte((byte)cmd.getId());
    s.writeShort((short)n);
    s.writeShort((short)0); // espaço para checksum_h
    s.writeShort((short)0); // espaço para checksum_d
    //
  }

  static public byte[] write_finish_sensor(final TXStreamWriter s){
    final byte[] ba = s.getByteArray();
    convert.to_array((short)(ba.length-10), ba, 1, true);
    //
    int checksum_h = 0;
    for(int i = 0; i<6; i++){
      checksum_h += ba[i]&0xFF;
    }
    convert.to_array((short)checksum_h, ba, 6, true);
    //
    int checksum_d = 0;
    for(int i = 10; i<ba.length; i++){
      checksum_d += ba[i]&0xFF;
    }
    convert.to_array((short)checksum_d, ba, 8, true);
    return ba;
  }

  /*
   * header RX len=12
   * 0.... uchar start; /// 1 'g'
   * 1...2 uint len; ////// 2 len do conteudo APOS cmd
   * 3...6 ulong sn; ////// 4 sn do hub
   * 7...8 uint section; // 2 secao
   * 9.... uchar cmd; ///// 1 protocolo
   * 10.11 uint n; //////// 2 numero p/ ack
   * 12.13 uint checksum_h
   * 14.15 uint checksum_d
   */
  static public void write_header_console(final TXStreamWriter s, final int sn, final ECMD cmd, final int n) throws Exception{
    s.writeByte(cmd.get_type());
    s.writeShort((short)0); // len
    s.writeInt(sn);
    s.writeShort((short)0);
    s.writeByte((byte)cmd.getId());
    s.writeShort((short)n);
    s.writeShort((short)0); // espaço para checksum_h
    s.writeShort((short)0); // espaço para checksum_d
    //
  }

  static public byte[] write_finish_console(final TXStreamWriter s){
    final byte[] ba = s.getByteArray();
    convert.to_array((short)(ba.length-16), ba, 1, true);
    //
    int checksum_h = 0;
    for(int i = 0; i<12; i++){
      checksum_h += ba[i]&0xFF;
    }
    convert.to_array((short)checksum_h, ba, 12, true);
    //
    int checksum_d = 0;
    for(int i = 16; i<ba.length; i++){
      checksum_d += ba[i]&0xFF;
    }
    convert.to_array((short)checksum_d, ba, 14, true);
    return ba;
  }

}
