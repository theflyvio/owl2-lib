/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 *
 * fw -> gw
 * pacote com um periodo completo de dados
 *
 */
public class TCMD_u_k extends TCMD {

  public TCMD_u u;
  public int part;

  public TCMD_u_k(final TCMD_u u_){
    u = u_;
  }

  /*
   * header_u
   * [2] part
   */
  public void read(final TXStreamReader s) throws Exception{
    part = s.readShort();
  }

}
