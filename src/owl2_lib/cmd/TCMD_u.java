/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import java.io.IOException;
import z_owl.EProgram;
import x.etc.io.streams.TXStreamReader;

/**
 *
 */
public class TCMD_u {

  final public EProgram program;
  final public int version;
  final public int part_count;
  final public char sub;

  public TCMD_u(final TXStreamReader sr) throws IOException{
    program = EProgram.getById(sr.readByte());
    version = sr.readShort();
    part_count = sr.readShort();
    sub = (char)sr.readByte();
  }

}
