/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import owl2_lib.owl_lib;
import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 *
 * fw -> gw
 * pacote com um periodo completo de dados
 *
 */
public class TCMD_r_p extends TCMD {

  public TCMD_RX header;
  //
  public int ver;
  public int period_size;
  public int period;
  public int day;
  public int month;
  public int year;
  public float vbat;
  //
  public float i_min;
  public float i_med;
  public float i_max;
  //
  public float v_min;
  public float v_med;
  public float v_max;
  //
  public float fp_min;
  public float fp_med;
  public float fp_max;
  //
  public int t_min;
  public int t_med;
  public int t_max;
  //
  public int v_dig;
  public boolean pre_presence;
  public boolean pos_presence;
  public int vbat_charging;
  public float energia_total;
  public float energia_ativa;
  public float energia_reativa;
  //
  public boolean final_values;

  public TCMD_r_p(final TCMD_RX header_){
    header = header_;
  }

  private int valid_temp(final int v, final int old){
    return v>-50&&v<125 ? v : old;
  }

  public void read(final TXStreamReader s) throws Exception{
    period_size = read_2(s);
    final int pp_period = read_2(s);
    period = pp_period&0x0FFF;
    day = s.readByte();
    month = s.readByte();
    if(header.v>=2){
      year = 2000+s.readByte();
    }else{
      year = 2020;
    }
    vbat = owl_lib.bits2vbat(s.readByte());
    // na v5 passou a ser o valor em amperes mas continuou sendo 2 casas
    i_min = read_4_float(s)/100;
    i_med = read_4_float(s)/100;
    i_max = read_4_float(s)/100;
    // depois da v5 precisa analisar a versao pra determinar o valor
    float m_v_min = read_4_float(s);
    float m_v_med = read_4_float(s);
    float m_v_max = read_4_float(s);
    //
    float m_f_min = read_2_float(s);
    float m_f_med = read_2_float(s);
    float m_f_max = read_2_float(s);
    //
    t_min = valid_temp(owl_lib.bits2temp(s.readByte()), -50);
    t_med = valid_temp(owl_lib.bits2temp(s.readByte()), -50);
    t_max = valid_temp(owl_lib.bits2temp(s.readByte()), -50);
    // arruma a media por um que estiver certo
    if(t_med==-50){
      t_med = t_min==-50 ? t_max : t_min;
    }
    if(t_min==-50){
      t_min = t_med;
    }
    if(t_max==-50){
      t_max = t_med;
    }
    if(header.v>=9){
      ver = s.readUByte();
      if(ver>=2){
        v_dig = read_4(s);
        if(ver>=3){
          pre_presence = (pp_period&0x8000)>0;
          pos_presence = (pp_period&0x4000)>0;
          if(ver>=4){
            vbat_charging = read_2(s);
            if(ver>=5){
              final_values = true;
              energia_total = read_4_float(s)/100;
              energia_ativa = read_4_float(s)/100;
              energia_reativa = read_4_float(s)/100;
              // agora que sabemos a versao, ajustamos os valores lidos
              v_min = m_v_min/100;
              v_med = m_v_med/100;
              v_max = m_v_max/100;
              //
              fp_min = m_f_min/100;
              fp_med = m_f_med/100;
              fp_max = m_f_max/100;
            }else{
              // antes da v5 os valores eram digitais, a rotina que usa os valores tem que fazer a conversao
              v_min = m_v_min;
              v_med = m_v_med;
              v_max = m_v_max;
              //
              fp_min = owl_lib.bits2fp((int)m_f_min);
              fp_med = owl_lib.bits2fp((int)m_f_med);
              fp_max = owl_lib.bits2fp((int)m_f_max);
            }
          }
        }
      }
    }
//    if(v_min>15000000){
//      v_min = 15000000;
//    }
//    if(v_med>15000000){
//      v_med = 15000000;
//    }
//    if(v_max>15000000){
//      v_max = 15000000;
//    }
//
  }

  public int rtc_h(){
    return (period*period_size)/60;
  }

  public int rtc_m(){
    return (period*period_size)%60;
  }

  public String get_print(){
    final StringBuilder sb = new StringBuilder();
    final int tmins = period*period_size;
    sb.append("registro r: ").append(day).append("/").append(month).append("/").append(year).append(' ').append(tmins/60).append(':').append(tmins%60)
            .append("\n   vbat=").append(vbat).append(" charg=").append(vbat_charging).append(" presence=").append(pre_presence ? 'Y' : 'N').append('/').append(pos_presence ? 'Y' : 'N')
            .append("\n   i   =").append(i_min).append(',').append(i_med).append(',').append(i_max)
            .append("\n   v   =").append(v_min).append(',').append(v_med).append(',').append(v_max)
            .append("\n   fp  =").append(fp_min).append(',').append(fp_med).append(',').append(fp_max)
            .append("\n   t   =").append(t_min).append(',').append(t_med).append(',').append(t_max)
            .append("\n   en t=").append(energia_total).append(" a=").append(energia_ativa).append(" r=").append(energia_reativa);
    return sb.toString();
  }
}
