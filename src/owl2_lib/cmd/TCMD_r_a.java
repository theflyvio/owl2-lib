/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import owl2_lib.owl_lib;
import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 *
 * fw -> gw
 * pacote com um periodo completo de dados
 *
 */
public class TCMD_r_a extends TCMD {

  public TCMD_RX header;
  //
  public int v; // versao desse registro (h.v 8)
  public TRTC rtc;
  public float vbat;
  public boolean i_alarm;
  public int direction_i;
  public int afund;
  public boolean presence_v;
  public boolean presence_i;
  public int i_inc_ref1;
  public int i_inc_ref2;
  public int i_inc_tx1;
  public int i_inc_tx2;

  public TCMD_r_a(final TCMD_RX header_){
    header = header_;
    rtc = new TRTC();
  }

  /*
   * alarme de corrente
   * curto temporario/permanente
   * uchar rec; 'a'
   * // 0x01
   * TRTC rtc;
   * uchar i_alarm;
   * uchar afund;
   * uchar presence;
   */
  public void read(final TXStreamReader s) throws Exception{
    rtc.read(s);
    vbat = owl_lib.bits2vbat(s.readByte());
    i_alarm = s.readByte()!=0;
    afund = s.readUByte();
    final int presence = s.readUByte();
    presence_v = (presence&0x01)==0x01;
    presence_i = (presence&0x02)==0x02;
    if(header.v>=3){
      i_inc_tx1 = read_2(s);
      i_inc_tx2 = read_2(s);
      if(header.v>=4){
        i_inc_ref1 = read_2(s);
        i_inc_ref2 = read_2(s);
        if(header.v>=8){
          v = s.readUByte();
          if(v>=2){
            direction_i = s.readUByte();
          }
        }
      }
    }
  }

  public String get_print(){
    return "registro r.a "+(rtc.valid ? rtc.utc.date_time(true) : "rtc_invalid")+" i_alarm="+i_alarm+" v="+presence_v+" i="+presence_i+" afund="+afund+" tx="+i_inc_tx1+","+i_inc_tx2+" ref="+i_inc_ref1+","+i_inc_ref2+" dir="+((direction_i>>1)&0x01)+","+(direction_i&0x01);
  }
}
