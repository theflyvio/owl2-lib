/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import z_owl.EInstallStep;
import owl2_lib.TCoordinates;
import static owl2_lib.cmd.TCMD.read_2;
import static owl2_lib.cmd.TCMD.read_2_float;
import static owl2_lib.cmd.TCMD.read_4;
import static owl2_lib.cmd.TCMD.read_4_float;
import owl2_lib.owl_lib;
import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 *
 * fw -> gw
 * pacote com um periodo completo de dados
 *
 */
public class TCMD_r_s extends TCMD {

  public TCMD_RX header;
  //
  public int v; // versao desse registro a partir de h.v 8
  public TRTC rtc;
  public int fw_version;
  public float vbat;
  public boolean presence_v;
  public boolean presence_i;
  public int direction_i;
  public int direction_iLoops;
  public boolean alarm;
  public int afund;
  public TRTC rtc_alarm;
  public float k_i;
  public int v_check;
  public int curto_c;
  public int i_mult;
  public int cag_set;
  public float i_min;
  public int ade_offset;
  public boolean only_hub1;
  public int install_v;
  public int install_v_dig;
  public float irms;
  public float fp;
  public int temp;
  public int vrms;
  public int period_size;
  public EInstallStep install_step;
  public int cag_mux;
  public int cag_pot;
  public float k_v;
  public int connect_spread;
  //
  public int reset_PCON0;
  public int reset_module;
  public int reset_event_n;
  public int reset_step;
  public int reset_interrupt;
  public int reset_count;
  public int reset_motive;
  public int reset_h;
  public int reset_m;
  public TCoordinates coo;

  public TCMD_r_s(final TCMD_RX header_){
    header = header_;
    rtc = new TRTC();
    rtc_alarm = new TRTC();
  }

  /*
   * alarme de corrente
   * curto temporario/permanente
   * uchar rec; 'a'
   * // 0x01
   * TRTC rtc;
   * uchar i_alarm;
   * uchar afund;
   * uchar presence;
   */
  public void read(final TXStreamReader s) throws Exception{
    rtc.read(s);
    fw_version = read_2(s);
    vbat = owl_lib.bits2vbat(s.readUByte());
    final int presence = s.readUByte();
    presence_v = (presence&0x01)>0;
    presence_i = (presence&0x02)>0;
    direction_i = s.readUByte();
    direction_iLoops = s.readUByte();
    final int amag = s.readUByte();
    alarm = (amag&0x80)>0;
    afund = amag&0x7F;
    rtc_alarm.read(s);
    k_i = owl_lib.bits2k(read_4(s));
    v_check = owl_lib.bits2v_check(read_2(s));
    curto_c = s.readUByte();
    i_mult = s.readUByte();
    cag_set = s.readUByte();
    i_min = owl_lib.bits2i_min(s.readUByte());
    ade_offset = read_4(s);
    only_hub1 = s.readUByte()>0;
    install_v = read_2(s);
    install_v_dig = read_4(s);
    irms = read_4_float(s);
    fp = read_2_float(s);
    temp = owl_lib.bits2temp(s.readUByte());
    vrms = read_4(s);
    period_size = read_2(s);
    install_step = EInstallStep.getById(s.readUByte());
    if(header.v>=5){
      reset_PCON0 = s.readUByte();
      reset_module = s.readUByte();
      reset_event_n = s.readUByte();
      reset_step = s.readUByte();
      reset_interrupt = s.readUByte();
      if(header.v>=6){
        reset_h = s.readUByte();
        reset_m = s.readUByte();
        reset_count = s.readUByte();
        reset_motive = s.readUByte();
        if(header.v>=7){
          coo = read_coo(s);
          if(header.v>=8){
            v = s.readUByte();
            cag_mux = s.readUByte();
            cag_pot = s.readUByte();
            if(v>=2){
              k_v = owl_lib.bits2k(read_4(s));
              connect_spread = read_2(s);
            }
          }
        }
      }
    }
  }

  private String getResetMotive(final int m){
    switch(m){
      case 1:
        return "UNKNOWN";

      case 2:
        return "NOT_ASKED";

      case 3:
        return "GPRS";

      case 4:
        return "UTC<00:04";

      case 5:
        return "UPDATE";

      default:
        return "WRONG_VALUE";

    }
  }

  private String getPCON0Text(final int v){
    String r = Integer.toBinaryString(v);
    if((v&0x80)>0){
      r += " STKOV";
    }
    if((v&0x40)>0){
      r += " STKUN";
    }
    if((v&0x20)==0){
      r += " WWDT";
    }
    if((v&0x10)==0){
      r += " WDT";
    }
    if((v&0x08)==0){
      r += " MCLR";
    }
    if((v&0x04)==0){
      r += " RES";
    }
    if((v&0x02)==0){
      r += " POR";
    }
    if((v&0x01)==0){
      r += " BOR";
    }
    return r+" ";
  }

  public String get_print(){
    final StringBuilder sb = new StringBuilder();
    sb.append("registro s:")
            .append("\n          reset (").append(reset_count).append(") ").append(reset_h).append(":").append(reset_m).append(" motive: ").append(getResetMotive(reset_motive)).append(" PCON0:").append(getPCON0Text(reset_PCON0)).append(" m:").append(reset_module).append(" e:").append(reset_event_n).append(" s:").append(reset_step).append(" i:").append(reset_interrupt)
            .append("\n          vbat:").append(vbat)
            .append("\n          rtc:").append(rtc.valid ? rtc.utc.date_time(true) : "invalid")
            .append("\n          cag:").append(cag_set)
            .append("\n          k i/v:").append(k_i).append("/").append(k_v)
            .append("\n          install_v_dig: ").append(install_v_dig).append(" mux: ").append(cag_mux).append(" pot: ")
            .append(cag_pot);
    return sb.toString();
  }
}
