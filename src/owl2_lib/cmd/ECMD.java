/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

/**
 *
 * @author Usuario
 */
public enum ECMD {
  G_ACK('!', (byte)'g'), // gw->fw->gw,
  G_PING('.', (byte)'g'), // gw->fw,
  G_PEER_DONE('#', (byte)'g'), // fw->gw
  G_SERVER_DONE('*', (byte)'g'), // gw->fw
  G_CONFIG('b', (byte)'g'), // gw->fw->gw
  U_COMMAND('c', (byte)'u'), // fw->gw
  U_LOGIN('j', (byte)'u'), // fw->gw
  G_LOGIN('l', (byte)'g'), // fw->gw
  G_REQ('q', (byte)'g'), // gw->fw
  G_RECORD('r', (byte)'g'), // gw->fw->gw
  G_UPDATE('u', (byte)'g'), // gw->fw->gw
  //
  U_ACK('#', (byte)'u'), // gw->fw->gw,
  U_PING(';', (byte)'u'), // gw->fw,
  //
  UNKNOWN(0xFF,(byte)0);

  final private int id;
  final byte peer_type;

  private ECMD(final int id_, final byte peer_type_){
    id = id_;
    peer_type = peer_type_;
  }

  public int getId(){
    return id;
  }

  public byte get_type(){
    return peer_type;
  }

  static public ECMD getById(final int id){
    for(final ECMD c : values()){
      if(c.id==id){
        return c;
      }
    }
    return null;
  }

}
