/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import z_owl.cmd.IRTC;
import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 *
 * fw -> gw
 * pacote com um registro "mesh"
 * 'a' = CMD_r_a alarme
 * 'p' = CMD_r_p periodo
 * 'c' = CMD_r_c config
 *
 */
public class TCMD_RX extends TCMD implements IRTC {

  public int v; // versao do registro
  public int sn;
  public int n; // numero do pacote na rede mesh
  public int when; // lista no hub
  public int crc; // crc rf
  public char rec; // tipo de registro

  public TCMD_RX(){
  }

/*
 * camada de transporte rf
 *  uchar v; // versao do protocolo
 *  ulong sn;
 *  uint n; // numero do pacote usado na rede mesh
 *  uchar cmd; lista que entrou no hub 'n'=now ou 'l'=lazy
 *  uchar rec; tipo de registro
 *
 */
  public void read(final TXStreamReader s) throws Exception{
    v = s.readByte();
    sn = read_4(s);
    n = read_2(s); // ignora o numero na rede mesh
    when = s.readByte()&0xFF; // ignora a lista no hub
    crc = s.readShort();
    rec = (char)(s.readByte()&0xFF);
  }

  @Override
  public int rtc_h(){
    return 0;
  }

  @Override
  public int rtc_m(){
    return 0;
  }

}
