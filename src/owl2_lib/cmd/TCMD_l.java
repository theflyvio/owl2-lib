/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 */
public class TCMD_l extends TCMD {

  public int sn;

  public TCMD_l(){
  }

  /*
   * [1] v
   * [1] count
   * count x
   * [4] sn
   */
  public void read(final TXStreamReader s) throws Exception{
    final int v = s.readByte();
    s.readByte(); // sempre 1
    sn = read_4(s);
  }

}
