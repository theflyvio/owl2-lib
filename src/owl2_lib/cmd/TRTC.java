/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import ab.etc.data.TUTC;
import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 */
public class TRTC {

  public TUTC utc;
  public int minutes;
  public int hours;
  public boolean valid;

  public void read(final TXStreamReader s) throws Exception{
    s.readByte(); // hundredths
    final int seconds = s.readByte();
    minutes = s.readByte();
    hours = s.readByte();
    final int days = s.readByte();
    final int months = s.readByte();
    final int years = 2000+s.readByte();
    try{
      utc = new TUTC(years, months, days, hours, minutes, seconds);
      valid = true;
    }catch(final Exception ex){
      utc = new TUTC(0);
      valid = false;
    }
  }

}
