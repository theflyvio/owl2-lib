/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import x.etc.io.streams.TXStreamReader;

/**
 *
 * @author Usuario
 */
public class TCMD_ack extends TCMD {

  public int n;
  public boolean ack;

  public TCMD_ack(){
  }

  /*
   * [1] v
   * [2] n
   * [1] value 0=noack 1=ack
   */
  public void read(final TXStreamReader s) throws Exception{
    //final int v =
    s.readByte();
    n = read_2(s);
    ack = s.readByte()!=0;
  }

}
