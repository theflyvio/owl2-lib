/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_lib.cmd;

import ab.etc.string.string;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import owl2_lib.sensor.TSensorConfig;
import owl2_lib.update.TFirmwareUpdate;
import x.etc.io.streams.TXStreamWriter;

/**
 * gw -> fw
 *
 */
public class TCMD_TX extends TCMD {

  final private TXStreamWriter s;

  public TCMD_TX(){
    s = TXStreamWriter.create(true);
  }

  /*
   * cmd '!'
   * header
   */
  public byte[] g_ask_ack(final int n) throws Exception{
    write_header_sensor(s, ECMD.G_ACK, n);
    return write_finish_sensor(s);
  }

  /*
   * cmd '*'
   * header
   */
  public byte[] g_ask_ping() throws Exception{
    write_header_sensor(s, ECMD.G_PING, 0);
    return write_finish_sensor(s);
    //return G_PING;
  }

  /*
   * cmd '.'
   * header
   */
  public byte[] g_ask_server_done() throws Exception{
    write_header_sensor(s, ECMD.G_SERVER_DONE, 0);
    return write_finish_sensor(s);
    //return G_SERVER_DONE;
  }

  /*
   * cmd 'b' atualizacao de configuracoes
   * header
   * [1] 'b'
   * [1] version
   * [4] sn
   * [2] v_check;
   * [1] curto_c;
   * [1] i_mult;
   * [1] cag_set;
   * [1] i_min;
   * [4] ade_offset;
   * [2] period_size;
   * // 0x02
   * [1] direction_i_loops
   * // 0x03
   * [2] connect_spread
   */
  public byte[] g_ask_b(final int n,
                        final int sn,
                        final TSensorConfig config) throws Exception{
    write_header_sensor(s, ECMD.G_CONFIG, n);
    s.writeByte((byte)'b'); // rec
    s.writeByte((byte)0x03); // v
    s.writeInt(sn);
    s.writeShort((short)(config.v_check>>8));
    s.writeByte((byte)config.curto_c);
    s.writeByte((byte)config.i_mult);
    s.writeByte((byte)config.set_cag);
    s.writeByte((byte)config.i_min);
    s.writeInt(config.ade_offset);
    s.writeShort((short)config.period_size);
    // 0x02
    s.writeByte((byte)config.direction_i_loops);
    // 0x03
    s.writeShort((short)config.connect_spread);
    return write_finish_sensor(s);
  }

  /*
   * cmd 'q'
   * header
   * [1] 'q'
   * [1] version
   * [4] sn
   * [1] req type
   * dados
   */
  private void g_ask_q(final int n, final int sn, final char req_type) throws Exception{
    write_header_sensor(s, ECMD.G_REQ, n);
    s.writeByte((byte)'q'); // rec
    s.writeByte((byte)0x02); // v
    s.writeInt(sn);
    s.writeByte((byte)req_type);
  }

  public byte[] g_ask_q_a(final int n, final int sn, final int mux, final int pot, final int samples) throws Exception{
    g_ask_q(n, sn, 'a');
    s.writeByte((byte)mux);
    s.writeByte((byte)pot);
    s.writeByte((byte)samples);
    return write_finish_sensor(s);
  }

  public byte[] g_ask_q_c(final int n, final int sn) throws Exception{
    g_ask_q(n, sn, 'c');
    return write_finish_sensor(s);
  }

  public byte[] g_ask_q_f(final int n,
                          final int sn,
                          final int client_id,
                          final String gprs_apn,
                          final String gprs_userpass,
                          final String gprs_ip,
                          final int gprs_port,
                          final int rf_channel,
                          final int rf_dbm) throws Exception{
    g_ask_q(n, sn, 'f');
    s.writeShort((short)client_id);
    s.writeByteArray(string.rpad(gprs_apn, 150, '\0').getBytes());
    s.writeByteArray(string.rpad(gprs_userpass, 40, '\0').getBytes());
    s.writeByteArray(string.rpad(gprs_ip, 16, '\0').getBytes());
    s.writeShort((short)gprs_port);
    s.writeByte((byte)rf_channel);
    s.writeByte((byte)rf_dbm);
    return write_finish_sensor(s);
  }

  public byte[] g_ask_q_h(final int n, final int sn) throws Exception{
    final ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
    g_ask_q(n, sn, 'h');
    s.writeByte((byte)utc.getDayOfMonth());
    s.writeByte((byte)utc.getMonthValue());
    s.writeByte((byte)(utc.getYear()%100));
    s.writeByte((byte)utc.getHour());
    s.writeByte((byte)utc.getMinute());
    s.writeByte((byte)utc.getSecond()); // 0x02
    return write_finish_sensor(s);
  }

  public byte[] g_ask_q_i(final int n, final int sn) throws Exception{
    g_ask_q(n, sn, 'i');
    return write_finish_sensor(s);
  }

  public byte[] g_ask_q_o(final int n, final int sn, final int connect_spread) throws Exception{
    g_ask_q(n, sn, 'o');
    s.writeShort((short)connect_spread);
    return write_finish_sensor(s);
  }

  public byte[] g_ask_q_p(final int n, final int sn, final int period_size) throws Exception{
    g_ask_q(n, sn, 'p');
    s.writeShort((short)period_size);
    return write_finish_sensor(s);
  }

  public byte[] g_ask_q_x(final int n, final int sn) throws Exception{
    g_ask_q(n, sn, 'x');
    return write_finish_sensor(s);
  }

  /*
   * cmd 'u'
   * header
   * [1] 'u'
   * [1] sub 'c'/'p'
   * [1] program
   * [2] version
   * [2] part_count
   * dados
   */
  private void g_ask_u(final int n, final char req_sub, final TFirmwareUpdate fwu) throws Exception{
    write_header_sensor(s, ECMD.G_UPDATE, n);
    s.writeByte((byte)'u'); // rec
    s.writeByte((byte)req_sub);
    s.writeByte((byte)fwu.getProgram().getId());
    s.writeShort((short)fwu.getVersion());
    s.writeShort((short)fwu.getPartCount());
  }

  public byte[] g_ask_u_c(final int n, final TFirmwareUpdate fwu) throws Exception{
    g_ask_u(n, 'c', fwu);
    return write_finish_sensor(s);
  }

  public byte[] g_ask_u_p(final int n, final TFirmwareUpdate fwu) throws Exception{
    final byte[] data = fwu.getDataNotSent();
    if(data==null){
      return null;
    }
    g_ask_u(n, 'p', fwu);
    s.writeShort((short)fwu.getPartSent());
    s.writeShort((byte)data.length);
    s.writeByteArrayOnly(data);
    return write_finish_sensor(s);
  }

  /*
   *
   *
   */
 /*
   * cmd '!'
   * header
   */
  public byte[] u_ask_ack(final int n, final int sn) throws Exception{
    write_header_console(s, sn, ECMD.U_ACK, n);
    return write_finish_console(s);
  }

  public byte[] u_ask_ping(final int sn) throws Exception{
    write_header_console(s, sn, ECMD.U_PING, 0);
    return write_finish_console(s);
  }

  public byte[] u_ask_j(final int n, final int sn, final String name) throws Exception{
    write_header_console(s, sn, ECMD.U_LOGIN, n);
    s.writeByte((byte)1); // v
    s.writeShortString(name);
    return write_finish_console(s);
  }

  public byte[] u_ask_c(final int n, final int sn, final byte[]cmd) throws Exception{
    write_header_console(s, sn, ECMD.U_COMMAND, n);
    s.writeByte((byte)1); // v
    s.writeByteArray(cmd);
    return write_finish_console(s);
  }

}
