/*
 *
 *
 *
 */
package z_owl.value;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import z_owl.cmd.IRTC;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 * @author TheFlyvio
 */
public class TValue {

  private TValueData value;

  // horario no server
  public int local_year;
  public int local_month;
  public int local_day;
  public int local_h;
  public int local_m;
  //
  public int rtc_h;
  public int rtc_m;

  public TValue(){
    value = new TValueDataInt(0);
  }

  private String getTime(final int h, final int m){
    if(h==-1){
      return "*n/a*";
    }
    return String.format("%02d", h)+":"+String.format("%02d", m);
  }

  public void setLOCAL(){
    final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
    local_year = now.getYear();
    local_month = now.getMonthValue();
    local_day = now.getDayOfMonth();
    local_h = now.getHour();
    local_m = now.getMinute();
  }

  public void setRTC(final IRTC c){
    if(c==null){
      rtc_h = -1;
      rtc_m = -1;
    }else{
      rtc_h = c.rtc_h();
      rtc_m = c.rtc_m();
    }
  }

  public String getLocalDate(){
    return String.format("%02d", local_day)+"/"+String.format("%02d", local_month)+"/"+String.format("%04d", local_year);
  }

  public String getLocalTime(){
    return getTime(local_h, local_m);
  }

  public boolean getRTCValid(){
    return rtc_h>-1;
  }

  public String getRTC(){
    return getTime(rtc_h, rtc_m);
  }

  final public void setInt(final IRTC c, final int v){
    setRTC(c);
    setLOCAL();
    value = new TValueDataInt(v);
  }

  final public void setFloat(final IRTC c, final float v){
    setRTC(c);
    setLOCAL();
    value = new TValueDataFloat(v);
  }

  final public void setBoolean(final IRTC c, final boolean v){
    setRTC(c);
    setLOCAL();
    value = new TValueDataBoolean(v);
  }

  public int getInt(){
    return value.getInt();
  }

  public float getFloat(){
    return value.getFloat();
  }

  public boolean getBoolean(){
    return value.getBoolean();
  }

  public void mult(final float k){
    value = new TValueDataFloat(value.getFloat()*k);
  }

  public void set(final TValue v){
    final IRTC rtc = new IRTC() {
      @Override
      public int rtc_h(){
        return v.rtc_h;
      }

      @Override
      public int rtc_m(){
        return v.rtc_m;
      }
    };
    switch(v.value.type()){
      case BOOLEAN:
        setBoolean(rtc, v.getBoolean());
        break;

      case FLOAT:
        setFloat(rtc, v.getFloat());
        break;

      case INT:
        setInt(rtc, v.getInt());
        break;
    }
  }

  public int toInt(){
    switch(value.type()){
      case BOOLEAN:
        return getBoolean() ? 1 : 0;

      case FLOAT:
        return (int)getFloat();

      case INT:
        return getInt();
    }
    return 0;
  }

  public String getText(int sizei, int sized){
    switch(value.type()){
      case BOOLEAN:
        return "["+getRTC()+"="+String.format("%"+sizei+"s", getBoolean())+"]";

      case FLOAT:
        return "["+getRTC()+"="+String.format("%"+sizei+"."+sized+"f", getFloat())+"]";

      case INT:
        return "["+getRTC()+"="+String.format("%"+sizei+"d", getInt())+"]";
    }
    return "?";
  }

  public void save(final TXStreamWriter sw) throws IOException{
    sw.writeInt(1);
    //
    sw.writeInt(local_day);
    sw.writeInt(local_month);
    sw.writeInt(local_year);
    sw.writeInt(local_h);
    sw.writeInt(local_m);
    sw.writeInt(rtc_h);
    sw.writeInt(rtc_m);
    //
    sw.writeInt(value.type().id);
    value.save(sw);
  }

  public void load(final TXStreamReader sr) throws IOException{
    sr.readInt();
    //
    local_day = sr.readInt();
    local_month = sr.readInt();
    local_year = sr.readInt();
    local_h = sr.readInt();
    local_m = sr.readInt();
    rtc_h = sr.readInt();
    rtc_m = sr.readInt();
    //
    value = EValue.getById(sr.readInt()).create();
    value.load(sr);
  }

  static private enum EValue {
    NONE(0),
    INT(1),
    FLOAT(2),
    BOOLEAN(3);

    final public int id;

    private EValue(final int id_){
      id = id_;
    }

    static public EValue getById(final int id){
      for(final EValue v : values()){
        if(v.id==id){
          return v;
        }
      }
      return NONE;
    }

    public TValueData create(){
      switch(this){
        case BOOLEAN:
          return new TValueDataBoolean(false);

        case FLOAT:
          return new TValueDataFloat(0);

        case INT:
          return new TValueDataInt(0);
      }
      return null;
    }
  }

  static private abstract class TValueData {

    abstract public EValue type();

    abstract public void setBoolean(final boolean v);

    abstract public void setInt(final int v);

    abstract public void setFloat(final float v);

    abstract public boolean getBoolean();

    abstract public int getInt();

    abstract public float getFloat();

    abstract public void save(final TXStreamWriter sw) throws IOException;

    abstract public void load(final TXStreamReader sr) throws IOException;
  }

  static private class TValueDataInt extends TValueData {

    public int value;

    @Override
    public EValue type(){
      return EValue.INT;
    }

    public TValueDataInt(final int v){
      value = v;
    }

    @Override
    public void setBoolean(final boolean v){
    }

    @Override
    public void setInt(int v){
      value = v;
    }

    @Override
    public void setFloat(float v){
      value = (int)v;
    }

    @Override
    public int getInt(){
      return value;
    }

    @Override
    public float getFloat(){
      return value;
    }

    @Override
    public boolean getBoolean(){
      return false;
    }

    @Override
    public void save(final TXStreamWriter sw) throws IOException{
      sw.writeInt(value);
    }

    @Override
    public void load(final TXStreamReader sr) throws IOException{
      value = sr.readInt();
    }

  }

  static private class TValueDataFloat extends TValueData {

    public float value;

    @Override
    public EValue type(){
      return EValue.FLOAT;
    }

    public TValueDataFloat(final float v){
      value = v;
    }

    @Override
    public void setBoolean(final boolean v){
    }

    @Override
    public void setInt(int v){
      value = v;
    }

    @Override
    public void setFloat(float v){
      value = v;
    }

    @Override
    public int getInt(){
      return (int)value;
    }

    @Override
    public float getFloat(){
      return value;
    }

    @Override
    public boolean getBoolean(){
      return false;
    }

    @Override
    public void save(final TXStreamWriter sw) throws IOException{
      sw.writeFloat(value);
    }

    @Override
    public void load(final TXStreamReader sr) throws IOException{
      value = sr.readFloat();
    }
  }

  static private class TValueDataBoolean extends TValueData {

    public boolean value;

    @Override
    public EValue type(){
      return EValue.BOOLEAN;
    }

    public TValueDataBoolean(final boolean v){
      value = v;
    }

    @Override
    public void setBoolean(final boolean v){
      value = v;
    }

    @Override
    public void setInt(int v){
    }

    @Override
    public void setFloat(float v){
    }

    @Override
    public int getInt(){
      return value ? 1 : 0;
    }

    @Override
    public float getFloat(){
      return value ? 1 : 0;
    }

    @Override
    public boolean getBoolean(){
      return value;
    }

    @Override
    public void save(final TXStreamWriter sw) throws IOException{
      sw.writeBoolean(value);
    }

    @Override
    public void load(final TXStreamReader sr) throws IOException{
      value = sr.readBoolean();
    }
  }

}
