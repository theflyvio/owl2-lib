/*
 *
 *
 *
 */
package z_owl.value;

import java.io.IOException;
import z_owl.cmd.IRTC;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 * @author TheFlyvio
 */
public class TValueMMM {

  public TValue min;
  public TValue max;
  public TValue med;

  public TValueMMM(){
    min = new TValue();
    max = new TValue();
    med = new TValue();
  }

  public void mult(final float k){
    min.mult(k);
    med.mult(k);
    max.mult(k);
  }

  public void setFloat(final IRTC rtc, final float min_, final float med_, final float max_){
    min.setFloat(rtc, min_);
    med.setFloat(rtc, med_);
    max.setFloat(rtc, max_);
  }

  public void set(final TValueMMM mmm){
    min.set(mmm.min);
    med.set(mmm.med);
    max.set(mmm.max);
  }

  public boolean isSamePeriod(){
    return min.rtc_h%6==max.rtc_h%6&&med.rtc_h%6==max.rtc_h%6;
  }

  public String getText(int sizei, int sized){
    return String.format("%-22s", "min"+min.getText(sizei, sized))
           +String.format("%-22s", " med"+med.getText(sizei, sized))
           +String.format("%-22s", " max"+max.getText(sizei, sized));
  }

  public void save(final TXStreamWriter sw) throws IOException{
    sw.writeInt(1);
    //
    min.save(sw);
    max.save(sw);
    med.save(sw);
  }

  public void load(final TXStreamReader sr) throws IOException{
    sr.readInt();
    //
    min.load(sr);
    max.load(sr);
    med.load(sr);
  }

}
