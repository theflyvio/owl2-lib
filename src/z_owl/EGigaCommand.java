package z_owl;

/**
 *
 * @author user
 */
public enum EGigaCommand {
  NONE(0, "NONE"),
  TURN_GPS_ON(1, ""),
  TURN_GPS_OFF(2, ""),
  CAG_SET(3, ""),
  CAG_RESET(4, ""),
  CAG_FREE(5, ""),
  FLASH_TEST(6, ""),
  GPRS_CONNECT(7, ""),
  GPRS_CONNECT_FAST(8, ""),
  SEND_CONFIG(9, ""),
  RESET(10, ""),
  RESET_MUX_POT(11, ""),
  FINISH_CALIBRATION(12, ""),
  SET_SERIAL(13, ""),
  SET_RTC(14, ""),
  SET_PROGRAM_MODE(15, ""),
  CLEAR_LISTS(16, ""),
  ADD_DAY(17, ""),
  PREAMBLE_LEN(18, ""),
  SET_CH(19, ""),
  RF_TEST(20,""),
  GPRS_TEST(21,""),
  GPRS_LOAD(22,""),
  SET_CLIENT(23,""),
  SET_MUX_POT(24,""),
  LISTENER_OFF(201, ""),
  LISTENER_ON(202, ""),
  SET_TALKER(203, ""),
  ;

  static final private EGigaCommand[] _values = values();
  final private int _id;
  final private String _name;

  private EGigaCommand(final int id, final String name){
    _id = id;
    _name = name;
  }

  public int getId(){
    return _id;
  }

  public String getName(){
    return _name;
  }

  static public EGigaCommand getById(final int id){
    for(final EGigaCommand p : _values){
      if(p._id==id){
        return p;
      }
    }
    return null;
  }

  static public EGigaCommand getByName(final String name){
    for(final EGigaCommand p : _values){
      if(p._name.equals(name)){
        return p;
      }
    }
    return null;
  }

}
