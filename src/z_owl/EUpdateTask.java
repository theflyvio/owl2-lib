/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

/**
 *
 * @author user
 */
public enum EUpdateTask {
  BROADCAST(0, "BROADCAST"),
  P2P(1, "P2P"),
  CHECK(2, "CHECK"),
  CHECKP2P(3, "CHECKP2P"),
  EXECUTE(4, "EXECUTE");

  static final private EUpdateTask[] _values = values();
  final private int _id;
  final private String _name;

  private EUpdateTask(final int id, final String name){
    _id = id;
    _name = name;
  }

  public int getId(){
    return _id;
  }

  public String getName(){
    return _name;
  }

  static public EUpdateTask getById(final int id){
    for(final EUpdateTask p : _values){
      if(p._id==id){
        return p;
      }
    }
    return null;
  }

  static public EUpdateTask getByName(final String name){
    for(final EUpdateTask p : _values){
      if(p._name.equals(name)){
        return p;
      }
    }
    return null;
  }

}
