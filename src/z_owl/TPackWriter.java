/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

import owl2_lib.TCoordinates;
import java.io.IOException;
import x.etc.io.streams.TXStreamWriter;
import x.etc.lib;

/**
 *
 * @author TheFlyvio
 */
final public class TPackWriter {

  final private TXStreamWriter sw;

  public TPackWriter(){
    sw = TXStreamWriter.create(true);
    try{
      // prepara o cabeçalho
      sw.writeByteArrayOnly(new byte[6]);
    }catch(final IOException ex){
    }
  }

  public void writeBoolean(final boolean v) throws IOException{
    writeOne(v ? 1 : 0);
  }

  public void writeOne(final int v) throws IOException{
    sw.writeByte((byte)v);
  }

  public void writeString(final String s, final int q) throws IOException{
    int max = s.length()>=q ? q-1 : s.length();
    sw.writeByteArrayOnly((s.substring(0, max)+lib.strReplicate("\0", q)).substring(0, q).getBytes());
  }

  public void writeTwo(final int v) throws IOException{
    final byte[] data = new byte[2];
    lib.shortToArray((short)v, data, 0, true);
    sw.writeByteArrayOnly(data);
  }

  public void writeFour(final int v) throws IOException{
    final byte[] data = new byte[4];
    lib.intToArray(v, data, 0, true);
    sw.writeByteArrayOnly(data);
  }

  public void writeEight(final long v) throws IOException{
    final byte[] data = new byte[8];
    lib.intToArray((int)(v>>>32), data, 0, true);
    lib.intToArray((int)v, data, 4, true);
    sw.writeByteArrayOnly(data);
  }

  public void writeFloat4(final float v) throws IOException{
    writeFour((int)(v*10000));
  }

  public void writeByteArray(final byte[] v, final boolean writeSize) throws IOException{
    if(writeSize){
      writeOne(v.length);
    }
    sw.writeByteArrayOnly(v);
  }

  public void writeProgram(final EProgram v) throws IOException{
    writeOne(v.getId());
  }

  public void writeProgramMode(final EProgramMode v) throws IOException{
    writeOne(v.getId());
  }

  public void writeInstallStep(final EInstallStep v) throws IOException{
    writeOne(v.getId());
  }

  public void writeGPSMode(final EGPSMode v) throws IOException{
    writeOne(v.getId());
  }

  public void writeGigaCommand(final EGigaCommand v) throws IOException{
    writeOne(v.getId());
  }

  long gps2ulong(float c){
    int g;
    int m;
    int d;
    boolean q;
    q = c<0;
    if(q){
      c = -c;
    }
    g = (int)c;
    c -= g;
    c *= 60; // transforma em minutos
    m = (int)c; // minutos
    c -= m;
    d = (int)(c*10000);
    return ((((g*60+m)*10000)+d)<<1)+(q ? 1 : 0);
  }

  public void writeCoordinates(final TCoordinates v) throws IOException{
    //writeFloat4(v.latitude);
    //writeFloat4(v.longitude);
    writeFour((int)gps2ulong(v.latitude));
    writeFour((int)gps2ulong(v.longitude));
  }

  public byte[] getByteArray(){
    final byte[] r = sw.getByteArray();
    final int len = r.length-6;
    r[0] = (byte)0xD9;
    r[1] = (byte)len; //(byte)((len+0x32)^0xFF);
    lib.shortToArray((short)lib.crc16(r, 6, len), r, 2, true);
    lib.shortToArray((short)lib.crc16(r, 0, 4), r, 4, true);
    return r;
  }
}
