/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

/**
 *
 * @author user
 */
public enum EProgramMode {
  CALIB(0, "Calibração"),
  TESTS(1, "Testes"),
  CONFIGURED(2, "Configurado"),
  INSTALL(3, "Instalando"),
  WORK(4, "Trabalhando"),
  FINISH_CALIBRATION(99, "Finalização da calibração"),
  NONE(255,"Aguardando conexão");

  static final private EProgramMode[] _values = values();
  final private int _id;
  final private String _name;

  private EProgramMode(final int id, final String name){
    _id = id;
    _name = name;
  }

  public int getId(){
    return _id;
  }

  public String getName(){
    return _name;
  }

  static public EProgramMode getById(final int id){
    for(final EProgramMode p : _values){
      if(p._id==id){
        return p;
      }
    }
    return null;
  }

  static public EProgramMode getByName(final String name){
    for(final EProgramMode p : _values){
      if(p._name.equals(name)){
        return p;
      }
    }
    return null;
  }

}
