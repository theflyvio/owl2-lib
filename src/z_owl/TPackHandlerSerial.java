/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

import x.etc.lib;
import x.serial.TXSerial;
import x.serial.TXSerial.TXSerialPort;

/**
 *
 * @author TheFlyvio
 */
abstract public class TPackHandlerSerial extends TPackHandler {

  final private TXSerial _serial;

  public TPackHandlerSerial(){
    //
    _serial = new TXSerial(true) {
      @Override
      protected void onException(final EXSerialOperation op, final Exception ex){
        lib.unhandledException("erro na serial");
        close();
      }

      @Override
      protected void onDataAvailable(final byte[] data){
        for(final byte b : data){
          onByteAvailable(b);
        }
      }
    };
  }

  public boolean isOpened(){
    return _serial.isOpened();
  }

  public TXSerialPort getPort(){
    return _serial.getPort();
  }

  public void changePort(final TXSerial.TXSerialPort p){
    _serial.close();
    if(p!=null){
      _serial.setPort(p);
      _serial.open();
      open();
    }
  }

  @Override
  public void send(final byte[] data){
    _serial.write(data);
  }

}
