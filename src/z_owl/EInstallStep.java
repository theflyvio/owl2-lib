/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

/**
 *
 * @author user
 */
public enum EInstallStep {
  UNINSTALLED(0, "UNINSTALLED"),
  GATEWAY(90,"INSTALLING"),
  INSTALLED(100, "INSTALLED");

  static final private EInstallStep[] _values = values();
  final private int _id;
  final private String _name;

  private EInstallStep(final int id, final String name){
    _id = id;
    _name = name;
  }

  public int getId(){
    return _id;
  }

  public String getName(){
    return _name;
  }

  static public EInstallStep getById(final int id){
    for(final EInstallStep p : _values){
      if(p._id==id){
        return p;
      }
    }
    return null;
  }

  static public EInstallStep getByName(final String name){
    for(final EInstallStep p : _values){
      if(p._name.equals(name)){
        return p;
      }
    }
    return null;
  }

}
