/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

import owl2_lib.TCoordinates;
import x.etc.lib;

/**
 *
 * @author TheFlyvio
 */
final public class TPackReader {

  final private byte[] _data;
  private int _pos;

  public TPackReader(final byte[] data){
    _data = data;
  }

  public boolean hasMore(int q){
    return _pos+q<=_data.length;
  }

  public boolean readBoolean(){
    return readOne()!=0;
  }

  public String readString(final int q){
    return new String(readByteArray(q)).replaceAll("\0", "");
  }

  public int readOne(){
    return _data[_pos++]&0xFF;
  }

  public int readTwo(){
    final int r = lib.arrayToShort(_data, _pos, true);
    _pos += 2;
    return r;
  }

  public int readFour(){
    final int r = lib.arrayToInt(_data, _pos, true);
    _pos += 4;
    return r;
  }

  public long readEight(){
    final long msb = lib.arrayToInt(_data, _pos, true);
    _pos += 4;
    final long lsb = ((long)lib.arrayToInt(_data, _pos, true))&0xFFFFFFFF;
    _pos += 4;
    return (msb<<32)|lsb;
  }

  public float readFloat4(){
    final float r = lib.arrayToInt(_data, _pos, true);
    _pos += 4;
    return r/10000;
  }

  public float readFloat2(){
    final float r = lib.arrayToInt(_data, _pos, true);
    _pos += 4;
    return r/100;
  }

  public float readFloat5(){
    final float r = lib.arrayToInt(_data, _pos, true);
    _pos += 4;
    return r/100000;
  }

  public byte[] readByteArray(final int n){
    final byte[] r = new byte[n];
    for(int i = 0; i<n; i++){
      r[i] = (byte)readOne();
    }
    return r;
  }

  public byte[] readByteArray(){
    return readByteArray(readOne());
  }

  public EProgram readProgram(){
    return EProgram.getById(readOne());
  }

  public EProgramMode readProgramMode(){
    return EProgramMode.getById(readOne());
  }

  public EInstallStep readInstallStep(){
    return EInstallStep.getById(readOne());
  }

  public EGPSMode readGPSMode(){
    return EGPSMode.getById(readOne());
  }

  public EGigaCommand readGigaCommand(){
    return EGigaCommand.getById(readOne());
  }

  float gps2float(long c){
    boolean neg = (c&0x01)>0;
    c >>= 1;
    float d = c%10000;
    d /= 10000;
    c /= 10000;
    int g = (int)(c/60);
    c -= g*60;
    return (neg ? -1 : 1)*(g+(c+d)/60);
  }

// formula de conversao no fw
//  float gps2double(ulong c) {
//  //  uchar neg = c & 0x01;
//  //  c >>= 1;
//  //  double d = c % 10000;
//  //  d /= 10000;
//  //  c /= 10000;
//  //  uint g = c / 60;
//  //  c -= g * 60;
//  //  return (neg ? -1 : 1)*(g + (c + d) / 60);
//  uchar neg = c & 0x01;
//  float f = c >> 1;
//  return (neg ? -1 : 1)*(f / 60);
//}


  public TCoordinates readCoordinates(){
    return new TCoordinates(gps2float(readFour()), gps2float(readFour()));
  }

}
