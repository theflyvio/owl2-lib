/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

/**
 *
 * @author user
 */
public enum EProtocol {
  NONE(0, "NONE", false),
  SET_MODE(1, "SET_MODE", true),
  SEND_SECTION(2, "SEND_SECTION", true),
  SEND_RELEASE(3, "SEND_RELEASE", true),
  SEND_SET_ID(4, "SEND_SET_ID", true),
  INFO(5, "INFO", false),
  SET_SECTION(6, "SET_SECTION", true),
  SET_RTC(7, "SET_RTC", true),
  SET_PRESENCE(8, "SET_PRESENCE", true),
  REQ_ID(9, "REQ_ID", true),
  SET_GPS(10, "SET_GPS", true),
  HUB_IDENTIFICATION(11, "HUB_IDENTIFICATION", true),
  HUB_CONFIG(12, "HUB_CONFIG", true),
  INSTALL_INFO(13, "INSTALL_INFO", true),
  REQ_REQ_ID(14, "REQ_REQ_ID", true),
  PRESENCE(15, "PRESENCE", true),
  STUCK(16, "STUCK", true),
  ACK(17, "ACK", false),
  SEND_COLLECT(18, "SEND_COLLECT", true),
  GET_COLLECT(19, "GET_COLLECT", true),
  PING(20, "PING", false),
  GET_NEIGHBOURS(21, "GET_NEIGHBOURS", true),
  INTERFACE_UPDATE_FW(22, "INTERFACE_UPDATE_FW", true),
  PACK_SENT(23, "PACK_SENT", true),
  UPDATE_MISSING(24, "UPDATE_MISSING", true),
  SET_SN(25, "SET_SN", true),
  MEASURE(26, "MEASURE", false),
  MEASURE_K(27, "MEASURE_K", true),
  PACK_TRANSFER(28, "TRANSFER", true),
  ALARM_OLD(29, "ALARM_OLD", true),
  GIGA_COMMAND(30, "GIGA_COMMAND", true),
  PRESENCE_AFUND(31, "PRESENCE_AFUND", true),
  DIRECTION_I(32, "DIRECTION", true),
  LISTENER_INFO(33, "LISTENER_INFO", false),
  LISTENER_NET_MESH(34, "LISTENER_NET_MESH", false),
  GIGA_CONFIG(35, "GIGA_CONFIG", true),
  ;

  static final private EProtocol[] _values = values();
  final private int _id;
  final private String _name;
  final public boolean hasACK;

  private EProtocol(final int id, final String name, final boolean hasACK_){
    _id = id;
    _name = name;
    hasACK = hasACK_;
  }

  public int getId(){
    return _id;
  }

  public String getName(){
    return _name;
  }

  static public EProtocol getById(final int id){
    for(final EProtocol p : _values){
      if(p._id==id){
        return p;
      }
    }
    return null;
  }

  static public EProtocol getByName(final String name){
    for(final EProtocol p : _values){
      if(p._name.equals(name)){
        return p;
      }
    }
    return null;
  }

}
