/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

import z_owl.cmd.TPING;
import x.etc.io.socket.tcp.TXTCP;
import x.etc.io.socket.tcp.TXTCPClient;
import x.etc.timer.TXTimer;

/**
 *
 * @author TheFlyvio
 */
abstract public class TPackHandlerTCP extends TPackHandler {

  final private TXTCPClient _client;
  final private TXTimer _checkConnection;
  private int pingFault;

  public TPackHandlerTCP(final TXTCP tcp, final String host, final int port){
    setRepeatWhenNoACK(false);
    _client = new TXTCPClient(tcp) {

      @Override
      protected void onDataAvailable(final byte[] data){
        for(final byte b : data){
          onByteAvailable(b);
        }
      }

      @Override
      protected void onConnect(){
        pingFault = 0;
        _checkConnection.stop();
        _checkConnection.start();
        TPackHandlerTCP.this.onConnect();
      }

      @Override
      protected void onCantConnect(){
        TPackHandlerTCP.this.onCantConnect();
      }

      @Override
      protected void onDisconnect(){
        TPackHandlerTCP.this.onDisconnect();
      }
    };
    setHost(host);
    setPort(port);
    _checkConnection = new TXTimer(30000) {
      @Override
      public void run(){
        _client.connect();
        if(isConnected()&&pingFault++>1){
          _client.close();
        }
      }
    };
    _checkConnection.start();
  }

  @Override
  protected void on_PING(final TPING ping){
    pingFault = 0;
  }

  public final boolean isConnected(){
    return _client.isConnected();
  }

  final public String getHost(){
    return _client.getHost();
  }

  final public int getPort(){
    return _client.getPort();
  }

  final public void setHost(String host){
    _client.setHost(host);
  }

  final public void setPort(int port){
    _client.setPort(port);
  }

  @Override
  protected void send(final byte[] data){
    _client.send(data);
  }

  abstract protected void onConnect();

  abstract protected void onCantConnect();

  abstract protected void onDisconnect();

}
