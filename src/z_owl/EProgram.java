/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

/**
 *
 * @author user
 */
public enum EProgram {
  NONE(0, "NONE", false),
  INSTALLER(1, "INSTALLER", false),
  DEEP_SLEEP(2, "DEEP_SLEEP", false),
  MESH_HUB(3, "MESH_HUB", true),
  MESH_PEER(4, "MESH_PEER", true),
  P2P_HUB(6, "P2P_HUB", false),
  P2P_PEER(7, "P2P_PEER", true),
  LISTENER(8, "LISTENER", false);

  static final private EProgram[] _values = values();
  final private int _id;
  final private String _name;
  final private boolean _sensor;

  private EProgram(final int id, final String name, final boolean sensor_){
    _id = id;
    _name = name;
    _sensor = sensor_;
  }

  public int getId(){
    return _id;
  }

  public String getName(){
    return _name;
  }

  static public EProgram getById(final int id){
    for(final EProgram p : _values){
      if(p._id==id){
        return p;
      }
    }
    return null;
  }

  static public EProgram getByName(final String name){
    for(final EProgram p : _values){
      if(p._name.equals(name)){
        return p;
      }
    }
    return null;
  }

  public boolean isSensor(){
    return _sensor;
  }

}
