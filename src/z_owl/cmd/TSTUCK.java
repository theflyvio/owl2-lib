/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSTUCK extends TSEND {

  final public long path;
  final public int id_origin;

  public TSTUCK(final long path_, final int id_origin_) throws Exception{
    super();
    path = path_;
    id_origin = id_origin_;
    updateWriter();
  }

  public TSTUCK(final TPackReader sr) throws Exception{
    path = sr.readEight();
    id_origin = sr.readOne();
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeEight(path);
    pw.writeOne(id_origin);
  }

  public String asText(){
    return "\nstuck path.: "+Long.toString(path, 2)
           +"\nstuck origin: "+id_origin;
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.STUCK;
  }
}
