/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EGigaCommand;
import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class TGIGA_COMMAND extends TSEND {

  final public EGigaCommand cmd;
  final public int v;
  final public int sn;
  final public int[] p;

//  public TGIGA_COMMAND(final TPackReader r) throws Exception{
//    cmd = r.readOne();
//    v = r.readOne();
//    sn = r.readFour();
//    updateWriter();
//  }
  public TGIGA_COMMAND(final int sn_, final EGigaCommand cmd_, final int[] p_) throws Exception{
    sn = sn_;
    v = 9;
    cmd = cmd_;
    p = p_;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeGigaCommand(cmd);
    pw.writeOne(0x09); // versao do protocolo
    pw.writeFour(sn);
    for(int i = 0; i<p.length; i++){
      pw.writeFour(p[i]);
    }
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.GIGA_COMMAND;
  }

}
