/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TALARM extends TSEND {

  final public long path;
  final public long alarm;

  public TALARM(final long path_, final long alarm_) throws Exception{
    super();
    path = path_;
    alarm = alarm_;
    updateWriter();
  }

  public TALARM(final TPackReader sr) throws Exception{
    path = sr.readEight();
    alarm = sr.readEight();
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeEight(path);
    pw.writeEight(alarm);
  }

  public String asText(){
    return "\nalarm path....: "+Long.toString(path, 2)
           +"\nalarm state...: "+Long.toString(alarm, 2);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.ALARM_OLD;
  }

}
