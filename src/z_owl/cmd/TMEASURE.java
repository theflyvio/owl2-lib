/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import owl2_lib.owl_lib;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TMEASURE {

  final public long sn;
  final public int loop; ///////// 02 secao configurada
  final public int it;////////////// 04 sn de quem enviou
  final public float k;
  final public int kfp;
  final public boolean getv;
  final public int temp;
  final public boolean mem_ok;
  final public int tests_n;
  final public int tests_sector;
  final public int tests_errors;
  final public TMeasure[] measure;
  public String realtxt;
  //
  static public int[] sector_errors = new int[128];
  static public int sector_last_sum = -1;

  public TMEASURE(final TPackReader r){
    sn = ((long)r.readFour())&0xFFFFFFFF;
    loop = r.readTwo();
    it = r.readTwo();
    k = owl_lib.bits2k(r.readFour());
    kfp = r.readFour();
    getv = r.readBoolean();
    temp = r.readTwo();
    mem_ok = r.readBoolean();
    tests_n = r.readTwo();
    tests_sector = r.readOne();
    tests_errors = r.readFour();
    measure = new TMeasure[4];
    //
    measure[0] = new TMeasureFloat("fp....");
    measure[0].read(r);
    //
    //measure[1] = new TMeasureFloat("Irms..");
    measure[1] = new TMeasureFloat("Irms..");
    measure[1].read(r);
    //
    measure[2] = new TMeasureFloat("Vrms..");
    measure[2].read(r);
    //
    measure[3] = new TMeasureAngle("Angle.");
    measure[3].read(r);
    //
    if(tests_errors!=sector_last_sum){
      // aumentou o numero de erros
      if(sector_last_sum!=-1){
        sector_errors[tests_sector]++;
      }
      sector_last_sum = tests_errors;
    }
  }

  static public void clearErrors(){
    sector_last_sum = -1;
    for(int i = 0; i<sector_errors.length; i++){
      sector_errors[i] = 0;
    }
  }

  public float getIrmsMed(){
    return measure[1].getMedFloat();
  }

  public float getFPMed(){
    return measure[1].getMedFloat();
  }

  public String asText(){
    final StringBuilder sb = new StringBuilder();
    sb.append("Real=");
    sb.append(realtxt);
    sb.append("\n");
    sb.append("Loop=");
    sb.append(loop);
    sb.append(" it=");
    sb.append(it);
    sb.append(" getV=");
    sb.append(getv);
    sb.append(" temp=");
    sb.append(temp);
    sb.append(" mem=");
    sb.append(mem_ok);
    sb.append(" +");
    sb.append(tests_n);
    sb.append(" sec=");
    sb.append(tests_sector);
    sb.append(" err=");
    sb.append(tests_errors);
    sb.append("\n");
    sb.append("               loop        med        min        max          %\n");
    //
    for(final TMeasure m : measure){
      m.setK(1);
      m.asText(sb);
    }
    sb.append("\nk=");
    sb.append(k);
    sb.append(" kfp=");
    sb.append(kfp);
    sb.append("\n");
    //measure[0].setK(kfp); //1f/32768);
    measure[0].asText(sb);
    //
    measure[0].setK(1f/32768);
    measure[0].asText(sb);
    //
    measure[1].setK(k);
    measure[1].asText(sb);
    //
    sb.append("\nsetores com erros:\n");
    int q = 0;
    for(int i = 0; i<sector_errors.length; i++){
      if(sector_errors[i]>0){
        sb.append(i);
        sb.append("=");
        sb.append(sector_errors[i]);
        if(++q==10){
          q = 0;
          sb.append("\n");
        }else{
          sb.append(" ");
        }
      }
    }

    return sb.toString();
  }

  static abstract public class TMeasure {

    final public String text;
    public float k;

    public TMeasure(final String text_){
      text = text_;
    }

    public float getMedFloat(){
      return 0;
    }

    public void setK(final float k_){
      k = k_;
    }

    static public String adjust(final String s, int len){
      return String.format("%1$-"+len+"s", s);
    }

    static public String adjust(final int i, int len){
      return String.format("%1$"+len+"d", i);
    }

    static public String adjust(final float f, int len){
      return String.format("%1$"+len+".2f", f);
    }

    abstract public void read(final TPackReader r);

    abstract public void asText(final StringBuilder sb);

  };

  static public class TMeasureFloat extends TMeasure {

    public float now;
    public float med;
    public float min;
    public float max;

    public TMeasureFloat(final String text_){
      super(text_);
    }

    @Override
    public float getMedFloat(){
      return med;
    }

    @Override
    public void read(final TPackReader r){
      now = r.readFloat2();
      med = r.readFloat2();
      min = r.readFloat2();
      max = r.readFloat2();
    }

    @Override
    public void asText(final StringBuilder sb){
      float know = now*k;
      float kmed = med*k;
      float kmin = min*k;
      float kmax = max*k;
      sb.append(adjust(text, 8));
      sb.append(adjust(know, 11));
      sb.append(adjust(kmed, 11));
      sb.append(adjust(kmin, 11));
      sb.append(adjust(kmax, 11));
      sb.append(adjust(100-(kmin*100/kmax), 10));
      sb.append("\n");
    }
  };

  static public class TMeasureAngle extends TMeasure {

    public float now;
    public float med;
    public float min;
    public float max;

    public TMeasureAngle(final String text_){
      super(text_);
    }

    @Override
    public float getMedFloat(){
      return med;
    }

    @Override
    public void read(final TPackReader r){
      now = 180-(r.readFloat2()*0.0969f);
      med = 180-(r.readFloat2()*0.0969f);
      min = 180-(r.readFloat2()*0.0969f);
      max = 180-(r.readFloat2()*0.0969f);
    }

    @Override
    public void asText(final StringBuilder sb){
      float know = now;
      float kmed = med;
      float kmin = min;
      float kmax = max;
      sb.append(adjust(text, 8));
      sb.append(adjust(know, 11));
      sb.append(adjust(kmed, 11));
      sb.append(adjust(kmin, 11));
      sb.append(adjust(kmax, 11));
      sb.append(adjust(100-(kmin*100/kmax), 10));
      sb.append("\n");
    }
  };

  static public class TMeasureRMS extends TMeasure {

    final static public float UNIT = 0.5f/0xFFFFFF*1000;

    public float now;
    public float med;
    public float min;
    public float max;

    public TMeasureRMS(final String text_){
      super(text_);
    }

    @Override
    public float getMedFloat(){
      return med;
    }

    @Override
    public void read(final TPackReader r){
      now = r.readFloat2()*UNIT;
      med = r.readFloat2()*UNIT;
      min = r.readFloat2()*UNIT;
      max = r.readFloat2()*UNIT;
    }

    @Override
    public void asText(final StringBuilder sb){
      float know = now;
      float kmed = med;
      float kmin = min;
      float kmax = max;
      sb.append(adjust(text, 8));
      sb.append(adjust(know, 11));
      sb.append(adjust(kmed, 11));
      sb.append(adjust(kmin, 11));
      sb.append(adjust(kmax, 11));
      sb.append(adjust(100-(kmin*100/kmax), 10));
      sb.append("\n");
    }
  };

  static public class TMeasureInt extends TMeasure {

    public int now;
    public int med;
    public int min;
    public int max;

    public TMeasureInt(final String text_){
      super(text_);
    }

    @Override
    public float getMedFloat(){
      return med;
    }

    @Override
    public void read(final TPackReader r){
      now = r.readFour();
      med = r.readFour();
      min = r.readFour();
      max = r.readFour();
    }

    @Override
    public void asText(final StringBuilder sb){
      float know = now*k;
      float kmed = med*k;
      float kmin = min*k;
      float kmax = max*k;
      sb.append(adjust(text, 8));
      sb.append(adjust((int)know, 11));
      sb.append(adjust((int)kmed, 11));
      sb.append(adjust((int)kmin, 11));
      sb.append(adjust((int)kmax, 11));
      sb.append(adjust(100-(kmin*100f/kmax), 10));
      sb.append("\n");
    }
  };

}
