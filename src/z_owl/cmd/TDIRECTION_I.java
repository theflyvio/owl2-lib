/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TDIRECTION_I extends TSEND {

  final public long path;
  final public int rtc_h;
  final public int rtc_m;
  final public long direction_i;

  public TDIRECTION_I(final long path_,
                      final int rtc_h_,
                      final int rtc_m_,
                      final long direction_i_) throws Exception{
    super();
    path = path_;
    rtc_h = rtc_h_;
    rtc_m = rtc_m_;
    direction_i = direction_i_;
    updateWriter();
  }

  public TDIRECTION_I(final TPackReader sr) throws Exception{
    path = sr.readEight();
    rtc_h = sr.readOne();
    rtc_m = sr.readOne();
    direction_i = sr.readEight();
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeEight(path);
    pw.writeOne(rtc_h);
    pw.writeOne(rtc_m);
    pw.writeEight(direction_i);
  }

  public String asText(){
    return "\ndirection path.: "+Long.toString(path, 2)
           +"\ndirection state: "+Long.toString(direction_i, 2)+String.format(" %02d:%02d", rtc_h, rtc_m);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.DIRECTION_I;
  }

}
