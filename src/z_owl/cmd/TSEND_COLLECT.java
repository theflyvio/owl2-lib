/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSEND_COLLECT extends TSEND {

  final public int task;
  final public int n;
  final public int subtask;
  final public int par1;
  final public int par2;
  final public int par3;
  final public byte[] data;

  public TSEND_COLLECT(final int task_,
                       final int subtask_,
                       final int n_,
                       final int par1_,
                       final int par2_,
                       final int par3_,
                       final byte[] data_
  ) throws Exception{
    super();
    task = task_;
    subtask = subtask_;
    n = n_;
    par1 = par1_;
    par2 = par2_;
    par3 = par3_;
    data = data_;
    updateWriter();
  }

  public TSEND_COLLECT(final TPackReader sr) throws Exception{
    task = sr.readOne();
    subtask = sr.readOne();
    n = sr.readOne();
    par1 = sr.readOne();
    par2 = sr.readOne();
    par3 = sr.readOne();
    data = sr.readByteArray(233);
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(task);
    pw.writeOne(subtask);
    pw.writeOne(n);
    pw.writeOne(par1);
    pw.writeOne(par2);
    pw.writeOne(par3);
    for(int i = 0; i<233; i++){
      if(data==null||i>=data.length){
        pw.writeOne(0);
      }else{
        pw.writeOne(data[i]);
      }
    }
  }

  public String asText(){
    return "\ncollect task="+task+" subtask="+subtask+" n="+n+" par1="+par1+" par2="+par2+" par3="+par3;
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SEND_COLLECT;
  }
}
