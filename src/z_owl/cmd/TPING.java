/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TPING extends TSEND {

  public TPING() throws Exception{
    super();
    updateWriter();
  }

  public TPING(final TPackReader sr) throws Exception{
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.PING;
  }
}
