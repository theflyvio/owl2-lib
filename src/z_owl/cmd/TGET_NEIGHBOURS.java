/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class TGET_NEIGHBOURS extends TSEND {

  final private boolean _clear;
  final private int _section;

  public TGET_NEIGHBOURS(final boolean clear, final int section) throws Exception{
    super();
    _clear = clear;
    _section = section;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeBoolean(_clear);
    pw.writeTwo(_section);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.GET_NEIGHBOURS;
  }
}
