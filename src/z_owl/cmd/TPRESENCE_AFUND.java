/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TPRESENCE_AFUND extends TSEND {

  final public long path;
  final public int rtc_h;
  final public int rtc_m;
  final public byte[] data;

  public TPRESENCE_AFUND(final long path_,
                         final int rtc_h_,
                         final int rtc_m_,
                         final byte[] data_) throws Exception{
    super();
    path = path_;
    rtc_h = rtc_h_;
    rtc_m = rtc_m_;
    data = data_;
    updateWriter();
  }

  public TPRESENCE_AFUND(final TPackReader sr) throws Exception{
    path = sr.readEight();
    rtc_h = sr.readOne();
    rtc_m = sr.readOne();
    data = sr.readByteArray(70);
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeEight(path);
    pw.writeOne(rtc_h);
    pw.writeOne(rtc_m);
    pw.writeByteArray(data, false);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.PRESENCE_AFUND;
  }

  public String asText(){
    return "\npresence path.: "+Long.toString(path, 2)+ String.format(" %02d:%02d", rtc_h,rtc_m);
  }

}
