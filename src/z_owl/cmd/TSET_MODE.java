/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class TSET_MODE extends TSEND {

  final private int _mode;

  public TSET_MODE(final int mode) throws Exception{
    super();
    _mode = mode;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne((byte)_mode);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SET_MODE;
  }

}
