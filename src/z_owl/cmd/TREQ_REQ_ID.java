/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TREQ_REQ_ID extends TSEND {

  public TREQ_REQ_ID(final TPackReader r) throws Exception{
    updateWriter();
  }

  public TREQ_REQ_ID() throws Exception{
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(EProtocol.REQ_REQ_ID.getId());
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.REQ_REQ_ID;
  }

}
