/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import java.time.LocalTime;
import z_owl.EGPSMode;
import z_owl.EInstallStep;
import z_owl.EProgram;
import z_owl.EProgramMode;
import z_owl.EProtocol;
import owl2_lib.TCoordinates;
import owl2_lib.owl_lib;
import z_owl.TPackReader;
import x.etc.lib;

/**
 *
 * @author TheFlyvio
 */
final public class TINFO extends TSEND {

  final public int v;
  final public int sequence;
  final public EProgram program;
  final public EProgramMode program_mode;
  final public EGPSMode gps_mode;
  final public TCoordinates coo;
  final public int install_timer;
  final public int install_nb_count;
  final public int install_nb_other_count;
  final public int install_nb_remaining;
  final public int rtc_hour;
  final public int rtc_min;
  final public long sensor_sn;
  final public int sensor_section;
  final public int sensor_id;
  final public int sensor_orderCount;
  final public int sensor_errorcount;
  final public byte[] sorder;
  final public int my_order;
  final public float vbat;
  final public int vpresence;
  final public boolean rf_working;
  final public int mem_working;
  final public int v_offset;
  final public int i_offset;
  final public int rtc_sec;
  final public int t_last;
  final public float fp_last;
  final public int angle_last;
  final public float i_last;
  final public int fw_version;
  final public int v_dig_last;
  final public int i_dig_last;
  final public float sensor_k_i;
  final public int direction_i; // v2
  final public int rf_rcvd_n;
  final public int rf_sent_n;
  final public int gprs_state;
  final public int gprs_step;
  final public int gprs_gprs_wait_rf_count;
  final public int list_0;
  final public int list_1;
  final public int cag_mux;
  final public int cag_pot;
  final public int afund_h;
  final public int afund_m;
  final public int afund_v;
  final int cag_set;
  final int afund_count;
  final int reset_count;
  final int client_id;
  final int rf_channel;
  final int rf_dbm;
  final EInstallStep install_step;
  final int vbat_charging;
  final float sensor_k_v;
  final float v_last;
  // fixo
  final public TPackNB[] install_nb;

  public TINFO(
          int v_,
          int sequence_,
          EProgram program_,
          EProgramMode program_mode_,
          EGPSMode gps_mode_,
          TCoordinates coo_,
          int install_timer_,
          int install_nb_count_,
          int install_nb_other_count_,
          int install_nb_remaining_,
          int rtc_hour_,
          int rtc_min_,
          long sensor_sn_,
          int sensor_section_,
          int sensor_id_,
          int sensor_orderCount_,
          int sensor_errorcount_,
          byte[] sorder_,
          int my_order_,
          float vbat_,
          int vpresence_,
          boolean rf_working_,
          int mem_working_,
          int v_offset_,
          int i_offset_,
          int rtc_sec_,
          int t_last_,
          float fp_last_,
          int angle_last_,
          float i_last_,
          int fw_version_,
          int v_dig_last_,
          int i_dig_last_,
          float sensor_k_i_,
          int direction_i_,
          TPackNB[] install_nb_,
          int rf_rcvd_n_,
          int rf_sent_n_,
          int gprs_state_,
          int gprs_step_,
          int gprs_gprs_wait_rf_count_,
          int list_0_,
          int list_1_,
          int cag_mux_,
          int cag_pot_,
          int afund_h_,
          int afund_m_,
          int afund_v_,
          int cag_set_,
          int afund_count_,
          int reset_count_,
          int client_id_,
          int rf_channel_,
          int rf_dbm_,
          EInstallStep install_step_,
          int vbat_charging_,
          float sensor_k_v_,
          float v_last_
  ){
    v = v_;
    sequence = sequence_;
    program = program_;
    program_mode = program_mode_;
    gps_mode = gps_mode_;
    coo = coo_;
    install_timer = install_timer_;
    install_nb_count = install_nb_count_;
    install_nb_other_count = install_nb_other_count_;
    install_nb_remaining = install_nb_remaining_;
    rtc_hour = rtc_hour_;
    rtc_min = rtc_min_;
    sensor_sn = sensor_sn_;
    sensor_section = sensor_section_;
    sensor_id = sensor_id_;
    sensor_orderCount = sensor_orderCount_;
    sensor_errorcount = sensor_errorcount_;
    sorder = sorder_;
    my_order = my_order_;
    vbat = vbat_;
    vpresence = vpresence_;
    rf_working = rf_working_;
    mem_working = mem_working_;
    v_offset = v_offset_;
    i_offset = i_offset_;
    rtc_sec = rtc_sec_;
    t_last = t_last_;
    fp_last = fp_last_;
    angle_last = angle_last_;
    i_last = i_last_;
    fw_version = fw_version_;
    v_dig_last = v_dig_last_;
    i_dig_last = v_dig_last_;
    sensor_k_i = sensor_k_i_;
    direction_i = direction_i_;
    //
    rf_rcvd_n = rf_rcvd_n_;
    rf_sent_n = rf_sent_n_;
    gprs_state = gprs_state_;
    gprs_step = gprs_step_;
    gprs_gprs_wait_rf_count = gprs_gprs_wait_rf_count_;
    list_0 = list_0_;
    list_1 = list_1_;
    cag_mux = cag_mux_;
    cag_pot = cag_pot_;
    afund_h = afund_h_;
    afund_m = afund_m_;
    afund_v = afund_v_;
    cag_set = cag_set_;
    afund_count = afund_count_;
    reset_count = reset_count_;
    //
    install_nb = install_nb_;
    //
    client_id = client_id_;
    rf_channel = rf_channel_;
    rf_dbm = rf_dbm_;
    install_step = install_step_;
    vbat_charging = vbat_charging_;
    sensor_k_v = sensor_k_v_;
    v_last = v_last_;
    try{
      updateWriter();
    }catch(Exception ex){
    }
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.INFO;
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(v);
    pw.writeOne(sequence);
    pw.writeProgram(program);
    pw.writeProgramMode(program_mode);
    pw.writeGPSMode(gps_mode);
    pw.writeCoordinates(coo);
    pw.writeOne(install_timer);
    pw.writeOne(install_nb_count);
    pw.writeOne(install_nb_other_count);
    pw.writeOne(install_nb_remaining);
    pw.writeOne(rtc_hour);
    pw.writeOne(rtc_min);
    pw.writeFour((int)sensor_sn);
    pw.writeTwo(sensor_section);
    pw.writeOne(sensor_id);
    pw.writeTwo(sensor_errorcount);
    pw.writeOne(sensor_orderCount);
    pw.writeByteArray(sorder, true);
    pw.writeOne(my_order);
    pw.writeFloat4(vbat);
    pw.writeOne(vpresence);
    pw.writeBoolean(rf_working);
    pw.writeOne(mem_working);
    pw.writeOne(rtc_sec); // v3
    pw.writeTwo(rf_rcvd_n); // v4
    pw.writeTwo(rf_sent_n); // v4
    pw.writeFour(t_last);
    pw.writeFloat4(fp_last);
    pw.writeFloat4(angle_last);
    pw.writeFloat4(i_last);
    pw.writeTwo(fw_version);
    pw.writeFour(v_dig_last);
    pw.writeFour(i_dig_last);
    pw.writeFour(owl_lib.k2bits(sensor_k_i));
    if(v>=2){
      pw.writeOne(direction_i); // v2
      if(v>=5){
        pw.writeOne(gprs_state);
        pw.writeOne(gprs_step);
        pw.writeOne(gprs_gprs_wait_rf_count);
        pw.writeFour(list_0);
        pw.writeFour(list_1);
        pw.writeOne(cag_mux);
        pw.writeOne(cag_pot);
        if(v>=6){
          pw.writeOne(afund_h);
          pw.writeOne(afund_m);
          pw.writeOne(afund_v);
          if(v>=7){
            pw.writeOne(cag_set);
            pw.writeOne(afund_count);
            pw.writeOne(reset_count);
            if(v>=8){
              pw.writeTwo(client_id);
              pw.writeOne(rf_channel);
              pw.writeOne(rf_dbm);
              pw.writeInstallStep(install_step);
              if(v>=9){
                pw.writeTwo(vbat_charging);
                if(v>=10){
                  pw.writeFour(owl_lib.k2bits(sensor_k_i));
                  pw.writeFour((int)(v_last*100));
                }
              }
            }
          }
        }
      }
    }
    //
    pw.writeOne(install_nb.length);
    for(final TPackNB e : install_nb){
      pw.writeFour((int)e.sn);
      pw.writeFour(e.section);
    }
  }

  public TINFO(final TPackReader r){
    v = r.readOne();
    sequence = r.readOne();
    program = r.readProgram();
    program_mode = r.readProgramMode();
    gps_mode = r.readGPSMode();
    coo = r.readCoordinates();
    install_timer = r.readOne();
    install_nb_count = r.readOne();
    install_nb_other_count = r.readOne();
    install_nb_remaining = r.readOne();
    rtc_hour = r.readOne();
    rtc_min = r.readOne();
    sensor_sn = ((long)r.readFour())&0xFFFFFFFF;
    sensor_section = r.readTwo();
    sensor_id = r.readOne();
    sensor_errorcount = r.readTwo();
    sensor_orderCount = r.readOne();
    sorder = r.readByteArray();
    my_order = r.readOne();
    vbat = r.readFloat4();
    vpresence = r.readOne();
    rf_working = r.readBoolean();
    mem_working = r.readOne();
    v_offset = r.readTwo();
    i_offset = r.readTwo();
    rtc_sec = r.readOne(); // v3
    rf_rcvd_n = r.readTwo(); // v4
    rf_sent_n = r.readTwo(); // v4
    t_last = r.readFour();
    fp_last = r.readFloat4();
    angle_last = r.readTwo();
    i_last = r.readFloat4();
    fw_version = r.readTwo();
    v_dig_last = r.readFour();
    i_dig_last = r.readFour();
    sensor_k_i = owl_lib.bits2k(r.readFour());
    direction_i = v>=2 ? r.readOne() : 0;
    gprs_state = v>=5 ? r.readOne() : -1;
    gprs_step = v>=5 ? r.readOne() : -1;
    gprs_gprs_wait_rf_count = v>=5 ? r.readOne() : -1;
    list_0 = v>=5 ? r.readFour() : -1;
    list_1 = v>=5 ? r.readFour() : -1;
    cag_mux = v>=5 ? r.readOne() : -1;
    cag_pot = v>=5 ? r.readOne() : -1;
    afund_h = v>=6 ? r.readOne() : -1;
    afund_m = v>=6 ? r.readOne() : -1;
    afund_v = v>=6 ? r.readOne() : -1;
    cag_set = v>=7 ? r.readOne() : -1;
    afund_count = v>=7 ? r.readOne() : -1;
    reset_count = v>=7 ? r.readOne() : -1;
    client_id = v>=8 ? r.readTwo() : 0;
    rf_channel = v>=8 ? r.readOne() : 0;
    rf_dbm = v>=8 ? r.readOne() : 0;
    install_step = v>=8 ? r.readInstallStep() : EInstallStep.UNINSTALLED;
    vbat_charging = v>=9 ? r.readTwo() : 0;
    sensor_k_v = v>=10 ? owl_lib.bits2k(r.readFour()) : 0;
    v_last = v>=10 ? r.readFour()/100f : 0;
    //
    install_nb = new TPackNB[r.readOne()];
    for(int i = 0; i<install_nb.length; i++){
      final long sn = r.readFour();
      final int section = r.readTwo();
      install_nb[i] = new TPackNB(sn, section);
    }
    try{
      updateWriter();
    }catch(Exception ex){
    }
  }

  private void flashText(final StringBuilder sb){
    sb.append("\nFLASH.....: ");
    if(v<3){
      sb.append(mem_working>0 ? "WORKING" : "NOT WORKING");
    }else{
      sb.append(mem_working==0 ? "NOT WORKING" : ((mem_working&0x02)==0x02 ? "1=OK" : "1=FAIL")+" "+((mem_working&0x01)==0x01 ? "0=OK" : "0=FAIL"));
    }
  }

  private void rfText(final StringBuilder sb){
    sb.append("\nRF........: ").append(rf_working ? "WORKING" : "NOT WORKING").append(" r_n:").append(rf_rcvd_n).append(" s_n:").append(rf_sent_n);
  }

  private String gprsState(final int s){
    switch(s){
      case 1:
        return "gsTURN_ON";

      case 2:
        return "gsTURN_ON_1";

      case 3:
        return "gsTURN_ON_2";

      case 6:
        return "gsCONNECTINGSTEP";

      case 7:
        return "gsCONNECTANSWER";

      case 50:
        return "gsWORKING";

      case 51:
        return "gsPREPARE_SEND";

      case 52:
        return "gsHEADER_SENDING";

      case 60:
        return "gsRING";

      case 61:
        return "gsRECEIVING";

      case 100:
        return "gsTURN_OFF";

      case 101:
        return "gsTURN_OFF_1";

      case 102:
        return "gsTURN_OFF_2";

      case 103:
        return "gsOFF";

      case 200:
        return "gsREQ_RF";

      case 201:
        return "gsREQ_RF_WAIT_ANSWER";

      case 202:
        return "gsREQ_RF_RECEIVED";

      case 203:
        return "gsREQ_RF_ENDED";

      default:
        return "("+gprs_state+")";

    }
  }

  private void gprsText(final StringBuilder sb){
    sb.append("\nGPRS......: ").append(gprsState(gprs_state));
    if(gprs_state>=6&&gprs_state<=7){
      sb.append(" step=").append(gprs_step);
    }else if(gprs_state>=200){
      sb.append(" LL:").append(gprs_gprs_wait_rf_count);
    }
  }

  private void vText(final StringBuilder sb){
    sb.append("\nv.........: ").append(String.format("%.2f", v_last))
            .append("(").append(String.format("%d", v_dig_last))
            .append(") cag_set=").append(cag_set).append(" mux=").append(cag_mux).append(" pot=").append(cag_pot)
            .append(" afund(").append(afund_count).append(")=").append(afund_h).append(":").append(afund_m).append(" ").append(afund_v);
  }

  public String asText(){
    final StringBuilder sb = new StringBuilder();
    sb.append("program...: ").append(program.getName()).append("/").append(program_mode.getName());
    if(install_step==EInstallStep.GATEWAY){
      sb.append("-Install");
    }
    sb.append("/").append(sequence)
            .append(" cl=").append(client_id).append(" rf=").append(rf_channel).append(':').append(rf_dbm)
            .append(" fw=").append(fw_version).append(" resets=").append(reset_count);
    if(program==EProgram.INSTALLER){
      sb.append("\nnb=").append(install_nb_count).append("/").append(install_nb_other_count).append(" +").append(install_nb_remaining);
      for(int i = 0; i<install_nb_count; i++){ // o lenght de install_nb é o total
        sb.append(" ").append(install_nb[i].section).append(":").append(install_nb[i].sn);
      }
    }else{
      switch(program){
        case P2P_HUB:
        case P2P_PEER:
          sb.append("\n\nSection/id: ").append(sensor_section).append("/").append(sensor_id);
          sb.append("\nInstall...: ").append(install_timer).append(" nb=").append(install_nb_count).append("/").append(install_nb_other_count);
          for(int i = 0; i<install_nb_count; i++){ // o lenght de install_nb é o total
            sb.append(" ").append(install_nb[i].section).append(":").append(install_nb[i].sn);
          }
          sb.append(" + remaining tests: ").append(install_nb_remaining);
          sb.append("\nErrorCount: ").append(sensor_errorcount);
          sb.append("\nMy order..: ").append(my_order).append(" of [");
          for(int i = 0; i<sensor_orderCount; i++){
            if(i>0){
              sb.append(",");
            }
            sb.append(sorder[i]);
          }
          break;

        case MESH_HUB:
        case MESH_PEER:
          break;

      }
      sb.append("\n\nGPS.......: ").append(gps_mode.getName()).append(" ").append(coo.asText());
      switch(program){
        case MESH_HUB:
        case MESH_PEER:
        case P2P_PEER:
          sb.append("\n\nBattery...: ").append(Float.toString(vbat)).append(" ch=").append(vbat_charging);
          sb.append("\ni.........: ").append(String.format("%.2f", i_last)).append(" dir: ").append(direction_i&0x01).append("(").append(direction_i&0x02).append(")");
          vText(sb);
          sb.append("\nfp........: ").append(fp_last);
          sb.append("\nt.........: ").append(t_last).append("°C");
          sb.append("\nPresence..: v=").append((vpresence&0x01)==0x01 ? "T" : "F").append(" i=").append((vpresence&0x02)==0x02 ? "T" : "F");
          break;

        default:
          break;
      }
      sb.append("\n\nRTC.......: ");
      switch(rtc_hour){
        case 0xFF:
          sb.append("NOT WORKING");
          break;

        case 0xFE:
          sb.append("WORKING, NOT UPDATED");
          break;

        default:
          sb.append(rtc_hour).append(":").append(rtc_min);
          if(v>2){
            sb.append(":").append(rtc_sec);
          }
          break;
      }
      rfText(sb);
      flashText(sb);
      gprsText(sb);
    }
    return sb.toString();
  }

  public String calibText(){
    final StringBuilder sb = new StringBuilder();
    sb.append("fw........: ").append(fw_version).append(" cl=").append(client_id).append(" rf=").append(rf_channel).append(':').append(rf_dbm).append(" resets=").append(reset_count);
    sb.append("\ncontador..: ").append(sequence);
    sb.append("\nBattery...: ").append(Float.toString(vbat)).append(" ch=").append(vbat_charging);
    sb.append("\nPresence..: v=").append((vpresence&0x01)==0x01 ? "T" : "F").append(" i=").append((vpresence&0x02)==0x02 ? "T" : "F");
    sb.append("\ni k/dir...: ").append(String.format("%14.12f", sensor_k_i)).append("/").append(direction_i&0x01).append("(").append(direction_i&0x02).append(")");
    sb.append("\ni.........: ").append(String.format("%.4f", i_last)).append("/").append(String.format("%14d", i_dig_last));
    vText(sb);
    sb.append("\nfp........: ").append(fp_last);
    sb.append("\nt.........: ").append(t_last).append("°C");
    sb.append("\nRTC.......: ");
    switch(rtc_hour){
      case 0xFF:
        sb.append("NOT WORKING");
        break;

      case 0xFE:
        sb.append("WORKING, NOT UPDATED");
        break;

      default:
        sb.append(rtc_hour).append(":").append(rtc_min);
        break;
    }
    sb.append("\nGPS.......: ").append(gps_mode.getName()).append(" ").append(coo.asText());
    rfText(sb);
    flashText(sb);
    gprsText(sb);
    return sb.toString();
  }

  public String getLog(){
    final StringBuilder sb = new StringBuilder();
    LocalTime now = LocalTime.now();
    /*
     * hora_local, rtc,
     * serial number,
     * sequencial,
     * gps_mode,
     * cag_set, mux, pot
     * vbat,
     * v_dig,
     * i_dig, i_k,
     * temp,
     * presence, 0x02=i 0x01=v
     * rf_working,
     * mem_working, 0x01 | 0x02
     * afund_hora, magnitude, count,
     * resets,
     * gprs_state, step, ll_count
     * charging
     */
    final String m_vbat = (""+vbat).replace('.', ',');
    String k = (""+sensor_k_i).replace('.', ',');
    //
    sb.append(lib.strzero(now.getHour(), 2)).append(':').append(lib.strzero(now.getMinute(), 2)).append(':').append(lib.strzero(now.getSecond(), 2)).append(';')
            .append(lib.strzero(rtc_hour, 2)).append(':').append(lib.strzero(rtc_min, 2)).append(':').append(lib.strzero(rtc_sec, 2)).append(';')
            .append(sensor_sn).append(';')
            .append(sequence).append(';')
            .append(gps_mode).append(';')
            .append(cag_set).append(';')
            .append(cag_mux).append(';')
            .append(cag_pot).append(';')
            .append(m_vbat).append(';')
            .append(v_dig_last).append(';')
            .append(i_dig_last).append(';')
            .append(k).append(';')
            .append(t_last).append(';')
            .append(vpresence).append(';')
            .append(rf_working).append(';')
            .append(mem_working).append(';')
            .append(lib.strzero(afund_h, 2)).append(':').append(lib.strzero(afund_m, 2)).append(';')
            .append(afund_v).append(';')
            .append(afund_count).append(';')
            .append(reset_count).append(';')
            .append(gprs_state).append(';')
            .append(gprs_step).append(';')
            .append(gprs_gprs_wait_rf_count).append(";")
            .append(vbat_charging)
            .append('\r');
    return sb.toString();
//  final public int v;
//  final public EProgram program;
//  final public EProgramMode program_mode;
//  final public TCoordinates coo;
//  final public int v_offset;
//  final public int i_offset;
//  final public float fp_last;
//  final public int angle_last;
//  final public float i_last;
//  final public int fw_version;
//  final public int direction_i; // v2
//  final public int rf_rcvd_n;
//  final public int rf_sent_n;
//  final public int list_0;
//  final public int list_1;
//  final int client_id;
//  final int rf_channel;
//  final int rf_dbm;
//  final EInstallStep install_step;
  }

  static public class TPackNB {

    final public long sn;
    final public int section;

    public TPackNB(final long sn_, final int section_){
      sn = sn_;
      section = section_;
    }

  }
}
