/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.EUpdateTask;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TINTERFACE_UPDATE_FW extends TSEND {

  final static public int CMD_UPDATE_BUFFER = 210; // maximo no fw
  //
  final public EUpdateTask task; // 1
  final public int version; // 2
  final public int parts; // 2
  final public int part; // 2
  final public byte[] data;
  final public int checksum;

  public TINTERFACE_UPDATE_FW(final EUpdateTask task_,
                              final int version_,
                              final int parts_,
                              final int part_,
                              final byte[] data_,
                              final int checksum_) throws Exception{
    super();
    task = task_;
    version = version_;
    parts = parts_;
    part = part_;
    data = data_;
    checksum = checksum_;
    //
    updateWriter();
  }

  public TINTERFACE_UPDATE_FW(final TPackReader sr) throws Exception{
    task = EUpdateTask.getById(sr.readOne());
    version = sr.readTwo();
    parts = sr.readTwo();
    part = sr.readTwo();
    checksum = sr.readTwo();
    final int len = sr.readOne();
    data = sr.readByteArray(len);
    //
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(task.getId());
    pw.writeTwo(version);
    pw.writeTwo(parts);
    pw.writeTwo(part);
    pw.writeTwo(checksum);
    pw.writeOne(data.length);
    pw.writeByteArray(data, false);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.INTERFACE_UPDATE_FW;
  }

}
