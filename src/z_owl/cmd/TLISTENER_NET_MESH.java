/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import java.time.LocalTime;
import z_owl.EGPSMode;
import z_owl.EProgram;
import z_owl.EProgramMode;
import z_owl.EProtocol;
import owl2_lib.TCoordinates;
import owl2_lib.owl_lib;
import z_owl.TPackReader;
import x.etc.lib;

/**
 *
 * @author TheFlyvio
 *
 *
 */
final public class TLISTENER_NET_MESH extends TSEND {

  static final public int NET_MESH_TX_NOW = 0; // entra na lista
  static final public int NET_MESH_TX_LAZY = 1; // entra na lista
  static final public int NET_MESH_TX_HUB_ACK = 2; // nao entra em lista
  static final public int NET_MESH_TX_HUB = 3; // nao entra em lista
  static final public int NET_MESH_TX_LISTENER = 4; // nao entra em lista
  static final public int NET_MESH_TX_TALKER = 5; // nao entra em lista

  final public int h_v;
  final public int h_sn;
  final public int h_n;
  final public int h_cmd;
  final public int h_crc;
  final public int h_rec; // 'l'
  // 'l'
  public int l_v;
  public int l_sn;
  public int l_seq;
  public EProgram l_program;
  public EProgramMode l_program_mode;
  public EGPSMode l_gps_mode;
  public TCoordinates l_gps_coo;
  public int l_rtc_h;
  public int l_rtc_m;
  public int l_rtc_s;
  public int l_cag_set;
  public int l_mux;
  public int l_pot;
  public int l_vbat;
  public int l_i_dig;
  public int l_v_dig;
  public float l_i_k;
  public int l_t;
  public int l_vpresence;
  public int l_rf_working;
  public int l_mem_working;
  public int l_rcvd_n;
  public int l_sent_n;
  public int l_gprs_state;
  public int l_gprs_step;
  public int l_gprs_wait_rf_count;
  public int l_afund_h;
  public int l_afund_m;
  public int l_afund_v;
  public int l_afund_count;
  public int l_reset_count;
  public int l_charging;
  public int l_fwversion;
  public int l_cag_set_v_dig;
  public int l_fp_dig;
  public int l_direction_i;

  public TLISTENER_NET_MESH(final TPackReader r){
    h_v = r.readOne();
    h_sn = r.readFour();
    h_n = r.readTwo();
    h_cmd = r.readOne(); // NET_MESH_TX_TALKER
    h_crc = r.readTwo();
    h_rec = r.readOne();
    //
    switch(h_rec){
      case 'l':
        l_v = r.readOne();
        // 0x01
        l_sn = r.readFour();
        l_seq = r.readOne();
        l_program = r.readProgram();
        l_program_mode = r.readProgramMode();
        l_gps_mode = r.readGPSMode();
        l_gps_coo = r.readCoordinates();
        l_rtc_h = r.readOne();
        l_rtc_m = r.readOne();
        l_rtc_s = r.readOne();
        l_cag_set = r.readOne();
        l_mux = r.readOne();
        l_pot = r.readOne();
        l_vbat = r.readTwo();
        l_i_dig = r.readFour();
        l_v_dig = r.readFour();
        l_i_k = owl_lib.bits2k(r.readFour());
        l_t = r.readOne()-55;
        l_vpresence = r.readOne();
        l_rf_working = r.readOne();
        l_mem_working = r.readOne();
        l_rcvd_n = r.readTwo();
        l_sent_n = r.readTwo();
        l_gprs_state = r.readOne();
        l_gprs_step = r.readOne();
        l_gprs_wait_rf_count = r.readOne();
        l_afund_h = r.readOne();
        l_afund_m = r.readOne();
        l_afund_v = r.readOne();
        l_afund_count = r.readOne();
        l_reset_count = r.readOne();
        if(l_v>=2){
          l_charging = r.readTwo();
          if(l_v>=3){
            l_fwversion = r.readTwo();
            l_cag_set_v_dig = r.readFour();
            l_fp_dig = r.readTwo();
            if(l_v>=4){
              l_direction_i = r.readOne();
            }
          }
        }
        break;
    }
    //
    try{
      updateWriter();
    }catch(Exception ex){
    }
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.LISTENER_INFO;
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(h_v);
    pw.writeFour(h_sn);
    pw.writeTwo(h_n);
    pw.writeOne(h_cmd);
    pw.writeTwo(h_crc);
    pw.writeOne(h_rec);
    //
    switch(h_rec){
      case 'l':
        pw.writeOne(l_v);
        // 0x01
        pw.writeFour(l_sn);
        pw.writeOne(l_seq);
        pw.writeProgram(l_program);
        pw.writeProgramMode(l_program_mode);
        pw.writeGPSMode(l_gps_mode);
        pw.writeCoordinates(l_gps_coo);
        pw.writeOne(l_rtc_h);
        pw.writeOne(l_rtc_m);
        pw.writeOne(l_rtc_s);
        pw.writeOne(l_cag_set);
        pw.writeOne(l_mux);
        pw.writeOne(l_pot);
        pw.writeTwo(l_vbat);
        pw.writeFour(l_i_dig);
        pw.writeFour(l_v_dig);
        pw.writeFour(owl_lib.k2bits(l_i_k));
        pw.writeOne(l_t+55);
        pw.writeOne(l_vpresence);
        pw.writeOne(l_rf_working);
        pw.writeOne(l_mem_working);
        pw.writeTwo(l_rcvd_n);
        pw.writeTwo(l_sent_n);
        pw.writeOne(l_gprs_state);
        pw.writeOne(l_gprs_step);
        pw.writeOne(l_gprs_wait_rf_count);
        pw.writeOne(l_afund_h);
        pw.writeOne(l_afund_m);
        pw.writeOne(l_afund_v);
        pw.writeOne(l_afund_count);
        pw.writeOne(l_reset_count);
        if(l_v>=2){
          pw.writeTwo(l_v);
          if(l_v>=3){
            pw.writeTwo(l_fwversion);
            pw.writeFour(l_cag_set_v_dig);
            pw.writeTwo(l_fp_dig);
            if(l_v>=4){
              pw.writeOne(l_direction_i);
            }
          }
        }
        break;
    }
  }

  public String asText(){
    final StringBuilder sb = new StringBuilder();
    switch(h_rec){
      case 'l':
        sb.append("sn=").append(l_sn)
                .append(" rec=").append(h_rec)
                .append(" seq=").append(l_seq)
                .append(" prog=").append(l_program.getName())
                .append(" mode=").append(l_program_mode.getName())
                .append(" cag=").append(l_cag_set)
                .append(" mux=").append(l_mux)
                .append(" pot=").append(l_pot)
                .append(" res=").append(l_reset_count)
                .append("\n   rtc=").append(l_rtc_h).append(":").append(l_rtc_m).append(":").append(l_rtc_s)
                .append("[").append(((float)l_vbat/100)).append("/").append(l_charging).append("]")
                .append(" v=").append(l_v_dig)
                .append(" i=").append(l_i_dig*l_i_k)
                .append(" t=").append(l_t)
                .append(" af[").append(l_afund_count).append("/").append(l_afund_h).append(":").append(l_afund_m).append("]=").append(l_afund_v)
                .append(" dir=").append(l_direction_i&0x01).append("(").append(l_direction_i&0x02).append(")")
                .append(" rf=").append(l_rf_working)
                .append(" mem=").append(l_mem_working)
                .append("\n   gprs rf=").append(l_gprs_wait_rf_count).append(" state=").append(l_gprs_state).append(" step=").append(l_gprs_step)
                .append("\n");
        break;
    }
    //pw.writeGPSMode(l_gps_mode);
    //pw.writeCoordinates(l_gps_coo);
    //pw.writeOne(l_vpresence);
    //pw.writeTwo(l_rcvd_n );
    //pw.writeTwo(l_sent_n );
    return sb.toString();
  }

  public String getLog(){
    final StringBuilder sb = new StringBuilder();
    LocalTime now = LocalTime.now();
    /*
     * hora_local, rtc,
     * serial number,
     * sequencial,
     * gps_mode,
     * cag_set, mux, pot
     * vbat,
     * v_dig,
     * i_dig, i_k,
     * temp,
     * presence, 0x02=i 0x01=v
     * rf_working,
     * mem_working, 0x01 | 0x02
     * afund_hora, magnitude, count,
     * resets,
     * gprs_state, step, ll_count
     * charging
     */
    String vbat = (""+((float)l_vbat)/100).replace('.', ',');
    String k = (""+l_i_k).replace('.', ',');
    //
    sb.append(lib.strzero(now.getHour(), 2)).append(':').append(lib.strzero(now.getMinute(), 2)).append(':').append(lib.strzero(now.getSecond(), 2)).append(';')
            .append(lib.strzero(l_rtc_h, 2)).append(':').append(lib.strzero(l_rtc_m, 2)).append(':').append(lib.strzero(l_rtc_s, 2)).append(';')
            .append(l_sn).append(';')
            .append(l_seq).append(';')
            .append(l_gps_mode).append(';')
            .append(l_cag_set).append(';')
            .append(l_mux).append(';')
            .append(l_pot).append(';')
            .append(vbat).append(';')
            .append(l_v_dig).append(';')
            .append(l_i_dig).append(';')
            .append(k).append(';')
            .append(l_t).append(';')
            .append(l_vpresence).append(';')
            .append(l_rf_working).append(';')
            .append(l_mem_working).append(';')
            .append(lib.strzero(l_afund_h, 2)).append(':').append(lib.strzero(l_afund_m, 2)).append(';')
            .append(l_afund_v).append(';')
            .append(l_afund_count).append(';')
            .append(l_reset_count).append(';')
            .append(l_gprs_state).append(';')
            .append(l_gprs_step).append(';')
            .append(l_gprs_wait_rf_count).append(";")
            .append(l_charging)
            .append('\r');
    return sb.toString();
  }

}
