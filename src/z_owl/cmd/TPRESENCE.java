/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TPRESENCE extends TSEND {

  final public long path;
  final public long presence;

  public TPRESENCE(final long path_, final long presence_) throws Exception{
    super();
    path = path_;
    presence = presence_;
    updateWriter();
  }

  public TPRESENCE(final TPackReader sr) throws Exception{
    path = sr.readEight();
    presence = sr.readEight();
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeEight(path);
    pw.writeEight(presence);
  }

  public String asText(){
    return "\npresence path.: "+Long.toString(path, 2)
           +"\npresence state: "+Long.toString(presence, 2);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.PRESENCE;
  }

}
