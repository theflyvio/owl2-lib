/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackWriter;

/**
 *
 * @author TheFlyvio
 */
abstract public class TSEND {

  final protected TPackWriter pw;

  public TSEND(){
    pw = new TPackWriter();
  }

  public byte[] getByteArray(){
    return pw.getByteArray();
  }

  protected void updateWriter() throws Exception{
    pw.writeOne(getProtocol().getId()); // cmd
    updateWriterSpecific();
  }

  abstract public EProtocol getProtocol();

  abstract protected void updateWriterSpecific() throws Exception;

}
