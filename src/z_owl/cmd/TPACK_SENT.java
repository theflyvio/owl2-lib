/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TPACK_SENT extends TSEND {

  final public int check; ///////// 02 checksum ou crc16
  final public int cmd; /////////// 01 comando
  final public int section;//////// 02 secao entre concentradores
  final public int id_order; ////// 01 id de quem esta na ordem
  final public int id_tx; ///////// 01 id do transmissor
  final public int id_origin; ///// 01 id da origem
  final public int dir_n; ///////// 01 numero do pacote da origem
  final public long path; ////////// 08 por onde passou o pacote (primeiro bit setado de acordo com direcao significa origem)

  public TPACK_SENT(final TPackReader r) throws Exception{
    check = r.readTwo();
    cmd = r.readOne();
    section = r.readTwo();
    id_order = r.readOne();
    id_tx = r.readOne();
    id_origin = r.readOne();
    dir_n = r.readOne();
    path = r.readEight();
    updateWriter();
  }

//  public TPACK_SENT(final int section_, final int id_, final TCoordinates coo_) throws Exception{
//    id = id_;
//    section = section_;
//    coo = coo_;
//    //
//    updateWriter();
//  }
//
  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeTwo(check);
    pw.writeOne(cmd);
    pw.writeTwo(section);
    pw.writeOne(id_order);
    pw.writeOne(id_tx);
    pw.writeOne(id_origin);
    pw.writeOne(dir_n);
    pw.writeEight(path);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.HUB_IDENTIFICATION;
  }

}
