/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSET_SECTION extends TSEND {

  final public int section;

  public TSET_SECTION(final int section_) throws Exception{
    super();
    section = section_;
    updateWriter();
  }

  public TSET_SECTION(final TPackReader sr) throws Exception{
    section = sr.readTwo();
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeTwo(section);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SET_SECTION;
  }

}
