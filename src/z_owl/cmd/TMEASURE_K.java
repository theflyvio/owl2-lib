/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import owl2_lib.owl_lib;
import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class TMEASURE_K extends TSEND {

  final private boolean _do_k;
  final private float _k;
  final private boolean _do_offset;
  final private int _offset;
  final private int _memtest;

  public TMEASURE_K(final boolean do_k_, final float k, final boolean do_offset_, final int offset_, final int memtest_) throws Exception{
    super();
    _do_k = do_k_;
    _k = k;
    _do_offset = do_offset_;
    _offset = offset_;
    _memtest = memtest_;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeBoolean(_do_k);
    pw.writeFour(owl_lib.k2bits(_k));
    pw.writeBoolean(_do_offset);
    pw.writeFour(_offset);
    pw.writeTwo(_memtest);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.MEASURE_K;
  }

}
