/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class TSEND_SECTION extends TSEND {

  final private long _sn;
  final private int _section;

  public TSEND_SECTION(final long sn, final int section) throws Exception{
    super();
    _sn = sn;
    _section = section;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeFour((int)_sn);
    pw.writeTwo(_section);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SEND_SECTION;
  }
}
