/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TUPDATE_MISSING extends TSEND {

  final public int task;
  final public int version; ///////   2
  final public int parts; /////////   2 total de partes
  final public int part; /////////   2 total de partes
  final public byte[] b;

  public TUPDATE_MISSING(final TPackReader r) throws Exception{
    task = r.readOne();
    version = r.readTwo();
    parts = r.readTwo();
    part = r.readTwo();
    r.readTwo(); // checksum
    b = r.readByteArray(r.readOne());
    //
    updateWriter();
  }

  public TUPDATE_MISSING(final int task_, final int version_, final int parts_, final int part_, final byte[] b_) throws Exception{
    task = task_;
    version = version_;
    parts = parts_;
    part = part_;
    b = b_;
    //
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(task);
    pw.writeTwo(version);
    pw.writeTwo(parts);
    pw.writeTwo(part);
    pw.writeTwo(0); // checksum
    pw.writeByteArray(b, true);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.UPDATE_MISSING;
  }

}
