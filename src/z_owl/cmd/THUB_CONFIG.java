/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class THUB_CONFIG extends TSEND {

  final private int _section;
  final private int _id;

  public THUB_CONFIG(final int section, final int id) throws Exception{
    super();
    _section = section;
    _id = id;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeTwo(_section);
    pw.writeOne(_id);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.HUB_CONFIG;
  }

}
