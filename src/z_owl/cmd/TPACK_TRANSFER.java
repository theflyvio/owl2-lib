/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import owl2_lib.TCoordinates;
import owl2_lib.owl_lib;
import z_owl.TPackReader;
import z_owl.TPackWriter;

/**
 *
 * @author TheFlyvio
 */
final public class TPACK_TRANSFER extends TSEND {

  /*
   * listener
   * dir
   * 'r' RX
   * 't' TX
   * 'o' RX-TIMEOUT
   * 'e' CRC ERROR
   */
  final public int dir;
  final public TFWPack pack;

  public TPACK_TRANSFER(final TPackReader r) throws Exception{
    dir = r.readOne();
    switch(dir){
      case 'r':
      case 't':
        pack = new TFWPack();
        pack.read(r);
        break;

      default:
        pack = null;
    }
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(dir);
    if(pack!=null){
      pack.write(pw);
    }
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.PACK_TRANSFER;
  }

  public String asText(){
    final StringBuilder sb = new StringBuilder();
    switch(dir){
      case 'r':
        sb.append("R ").append(pack.asText());
        break;

      case 't':
        sb.append("T ").append(pack.asText());
        break;

      case 'o':
        sb.append("TIMEOUT");
        break;

      case 'e':
        sb.append("CRC ERROR");
        break;
    }
    return sb.toString();
  }

  static public class TFWPack {

    /*
     * header
     */
    public int check; ///////// 02 checksum ou crc16
    public int cmd; /////////// 01 comando
    // comando por id
    public int section; /////// 14
    public int id_order; ////// 01 id de quem esta na ordem
    public int id_tx; ///////// 01 id do transmissor
    public int id_origin; ///// 01 id da origem
    public int dir_n; ///////// 01 numero do pacote da origem
    public long path; ///////// 08 por onde passou o pacote (primeiro bit setado de acordo com direcao significa origem)
    // comando por sn
    public int sn; // 4+10
    //
    public TFWPack_Task task;

    public void read(final TPackReader r){
      check = r.readTwo();
      cmd = r.readOne();
      int cmd_task = cmd&0xF0;
      int cmd_subtask = cmd&0x0F;
      if(cmd_task==0x00){
        sn = r.readFour();
        r.readByteArray(10);
      }else{
        section = r.readTwo();
        id_order = r.readOne();
        id_tx = r.readOne();
        id_origin = r.readOne();
        dir_n = r.readOne();
        path = r.readEight();
      }
      switch(cmd_task){
        case 0x00: // CMD_BY_SN
          task = new TFWPack_B();
          task.read(r, cmd_subtask);
          break;

        case 0x10: // CMD_UPDATE
          task = new TFWPack_U();
          task.read(r, cmd_subtask);
          break;

        case 0x20:
          task = new TFWPack_C();
          task.read(r, cmd_subtask);
          break;

        case 0x30:
          task = new TFWPack_M();
          task.read(r, cmd_subtask);
          break;

        case 0x40:
          task = new TFWPack_P();
          task.read(r, cmd_subtask);
          break;

        case 0x50:
          task = new TFWPack_X();
          task.read(r, cmd_subtask);
          break;

        case 0x60:
          task = new TFWPack_Q();
          task.read(r, cmd_subtask);
          break;

        case 0x70:
          task = new TFWPack_S();
          task.read(r, cmd_subtask);
          break;

        case 0x80:
          task = new TFWPack_D();
          task.read(r, cmd_subtask);
          break;

        default:
          task = null;
      }

    }

    private void write(final TPackWriter pw) throws Exception{
      pw.writeTwo(check);
      pw.writeOne(cmd);
      if((cmd&0xF0)==0x00){
        pw.writeFour(sn);
        pw.writeEight(0);
        pw.writeTwo(0);
      }else{
        pw.writeTwo(section);
        pw.writeOne(id_order);
        pw.writeOne(id_tx);
        pw.writeOne(id_origin);
        pw.writeOne(dir_n);
        pw.writeEight(path);
      }
      if(task!=null){
        task.write(pw);
      }
    }

    public String cmdName(final int cmd){
      switch(cmd&0xF0){
        case 0x00:
          return "BSN";

        case 0x10:
          return "UPD";

        case 0x20:
          return "COL";

        case 0x30:
          return "MNG";

        case 0x40:
          return "PRE";

        case 0x50:
          return "STK";

        case 0x60:
          return "REQ";

        case 0x70:
          return "SET";

        case 0x80:
          return "DIR";

      }
      return "???";
    }

    public String asText(){
      final StringBuilder sb = new StringBuilder();
      if(task==null){
        sb.append("INV 0x").append(Integer.toHexString(cmd));
      }else{
        sb.append(cmdName(cmd));
        //
        if((cmd&0xF0)==0){
          sb.append(" sn=").append(sn);
        }else{
          boolean stuck = (dir_n&0x20)>0;
          sb.append(" id=").append(String.format("%02d", section))
                  .append(":").append(id_order)
                  .append("/").append(id_tx)
                  .append("/").append(id_origin)
                  .append((dir_n&0x40)>0 ? '<' : '-')
                  .append((dir_n&0x80)>0 ? '>' : '-')
                  .append(stuck ? '*' : ' ');
        }
        sb.append(task.asText());
        if(path!=0){
          sb.append(' ').append(owl_lib.getIdsOfPath(path));
        }
      }
      return sb.toString();
    }

    /*
     *
     * sub-pacotes
     *
     */
    static abstract public class TFWPack_Task {

      abstract protected void read(final TPackReader r, final int cmd_subtask);

      abstract public String asText();

      abstract protected void write(final TPackWriter pw) throws Exception;

      abstract public int getN();

      abstract public int getC_Task();

      abstract public int getC_SubTask();

    }

    /*
     *
     * 0x0x CMD_BY_SN
     *
     */
    static public class TFWPack_B extends TFWPack_Task {

      public int subtask;
      // 0x0 PACKB_INFO
      public int info_fw_version; ////// 02 fw_version 69+
      public int info_section; ///////// 02 secao configurada
      public int info_sn; ///////////// 04 sn de quem enviou
      public int info_neighbours; ///// 01 numero de vizinhos encontrados
      public int info_o_neighbours; /// 01 numero de vizinhos encontrados em outra secao
      public int info_try_neighbours; // 01 quantas tentavias faltam
      public int info_v; ////////////// 01 tensao detectada
      public float info_geti; /////////// 04 corrente da rede
      public int info_getv; /////////// 04 tensao da rede 69+
      public float info_vbat; /////////// 04 bateria
      public int info_install_count; // 01 minutos desde a entrada em install
      public int info_rtc_hours; ////// 01 hora no rtc
      public int info_rtc_minutes; //// 01 minutos no rtc
      public TCoordinates info_coo;
      public boolean direction_i; // fw 74+
      // 0x1 cfgsection
      public int cfgsection_section; //////// 01 secao configurada
      // 0x2 cfgmode
      public int cfgmode_mode; /////////// 01 modo 0=factory 1=install

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
        switch(subtask){
          case 0x0:
            info_fw_version = r.readTwo(); // 70+
            info_section = r.readTwo();
            info_sn = r.readFour();
            info_neighbours = r.readOne();
            info_o_neighbours = r.readOne();
            info_try_neighbours = r.readOne();
            info_v = r.readOne();
            info_geti = r.readFloat4();
            info_getv = r.readFour(); // 70+
            info_vbat = r.readFloat4();
            info_install_count = r.readOne();
            info_rtc_hours = r.readOne();
            info_rtc_minutes = r.readOne();
            info_coo = r.readCoordinates();
            direction_i = info_fw_version>=74 ? r.readBoolean() : false;
            break;

          case 0x1:
            cfgsection_section = r.readOne();
            break;

          case 0x2:
            cfgmode_mode = r.readOne();
            break;

        }
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        switch(subtask){
          case 0x0:
            w.writeTwo(info_fw_version); // 70+
            w.writeTwo(info_section);
            w.writeFour(info_sn);
            w.writeOne(info_neighbours);
            w.writeOne(info_o_neighbours);
            w.writeOne(info_try_neighbours);
            w.writeOne(info_v);
            w.writeFloat4(info_geti);
            w.writeFour(info_getv); // 70+
            w.writeFloat4(info_vbat);
            w.writeOne(info_install_count);
            w.writeOne(info_rtc_hours);
            w.writeOne(info_rtc_minutes);
            w.writeCoordinates(info_coo);
            if(info_fw_version>=74){
              w.writeBoolean(direction_i);
            }
            break;

          case 0x1:
            w.writeOne(cfgsection_section);
            break;

          case 0x2:
            w.writeOne(cfgmode_mode);
            break;

        }
      }

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        switch(subtask){
          case 0x0:
            sb.append('\n');
            sb.append("             Instalação: ").append(info_install_count).append(" fw=").append(info_fw_version).append("\n");
            sb.append("             Section...: ").append(info_section).append("\n");
            sb.append("             sn........: ").append(info_sn).append("\n");
            sb.append("             Vizinhos..: ").append(info_neighbours).append("/").append(info_o_neighbours).append(" +tentativas: ").append(info_try_neighbours).append("\n");
            sb.append("             Tensão?...: ").append(info_v>0 ? "SIM" : "NÃO").append(" dig=").append(info_getv).append("\n");
            sb.append("             Rede......: ").append(info_geti).append(" dir=").append(direction_i ? "neg" : "pos").append("\n");
            sb.append("             Bateria...: ").append(info_vbat).append("\n");
            sb.append("             RTC.......: ");
            switch(info_rtc_hours){
              case 0xFF:
                sb.append("NOT WORKING");
                break;

              case 0xFE:
                sb.append("WORKING, NOT UPDATED");
                break;

              default:
                sb.append(info_rtc_hours).append(":").append(info_rtc_minutes);
                break;
            }
            sb.append("\n");
            sb.append("             GPS.......: ").append(info_coo.latitude).append("/").append(info_coo.longitude);
            break;

          case 0x1:
            sb.append(" cfg_section: ").append(cfgsection_section);
            break;

          case 0x2:
            sb.append(" cfg_mode: ").append(cfgmode_mode);
            break;

          case 0xF:
            sb.append(" release");
            break;

        }
        return sb.toString();
      }

    }

    /*
     *
     * 0x1x CMD_UPDATE
     *
     */
    static public class TFWPack_U extends TFWPack_Task {

      public int subtask;
      //
      public int version; ///////   2
      public int parts; /////////   2 total de partes
      // p = parte
      public int p_part; //////   2
      public int p_len; // 1
      //public byte[] p_b; // [CMD_UPDATE_BUFFER]; /// 220
      // check
      public int check_len;
      public byte[] check_b; //[75]; // para 128k = 596 partes (bits)

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
        version = r.readTwo();
        parts = r.readTwo();
        switch(subtask){
          case 0x0:
          case 0x1:
            p_part = r.readTwo();
            p_len = r.readOne();
            //p_b = r.readByteArray(220);
            break;

          case 0x2:
          case 0x3:
            check_len = r.readOne();
            check_b = r.readByteArray(75);
            break;

        }
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        w.writeTwo(version);
        w.writeTwo(parts);
        switch(subtask){
          case 0x0:
          case 0x1:
            w.writeTwo(p_part);
            w.writeOne(p_len);
            //w.writeByteArray(p_b, false);
            break;

          case 0x2:
          case 0x3:
            w.writeOne(check_len);
            w.writeByteArray(check_b, false);
            break;

        }
      }

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        sb.append("fw=").append(version).append(" pts=").append(parts);
        switch(subtask){
          case 0x0:
          case 0x1:
            sb.append(" pt=").append(p_part).append(" len=").append(p_len);
            break;

          case 0x2:
          case 0x3:
            sb.append("chk [");
            int p = 0;
            boolean any = false;
            while(p<parts){
              int pbyte = p/8;
              int pbit = p%8;
              if((check_b[pbyte]&(((long)1)<<pbit))==0){
                if(any){
                  sb.append(',');
                }else{
                  any = true;
                }
                sb.append(p+1);
              }
              p++;
            }
            sb.append(']');
            break;

          case 0xF:
            sb.append("release");
            break;

        }
        return sb.toString();
      }

    }

    /*
     *
     * 0x2x CMD_COLLECT
     *
     */
    static public class TFWPack_C extends TFWPack_Task {

      static public int N_SENSOR = 62;
      public int cmd_subtask;
      public int subtask;
      public int n;
      public int par1;
      public int par2;
      public int par3;
      //
      //public byte[] data;

      @Override
      protected void read(final TPackReader r, final int cmd_subtask_){
        cmd_subtask = cmd_subtask_;
        subtask = r.readOne();
        n = r.readOne();
        par1 = r.readOne();
        par2 = r.readOne();
        par3 = r.readOne();
        //data = r.readByteArray(233);
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        w.writeOne(subtask);
        w.writeOne(n);
        w.writeOne(par1);
        w.writeOne(par2);
        w.writeOne(par3);
        //w.writeByteArray(data, false);
      }

      @Override
      public int getN(){
        return n;
      }

      @Override
      public int getC_Task(){
        return cmd_subtask;
      }

      @Override
      public int getC_SubTask(){
        return subtask;
      }

      public String subtaskName(){
        switch(cmd_subtask){
          case 1:
            return "get version id="
                   +par3
                   +((subtask&0x80)>0 ? " resetGPS" : "")
                   +((subtask&0x40)>0 ? " restarMeasures" : "");

          case 2:
            return "get vbat+viz+rtc";

          case 3:
            return "get irms "+subtask;

          case 4:
            return "get fp "+subtask;

          case 5:
            return "get temp "+subtask;

          case 6:
            return "get vrms "+subtask;

          case 0xA:
            switch(subtask){
              case 1:
                return "set k="+(((par1<<16)|(par2<<8)|par3)/1E12f);

              case 2:
                return "set rtc "+par1+":"+par2+" id="+par3;

              case 3:
                return "reset "+par3;

              case 4:
                return "reqid id="+par3;

              case 5:
                return "set curto_c="+par1+" id="+par3;

              case 6:
                return "set i_mult="+par1+" id="+par3;

              case 7:
                return "set direction_iLoops="+par1+" id="+par3;

              case 8:
                return "set only_hub1="+par1;

            }
            return "set ?";

          case 0xB:
            switch(subtask){
              case 1:
                return "get k";

              case 2:
                return "get presence";

              case 3:
                return "get vrms";

              case 4:
                return "tests";

              case 5:
                return "get pres/afund";

              case 6:
                return "get install_v";

              case 7:
                return "get install_v_dig";

              case 8:
                if((par1&0x80)==0x80){
                  int v = (((par1&0x7F)<<8)|par2)<<8;
                  return "get/set v_check="+v+" id="+par3;
                }
                return "get v_check";

              case 9:
                return "get_sync_vrms";

              case 10:
                return "get_sync_irms";

              case 11:
                return "get_sync_fp";

              case 13:
                return "get direction_i";

              case 14:
                return "get/set CAG="+par1+" id="+par3;

              case 15:
                return "get/set i_min="+(par1/100f);

              case 16:
                return "get/set ade_offset";
            }
            return "get ?";
        }
        return "unknown";
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        sb.append("task=").append(cmd_subtask).append(" n.").append(n).append(' ').append(subtaskName());
        return sb.toString();
      }

    }

    /*
     *
     * 0x3x CMD_MANAGEMENT
     *
     */
    static public class TFWPack_M extends TFWPack_Task {

      public int subtask;
      //
      public long affected;
      //
      public int nb4_count;
      public int[] nb4_sn;
      public int[] nb4_section;
      //
      public int nb5_sn;
      public int nb5_section;

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
        affected = r.readEight();
        switch(subtask){
          case 0x4:
            nb4_count = r.readOne();
            nb4_sn = new int[nb4_count];
            nb4_section = new int[nb4_count];
            for(int i = 0; i<nb4_count; i++){
              nb4_sn[i] = r.readFour();
              nb4_section[i] = r.readTwo();
            }
            break;

          case 0x5:
            nb5_sn = r.readFour();
            nb5_section = r.readTwo();
            break;

        }
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        w.writeEight(affected);
        switch(subtask){
          case 0x4:
            w.writeOne(nb4_count);
            for(int i = 0; i<nb4_count; i++){
              w.writeFour(nb4_sn[i]);
              w.writeTwo(nb4_section[i]);
            }
            break;

          case 0x5:
            w.writeFour(nb5_sn);
            w.writeTwo(nb5_section);
            break;

        }
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        sb.append("Mngm ");
        switch(subtask){
          case 0x2:
            sb.append("ping");
            break;

          case 0x3:
            sb.append("pong");
            break;

          case 0x4:
            sb.append("nb check ").append("[");
            for(int i = 0; i<nb4_sn.length; i++){
              if(i>0){
                sb.append(',');
              }
              sb.append(nb4_sn[i]);
            }
            sb.append("]");
            break;

          case 0x5:
            sb.append("nb answer ").append(" sn=").append(nb5_sn);
            break;

        }
        return sb.toString();
      }

    }

    /*
     *
     * 0x4x CMD_PRESENCE
     *
     */
    static public class TFWPack_P extends TFWPack_Task {

      public int subtask;
      //
      public long presence_old;
      public int rtc_h;
      public int rtc_m;
      //public boolean[] presence;
      //public int[] afund;

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
        switch(subtask){
          case 0x00:
            presence_old = r.readEight();
            break;

          case 0x01:
            rtc_h = r.readOne();
            rtc_m = r.readOne();
            break;

          case 0x02:
            rtc_h = r.readOne();
            rtc_m = r.readOne();
            break;

        }
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        switch(subtask){
          case 0x00:
            w.writeEight(presence_old);
            break;

          case 0x01:
            w.writeOne(rtc_h);
            w.writeOne(rtc_m);
            break;

          case 0x02:
            w.writeOne(rtc_h);
            w.writeOne(rtc_m);
            break;
        }
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        switch(subtask){
          case 0x00:
            sb.append("presence ").append(owl_lib.getIdsOfPath(presence_old));
            break;

          case 0x01:
            sb.append("presence_afund ").append(String.format("%02d:%02d", rtc_h, rtc_m));
            break;

          case 0x02:
            sb.append("pres_v_i_afund ").append(String.format("%02d:%02d", rtc_h, rtc_m));
            break;

        }
        return sb.toString();
      }

    }

    /*
     *
     * 0x5x CMD_STUCK
     *
     */
    static public class TFWPack_X extends TFWPack_Task {

      public int subtask;
      //

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        sb.append("stuck");
        return sb.toString();
      }

    }

    /*
     *
     * 0x6x CMD_REQID
     *
     */
    static public class TFWPack_Q extends TFWPack_Task {

      public int sn;
      public TCoordinates coo;
      public int subtask;
      public int k;
      //

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
        sn = r.readFour();
        coo = r.readCoordinates();
        k = r.readFour();
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        w.writeFour(sn);
        w.writeCoordinates(coo);
        w.writeFour(k);
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        sb.append("reqid sn=").append(sn).append(" coo=").append(coo.asText());
        return sb.toString();
      }

    }

    /*
     *
     * 0x7x CMD_SETID
     *
     */
    static public class TFWPack_S extends TFWPack_Task {

      public int subtask;
      //
      public int sn;
      public int id;
      public int orderCount;
      public byte[] order;
      //
      public int operation; // 0=set id 1=set k

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
        sn = r.readFour();
        id = r.readOne();
        orderCount = r.readOne();
        order = r.readByteArray(orderCount);
        if((sn&80000000)>0){
          if((sn&0xF0000000)>0){
            operation = 1;
          }else{
            operation = 2;
          }
        }else{
          operation = 0;
        }
      }

      public float getK(){
        return (sn&0x0FFFFFFF)/10000.0f;
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        w.writeFour(sn);
        w.writeOne(id);
        w.writeOne(orderCount);
        w.writeByteArray(order, false);
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        switch(operation){
          case 0:
            sb.append("setid sn=").append(sn).append(" id=").append(id);
            break;

          case 1:
            sb.append("setk id=").append(id).append(" k=").append(getK());
            break;
        }
        sb.append(" setorder [");
        for(int i = 0; i<order.length; i++){
          if(i>0){
            sb.append(',');
          }
          sb.append(order[i]);
        }
        sb.append(']');
        return sb.toString();
      }

    }

    /*
     *
     * 0x8x CMD_DIRECTION_I
     *
     */
    static public class TFWPack_D extends TFWPack_Task {

      public int subtask;
      //
      public int rtc_h;
      public int rtc_m;
      public long direction_i;

      @Override
      public int getN(){
        return 0;
      }

      @Override
      public int getC_Task(){
        return 0;
      }

      @Override
      public int getC_SubTask(){
        return 0;
      }

      @Override
      protected void read(final TPackReader r, final int cmd_subtask){
        subtask = cmd_subtask;
        switch(subtask){
          case 0x00:
            rtc_h = r.readOne();
            rtc_m = r.readOne();
            direction_i = r.readEight();
            break;
        }
      }

      @Override
      protected void write(final TPackWriter w) throws Exception{
        switch(subtask){
          case 0x00:
            w.writeOne(rtc_h);
            w.writeOne(rtc_m);
            w.writeEight(direction_i);
            break;
        }
      }

      @Override
      public String asText(){
        final StringBuilder sb = new StringBuilder();
        switch(subtask){
          case 0x00:
            sb.append("direction_i ").append(String.format("%02d:%02d %s", rtc_h, rtc_m, owl_lib.getIdsOfPath(direction_i)));
            break;
        }
        return sb.toString();
      }

    }

  }
}
