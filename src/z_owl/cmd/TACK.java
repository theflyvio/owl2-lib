/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TACK extends TSEND {

  final public EProtocol cmd;

  public TACK(final EProtocol cmd_) throws Exception{
    super();
    cmd = cmd_;
    updateWriter();
  }

  public TACK(final TPackReader sr) throws Exception{
    cmd = EProtocol.getById(sr.readOne());
    updateWriter();
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.ACK;
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(cmd.getId());
  }

  public String asText(){
    return "\nack "+cmd.getName();
  }
}
