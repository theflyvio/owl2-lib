/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class TSEND_RELEASE extends TSEND {

  final private long _sn;
  final private int _cmd; // 0x01=coo 0x02=coo+time 0x04=setar CAG
  final private long _ullat;
  final private long _ullon;
  final private int _v;
  final private int _utc_hour;
  final private int _utc_min;
  final private int _utc_sec;

  public TSEND_RELEASE(final long sn,
                       final int cmd,
                       final long ullat,
                       final long ullon,
                       final int v,
                       final int utc_hour,
                       final int utc_min,
                       final int utc_sec) throws Exception{
    super();
    _sn = sn;
    _cmd = cmd;
    _ullat = ullat;
    _ullon = ullon;
    _v = v;
    _utc_hour = utc_hour;
    _utc_min = utc_min;
    _utc_sec = utc_sec;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeFour((int)_sn);
    pw.writeOne(_cmd);
    pw.writeFour((int)_ullat);
    pw.writeFour((int)_ullon);
    pw.writeTwo(_v);
    pw.writeOne(_utc_hour);
    pw.writeOne(_utc_min);
    pw.writeOne(_utc_sec);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SEND_RELEASE;
  }
}
