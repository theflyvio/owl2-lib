/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;

/**
 *
 * @author TheFlyvio
 */
final public class TGIGA_CONFIG extends TSEND {

  final private int client_id; // 2
  final private String gprs_apn; // 150
  final private String gprs_user; // user+password 40
  final private String gprs_password; //
  final private String gprs_ip; // 16
  final private int gprs_port; // 2
  final private int rf_channel; // 1
  final private int rf_dbm; // 1

  public TGIGA_CONFIG(final int client_id_,
                      final String gprs_apn_,
                      final String gprs_user_,
                      final String gprs_password_,
                      final String gprs_ip_,
                      final int gprs_port_,
                      final int rf_channel_,
                      final int rf_dbm_) throws Exception{
    super();
    client_id = client_id_;
    gprs_apn = gprs_apn_;
    gprs_user = gprs_user_;
    gprs_password = gprs_password_;
    gprs_ip = gprs_ip_;
    gprs_port = gprs_port_;
    rf_channel = rf_channel_;
    rf_dbm = rf_dbm_;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeTwo(client_id);
    pw.writeString(gprs_apn,150);
    pw.writeString(gprs_user+","+gprs_password, 40);
    pw.writeString(gprs_ip, 16);
    pw.writeTwo(gprs_port);
    pw.writeOne(rf_channel);
    pw.writeOne(rf_dbm);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.GIGA_CONFIG;
  }

}
