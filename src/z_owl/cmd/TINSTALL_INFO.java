/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import owl2_lib.TCoordinates;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TINSTALL_INFO {

  final public int NB_COUNT = 16;

  final public int fw_version; ////// 02 versao fw 70+
  final public int section; ///////// 02 secao configurada
  final public long sn;////////////// 04 sn de quem enviou
  final public int neighbours; ////// 01 numero de vizinhos encontrados
  final public int oneighbours; ///// 01 numero de vizinhos encontrados em outra secao
  final public int try_neighbours; // 01 tentativas faltando
  final public int v; /////////////// 01 tensao detectada
  final public float geti; /////////// 04 corrente da rede
  final public int getv; ///////////// 04 tensao da rede 70+
  final public float vbat; /////////// 04 bateria
  final public int install_count; /// 01 minutos desde a entrada em install
  final public int rtc_hours; /////// 01 hora no rtc
  final public int rtc_minutes; ///// 01 minutos no rtc
  final public TCoordinates coo;
  final public boolean direction_i; // fw 74+
  //
  final public TINSTALL_INFO_NB[] nb;
  final public int[] snr;

  public TINSTALL_INFO(final TPackReader r){
    fw_version = r.readTwo(); // 70+
    section = r.readTwo();
    sn = r.readFour();
    neighbours = r.readOne();
    oneighbours = r.readOne();
    try_neighbours = r.readOne();
    v = r.readOne();
    geti = r.readFloat4();
    getv = r.readFour(); // 70+
    vbat = r.readFloat4();
    install_count = r.readOne();
    rtc_hours = r.readOne();
    rtc_minutes = r.readOne();
    coo = r.readCoordinates();
    direction_i = fw_version>=74 ? r.readBoolean() : false; // fw 74+
    //
    nb = new TINSTALL_INFO_NB[NB_COUNT];
    for(int i = 0; i<NB_COUNT; i++){
      nb[i] = new TINSTALL_INFO_NB(r);
    }
    snr = new int[NB_COUNT];
    for(int i = 0; i<NB_COUNT; i++){
      snr[i] = r.readOne();
    }
  }

  public String asText(){
    final StringBuilder sb = new StringBuilder();
    sb.append("Instalação: ").append(install_count).append(" fw=").append(fw_version).append("\n")
            .append("Section:sn: ").append(section).append(":").append(sn).append("\n")
            .append("Tensão?...: ").append(v>0 ? "SIM" : "NÃO").append(" dig=").append(getv).append("\n")
            .append("Rede......: ").append(geti).append(" dir=").append(fw_version>=74 ? direction_i ? "neg" : "pos" : "?").append("\n")
            .append("Bateria...: ").append(vbat).append("\n")
            .append("RTC.......: ");
    switch(rtc_hours){
      case 0xFF:
        sb.append("NOT WORKING");
        break;

      case 0xFE:
        sb.append("WORKING, NOT UPDATED");
        break;

      default:
        sb.append(rtc_hours).append(":").append(rtc_minutes);
        break;
    }
    sb.append("\n");
    sb.append("GPS.......: ").append(coo.latitude).append("/").append(coo.longitude);
    sb.append("\n");
    sb.append("Vizinhos..: ").append(neighbours-oneighbours).append("/").append(neighbours).append(" +tentativas: ").append(try_neighbours).append("\n");
    for(int i = 0; i<neighbours; i++){
      sb.append("   ")
              .append("[").append(String.format("%3d", snr[i])).append("]")
              .append(String.format("%06d:%09d", nb[i].section, nb[i].sn));
      if((i+1)%4==0){
        sb.append("\n");
      }
    }

    return sb.toString();
  }

  static public class TINSTALL_INFO_NB {

    long sn; ////// 4
    int section; // 2

    public TINSTALL_INFO_NB(final TPackReader r){
      sn = r.readFour();
      section = r.readTwo();
    }
  }
}
