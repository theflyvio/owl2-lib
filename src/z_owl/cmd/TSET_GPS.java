/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import owl2_lib.TCoordinates;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSET_GPS extends TSEND {

  final public TCoordinates coo;

  public TSET_GPS(final TPackReader r) throws Exception{
    coo = r.readCoordinates();
    updateWriter();
  }

  public TSET_GPS(final TCoordinates coo_) throws Exception{
    coo = coo_;
    //
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeCoordinates(coo);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SET_GPS;
  }

}
