/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProgram;
import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TLISTENER_INFO extends TSEND {

  final public int v;
  final public int sequence;
  final public EProgram program;
  final public int fwversion;
  final public boolean listener_on;
  final public int talker_sn;
  final public int rf_channel;
  final public int rf_dbm;
  final public int client_id;

  public TLISTENER_INFO(
          int v_,
          int sequence_,
          EProgram program_,
          final int fwversion_,
          final boolean listener_on_,
          final int talker_sn_,
          final int rf_channel_,
          final int rf_dbm_,
          final int client_id_){
    v = v_;
    sequence = sequence_;
    program = program_;
    fwversion = fwversion_;
    listener_on = listener_on_;
    talker_sn = talker_sn_;
    rf_channel = rf_channel_;
    rf_dbm = rf_dbm_;
    client_id = client_id_;
    try{
      updateWriter();
    }catch(Exception ex){
    }
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.LISTENER_INFO;
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(v);
    pw.writeOne(sequence);
    pw.writeProgram(program);
    pw.writeTwo(fwversion);
    pw.writeBoolean(listener_on);
    pw.writeFour(talker_sn);
    pw.writeOne(rf_channel);
    pw.writeOne(rf_dbm);
    pw.writeTwo(client_id);
  }

  public TLISTENER_INFO(final TPackReader r){
    v = r.readOne();
    sequence = r.readOne();
    program = r.readProgram();
    fwversion = r.readTwo();
    listener_on = r.readBoolean();
    talker_sn = r.readFour();
    rf_channel = v>=2 ? r.readOne() : 25;
    rf_dbm = v>=2 ? r.readOne() : 3;
    client_id = v>=2 ? r.readTwo(): 1;
    try{
      updateWriter();
    }catch(Exception ex){
    }
  }

  public String asText(){
    final StringBuilder sb = new StringBuilder();
    sb.append(program.getName())
            .append(":")
            .append(listener_on ? "ON" : "OFF")
            .append(" seq=").append(sequence)
            .append(" fw=").append(fwversion)
            .append(" cl=").append(client_id)
            .append(" rf=").append(rf_channel).append(":").append(rf_dbm)
            .append(" talker=").append(talker_sn);
    return sb.toString();
  }

}
