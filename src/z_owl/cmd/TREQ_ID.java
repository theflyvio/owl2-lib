/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import owl2_lib.TCoordinates;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TREQ_ID extends TSEND {

  final public int sn;
  final public TCoordinates coo;
  final public float k;

  public TREQ_ID(final TPackReader r) throws Exception{
    sn = r.readFour();
    coo = r.readCoordinates();
    k = ((float)r.readFour())/1E12f;
    updateWriter();
  }

  public TREQ_ID(final int sn_, final TCoordinates coo_, final float k_) throws Exception{
    sn = sn_;
    coo = coo_;
    k = k_;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeFour(sn);
    pw.writeCoordinates(coo);
    pw.writeFour((int)(k*1E12f));
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.REQ_ID;
  }

}
