/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSET_PRESENCE extends TSEND {

  final public boolean presence;

  public TSET_PRESENCE(final boolean presence_) throws Exception{
    super();
    presence = presence_;
    updateWriter();
  }

  public TSET_PRESENCE(final TPackReader sr) throws Exception{
    presence = sr.readOne()>0;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(presence ? 1 : 0);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SET_PRESENCE;
  }
}
