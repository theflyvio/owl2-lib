/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSET_RTC extends TSEND {

  final public int h;
  final public int m;
  final public int s;

  public TSET_RTC(final TPackReader r) throws Exception{
    h = r.readOne();
    m = r.readOne();
    s = r.readOne();
    updateWriter();
  }

  public TSET_RTC(final int h_, final int m_, final int s_) throws Exception{
    h = h_;
    m = m_;
    s = s_;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeOne(h);
    pw.writeOne(m);
    pw.writeOne(s);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SET_RTC;
  }

}
