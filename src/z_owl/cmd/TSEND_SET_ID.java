/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSEND_SET_ID extends TSEND {

  final public int sn; // 4
  final public int id; // 1
  final public byte[] order; // 1
  final public boolean propagate; // indica ao hub se é pra propagar o SET_ID ou apenas se ajustar

  public TSEND_SET_ID(final int sn_, final int id_, final byte[] order_, final boolean propagate_) throws Exception{
    super();
    sn = sn_;
    id = id_;
    order = order_;
    propagate = propagate_;
    //
    updateWriter();
  }

  public TSEND_SET_ID(final TPackReader sr) throws Exception{
    sn = sr.readFour();
    id = sr.readOne();
    propagate = sr.readBoolean();
    order = sr.readByteArray();
    //
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeFour(sn);
    pw.writeOne(id);
    pw.writeBoolean(propagate);
    pw.writeByteArray(order, true);
  }

  public String getText(){
    final StringBuilder sb = new StringBuilder("sn:");
    sb.append(sn);
    sb.append(" id:");
    sb.append(id);
    sb.append(" order [");
    for(int i = 0; i<order.length; i++){
      if(i>0){
        sb.append(",");
      }
      sb.append((int)order[i]);
    }
    sb.append("] propagate=");
    sb.append(propagate);
    return sb.toString();
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SEND_SET_ID;
  }

}
