/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import owl2_lib.TCoordinates;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class THUB_IDENTIFICATION extends TSEND {

  final public int id;
  final public int section;
  final public TCoordinates coo;
  final public boolean only1hub;

  public THUB_IDENTIFICATION(final int section_, final int id_, final TCoordinates coo_, final boolean only1hub_) throws Exception{
    id = id_;
    section = section_;
    coo = coo_;
    only1hub = only1hub_;
    //
    updateWriter();
  }

  public THUB_IDENTIFICATION(final TPackReader sr) throws Exception{
    section = sr.readTwo();
    id = sr.readOne();
    coo = sr.readCoordinates();
    only1hub = sr.hasMore(1) ? sr.readBoolean() : false;
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeTwo(section);
    pw.writeOne(id);
    pw.writeCoordinates(coo);
    pw.writeBoolean(only1hub);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.HUB_IDENTIFICATION;
  }

}
