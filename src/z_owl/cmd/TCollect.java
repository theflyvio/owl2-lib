/*
 *
 *
 *
 */
package z_owl.cmd;

import java.io.IOException;
import owl2_lib.owl_lib;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 * @author TheFlyvio
 */
public class TCollect implements IRTC {

  final private long data;
  private long value;
  //

  public TCollect(final long data_){
    data = data_; //new TData(data_);
  }

  @Override
  public int rtc_h(){
    value = data;
    extract(6); // rtc_m
    return extractInt(5);
  }

  @Override
  public int rtc_m(){
    value = data;
    return extractInt(6);
  }

  public int version(){
    value = data;
    extract(11); // rtc
    return extractVersion();
  }

  public int viz(){
    value = data;
    extract(11); // rtc
    return extractViz();
  }

  public float vbat(){
    value = data;
    extract(11); // rtc
    extractViz();
    return extractVBat();
  }

  public int irms(){
    value = data;
    extract(11); // rtc
    return extractIRMS();
  }

  public int vrms(){
    value = data;
    extract(11); // rtc
    return extractVRMS();
  }

  public int vrms_24(){
    value = data;
    return extractVRMS_24();
  }

  public int install_v(){
    value = data;
    extract(11); // rtc
    return extractInt(16);
  }

  public int fp(){
    value = data;
    extract(11); // rtc
    return extractFP();
  }

  public int temp(){
    value = data;
    extract(11); // rtc
    return extractTemp();
  }

  public float k(){
    value = data;
    return extractK();
  }

  public int cagRESULT(){
    value = data;
    extract(11); // rtc
    return (int)extract(8);
  }

  public boolean direction_i(){
    value = data;
    extract(11); // rtc
    return extract(1)==1;
  }

  public float i_min(){
    value = data;
    extract(11); // rtc
    return owl_lib.bits2i_min((int)extract(8));
  }

  public int ade_offset(){
    value = data;
    extract(11); // rtc
    return ((short)extract(16))<<8;
  }

  public boolean presence(){
    value = data;
    extract(11); // rtc
    return extract(1)==1;
  }

  public boolean only_hub1(){
    value = data;
    extract(11); // rtc
    return extract(1)==1;
  }

  public int curto_c(){
    value = data;
    extract(11); // rtc
    return extractInt(8);
  }

  public int direction_i_loops(){
    value = data;
    extract(11); // rtc
    return extractInt(8);
  }

  public int i_mult(){
    value = data;
    extract(11); // rtc
    return extractInt(8);
  }

  public int pot_b5(){
    value = data;
    return extractInt(8);
  }

  public boolean comparador_b5(){
    value = data;
    extract(8);
    return extract(1)==1;
  }

  public int rms_b5(){
    value = data;
    extract(8);
    extract(1);
    return extractInt(10);
  }

  public int pico_b5(){
    value = data;
    extract(8);
    extract(1);
    extract(10);
    return extractInt(10);
  }

  public int rms_b6(){
    value = data;
    return extractInt(15);
  }

  public int pico_b6(){
    value = data;
    extract(15);
    return extractInt(10);
  }

  public int v_check(){
    value = data;
    extract(11); // rtc
    return owl_lib.bits2v_check(extractInt(15));
  }

  public int ticks(){ // cada tick = 0,0095616752725186
    value = data;
    extract(11); // rtc
    return extractInt(15);
  }

  public boolean alarm(){
    value = data;
    extract(11); // rtc
    return extract(1)==1;
  }

  public boolean presence_v(int fwversion){
    value = data;
    extract(11); // rtc
    extract(7); // afund
    extract(1); // alarm
    if(fwversion>=96){
      extract(1); // i
    }
    return extract(1)==1; // v
  }

  public boolean presence_i(int fwversion){
    if(fwversion<96){
      return true;
    }
    value = data;
    extract(11); // rtc
    extract(7); // afund
    extract(1); // alarm
    return extract(1)==1; // i
  }

  public boolean presence_alarm(){
    value = data;
    extract(11); // rtc
    extract(7); // afund
    return extract(1)==1; // v
  }

  public int presence_afund_magnitude(){
    value = data;
    extract(11); // rtc
    return extractInt(7);
  }

//  public int presence_afund_h(){
//    value = data;
//    extract(6); // rtc_m
//    return extractInt(5);
//  }
//
//  public int presence_afund_m(){
//    value = data;
//    return extractInt(6);
//  }
  public long extract(int bits){
    int mask = (((int)1)<<bits)-1;
    long v = value&mask;
    value >>= bits;
    return v;
  }

  public int extractInt(int bits){
    return (int)extract(bits);
  }

  public int extractVersion(){
    return extractInt(16);
  }

  public float extractVBat(){
    return owl_lib.bits2vbat(extractInt(7));
  }

  public int extractViz(){
    return extractInt(4);
  }

  public int extractVRMS_24(){
    return extractInt(24);
  }

  public int extractIRMS(){
    return extractInt(19)<<5;
  }

  public int extractVRMS(){
    return extractInt(19)<<5;
  }

  public int extractFP(){
    return (short)extractInt(16);
  }

  public int extractTemp(){
    return owl_lib.bits2temp(extractInt(8));
  }

  public float extractK(){
    return owl_lib.bits2k(extractInt(24));
  }

  public void save(final TXStreamWriter w) throws IOException{
    w.writeInt(1);
    w.writeLong(data);
  }

  static public TCollect load(final TXStreamReader r) throws IOException{
    final int v = r.readInt();
    final long data = r.readLong();
    return new TCollect(data);
  }

}
