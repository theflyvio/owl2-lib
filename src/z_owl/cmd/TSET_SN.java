/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl.cmd;

import z_owl.EProtocol;
import z_owl.TPackReader;

/**
 *
 * @author TheFlyvio
 */
final public class TSET_SN extends TSEND {

  final public long sn; // 4

  public TSET_SN(final long sn_) throws Exception{
    super();
    sn = sn_;
    //
    updateWriter();
  }

  public TSET_SN(final TPackReader sr) throws Exception{
    sn = sr.readFour();
    //
    updateWriter();
  }

  @Override
  protected void updateWriterSpecific() throws Exception{
    pw.writeFour((int)sn);
  }

  @Override
  public EProtocol getProtocol(){
    return EProtocol.SET_SN;
  }

}
