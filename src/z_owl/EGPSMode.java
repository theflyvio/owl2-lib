/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

/**
 *
 * @author user
 */
public enum EGPSMode {
  OFF(0, "OFF"),
  SYNCING(1, "SYNCING"),
  STABILIZING(2, "STABILIZING"),
  WORKING(3, "WORKING"),
  TURN_ON(4, "TURN_ON"),
  TURN_OFF(5, "TURN_OFF"),
  NOT_FOUND(6, "NOT_FOUND"),
  DONE(7, "DONE"),
  NOT_WORKING(8, "NOT_WORKING");

  static final private EGPSMode[] _values = values();
  final private int _id;
  final private String _name;

  private EGPSMode(final int id, final String name){
    _id = id;
    _name = name;
  }

  public int getId(){
    return _id;
  }

  public String getName(){
    return _name;
  }

  static public EGPSMode getById(final int id){
    for(final EGPSMode p : _values){
      if(p._id==id){
        return p;
      }
    }
    return null;
  }

  static public EGPSMode getByName(final String name){
    for(final EGPSMode p : _values){
      if(p._name.equals(name)){
        return p;
      }
    }
    return null;
  }

}
