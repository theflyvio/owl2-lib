/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_owl;

import owl2_lib.TCoordinates;
import z_owl.cmd.TACK;
import z_owl.cmd.TALARM;
import z_owl.cmd.TDIRECTION_I;
import z_owl.cmd.TGET_COLLECT;
import z_owl.cmd.TGIGA_COMMAND;
import z_owl.cmd.THUB_CONFIG;
import z_owl.cmd.TSEND_RELEASE;
import z_owl.cmd.TSEND_SECTION;
import z_owl.cmd.THUB_IDENTIFICATION;
import z_owl.cmd.TINFO;
import z_owl.cmd.TINSTALL_INFO;
import z_owl.cmd.TLISTENER_INFO;
import z_owl.cmd.TLISTENER_NET_MESH;
import z_owl.cmd.TMEASURE;
import z_owl.cmd.TPACK_SENT;
import z_owl.cmd.TPACK_TRANSFER;
import z_owl.cmd.TPING;
import z_owl.cmd.TPRESENCE;
import z_owl.cmd.TPRESENCE_AFUND;
import z_owl.cmd.TREQ_ID;
import z_owl.cmd.TREQ_REQ_ID;
import z_owl.cmd.TSEND;
import z_owl.cmd.TSEND_COLLECT;
import z_owl.cmd.TSET_GPS;
import z_owl.cmd.TSEND_SET_ID;
import z_owl.cmd.TSET_MODE;
import z_owl.cmd.TSET_PRESENCE;
import z_owl.cmd.TSET_RTC;
import z_owl.cmd.TSET_SECTION;
import z_owl.cmd.TSET_SN;
import z_owl.cmd.TSTUCK;
import z_owl.cmd.TUPDATE_MISSING;
import x.etc.lib;
import x.etc.timer.TXTimer;

/**
 *
 * @author TheFlyvio
 */
abstract public class TPackHandler {

  final private TXTimer _timeout;
  final private TXTimer _timerinactive;
  final private TXTimer _timerAck;
  //
  final private byte _header[];
  private EReceiveState _state;
  private int _inactiveTime;
  private byte _data[];
  private int _pos;
  private TSEND _sending;
  private boolean _repeatWhenNoACK;

  public TPackHandler(){
    _repeatWhenNoACK = true;
    _header = new byte[6];
    //_header[0] = (byte)0xD9;
    //
    _timeout = new TXTimer(1000) {
      @Override
      public void run(){
        stop();
        reset();
      }
    };
    //
    _timerinactive = new TXTimer(1000) {
      @Override
      public void run(){
        callOnInactive(_inactiveTime++);
      }
    };
    //
    _timerAck = new TXTimer(10000) {
      @Override
      public void run(){
        if(_sending!=null&&_repeatWhenNoACK){
          resend(0);
        }
      }
    };
    _state = EReceiveState.NONE;
  }

  public EProtocol getSending(){
    return _sending==null ? EProtocol.NONE : _sending.getProtocol();
  }

  public void resend(int time){
    if(_sending!=null){
      final long ms = System.currentTimeMillis()+time;
      do{
        send(_sending.getByteArray());
      }while(System.currentTimeMillis()<ms);
    }
  }

  final public void setRepeatWhenNoACK(final boolean r){
    _repeatWhenNoACK = r;
  }

  protected void reset(){
    _state = EReceiveState.NONE;
  }

  protected void open(){
    _timerinactive.start();
  }

  protected void close(){
    reset();
    _timerinactive.stop();
  }

  private int si = 0;

  public void onByteAvailable(final byte b){
    _inactiveTime = 0;
    _timerinactive.stop();
    _timerinactive.start();
    _timeout.stop();
    _timeout.start();
    switch(_state){
      case NONE:
        if(b==(byte)0xD9||b==(byte)0xDA){
          _header[0] = b;
          _state = EReceiveState.HEADER;
          _pos = 1;
          si = 0;
          //System.err.println("start received");
        }else{
          if(si++%100==0){
            System.err.println("start ignored "+(b&0xFF)+" "+si);
          }
        }
        break;

      case HEADER:
        _header[_pos++] = b;
        if(_pos==6){
          if(lib.crc16(_header, 0, 4)==lib.arrayToShort(_header, 4, true)){
            int len = (_header[1]&0xFF)+(_header[0]==(byte)0xDA ? 256 : 0);
            _data = new byte[len];
            _pos = 0;
            _state = EReceiveState.DATA;
          }else{
            System.err.println("erro header");
            _state = EReceiveState.NONE;
          }
        }
        break;

      case DATA:
        _data[_pos++] = b;
        if(_pos==_data.length){
          _state = EReceiveState.NONE;
          int calc = lib.crc16(_data, 0, _data.length);
          int recb = lib.arrayToShort(_header, 2, true);
          if(calc==recb){
            processData();
          }else{
            System.err.println("erro data "+calc+"<>"+recb);
          }
        }
        break;
    }
  }

  public TSEND loadSEND(final byte[] _data){
    final TPackReader pr = new TPackReader(_data);
    // pula camada de transporte
    pr.readTwo();
    pr.readFour();
    final EProtocol cmd = EProtocol.getById(pr.readOne());
    try{
      switch(cmd){
        case HUB_IDENTIFICATION:
          return new THUB_IDENTIFICATION(pr);

        case INFO:
          return (new TINFO(pr));

        case REQ_ID:
          return (new TREQ_ID(pr));

        case SEND_SET_ID: // somente send fw
          return (new TSEND_SET_ID(pr));

        case DIRECTION_I: //
          return (new TDIRECTION_I(pr));

        case PRESENCE:
          return (new TPRESENCE(pr));

        case PRESENCE_AFUND:
          return (new TPRESENCE_AFUND(pr));

        case ALARM_OLD:
          return (new TALARM(pr));

        case STUCK:
          return new TSTUCK(pr);

        case ACK:
          return new TACK(pr);

        case SEND_COLLECT:
          return new TSEND_COLLECT(pr);

        //case GET_COLLECT:
        //  return new TGET_COLLECT(pr);

        case PACK_TRANSFER:
          return new TPACK_TRANSFER(pr);

        case PING:
          return new TPING(pr);

        case UPDATE_MISSING:
          return new TUPDATE_MISSING(pr);

        case PACK_SENT:
          return new TPACK_SENT(pr);

        case LISTENER_INFO:
          return new TLISTENER_INFO(pr);

        case LISTENER_NET_MESH:
          return new TLISTENER_NET_MESH(pr);

        case SEND_RELEASE: // somente envio INSTALLER
        case SEND_SECTION: // somente envio INSTALLER
        case SET_GPS: // somente send fw
        case SET_MODE: // somente send fw
        case SET_PRESENCE: // somente send fw
        case SET_RTC: // somente send fw
        case SET_SECTION: // somente send fw
        case HUB_CONFIG:
        case NONE:
        case REQ_REQ_ID:
        case GET_NEIGHBOURS:
        case INTERFACE_UPDATE_FW:
        case MEASURE_K:
        case SET_SN:
        case GIGA_COMMAND:
        case INSTALL_INFO:
        case MEASURE:
          callOnException("TPackHandler.loadSEND", new Exception("invalid cmd "+cmd.getName()));
          break;
      }
    }catch(final Exception ex){
      callOnException("TPackHandler.loadSEND", ex);
    }
    return null;
  }

  private void processData(){
    final TPackReader pr = new TPackReader(_data);
    final EProtocol cmd = EProtocol.getById(pr.readOne());
    try{
      switch(cmd){
        case HUB_IDENTIFICATION:
          callOn_HUB_IDENTIFICATION(new THUB_IDENTIFICATION(pr));
          break;

        case INFO:
          callOn_INFO(new TINFO(pr));
          break;

        case LISTENER_INFO:
          callOn_LISTENER_INFO(new TLISTENER_INFO(pr));
          break;

        case LISTENER_NET_MESH:
          callOn_LISTENER_NET_MESH(new TLISTENER_NET_MESH(pr));
          break;

        case MEASURE:
          callOn_MEASURE(new TMEASURE(pr));
          break;

        case INSTALL_INFO:
          callOn_INSTALL_INFO(new TINSTALL_INFO(pr));
          break;

        case REQ_ID:
          callOn_REQ_ID(new TREQ_ID(pr));
          break;

        case SEND_SET_ID: // somente send fw
          callOn_SEND_SET_ID(new TSEND_SET_ID(pr));
          break;

        case DIRECTION_I:
          callOn_DIRECTION_I(new TDIRECTION_I(pr));
          break;

        case PRESENCE:
          callOn_PRESENCE(new TPRESENCE(pr));
          break;

        case PRESENCE_AFUND:
          callOn_PRESENCE_AFUND(new TPRESENCE_AFUND(pr));
          break;

        case ALARM_OLD:
          callOn_ALARM(new TALARM(pr));
          break;

        case STUCK:
          callOn_STUCK(new TSTUCK(pr));
          break;

        case ACK:
          callOn_ACK(new TACK(pr));
          break;

        case SEND_COLLECT:
          callOn_SEND_COLLECT(new TSEND_COLLECT(pr));
          break;

        //case GET_COLLECT:
        //  callOn_GET_COLLECT(new TGET_COLLECT(pr));
        //  break;

        case PACK_TRANSFER:
          callOn_PACK_TRANSFER(new TPACK_TRANSFER(pr));
          break;

        case PING:
          callOn_PING(new TPING(pr));
          break;

        case PACK_SENT:
          callOn_PACK_SENT(new TPACK_SENT(pr));
          break;

        case UPDATE_MISSING:
          callOn_UPDATE_MISSING(new TUPDATE_MISSING(pr));
          break;

        case SEND_RELEASE: // somente envio INSTALLER
        case SEND_SECTION: // somente envio INSTALLER
        case SET_GPS: // somente send fw
        case SET_MODE: // somente send fw
        case SET_PRESENCE: // somente send fw
        case SET_RTC: // somente send fw
        case SET_SECTION: // somente send fw
        case HUB_CONFIG:
        case NONE:
        case REQ_REQ_ID:
        case GET_NEIGHBOURS:
        case INTERFACE_UPDATE_FW:
        case MEASURE_K:
        case SET_SN:
          callOnException("TPackHandler.processData", new Exception("invalid cmd "+cmd.getName()));
          break;
      }
      if(cmd.hasACK){
        send(new TACK(cmd).getByteArray());
      }
    }catch(final Exception ex){
      callOnException("TPackHandler.processData", ex);
    }
  }

  private void callOn_INFO(final TINFO info){
    on_INFO(info);
  }

  private void callOn_LISTENER_INFO(final TLISTENER_INFO info){
    on_LISTENER_INFO(info);
  }

  private void callOn_LISTENER_NET_MESH(final TLISTENER_NET_MESH nm){
    on_LISTENER_NET_MESH(nm);
  }

  private void callOn_MEASURE(final TMEASURE m){
    on_MEASURE(m);
  }

  private void callOn_INSTALL_INFO(final TINSTALL_INFO info){
    on_INSTALL_INFO(info);
  }

  private void callOn_REQ_ID(final TREQ_ID reqid){
    on_REQ_ID(reqid);
  }

  private void callOn_DIRECTION_I(final TDIRECTION_I p){
    on_DIRECTION_I(p);
  }

  private void callOn_PRESENCE(final TPRESENCE p){
    on_PRESENCE(p);
  }

  private void callOn_PRESENCE_AFUND(final TPRESENCE_AFUND p){
    on_PRESENCE_AFUND(p);
  }

  private void callOn_ALARM(final TALARM a){
    on_ALARM(a);
  }

  private void callOn_STUCK(final TSTUCK stuck){
    on_STUCK(stuck);
  }

  private void callOn_ACK(final TACK ack){
    if(ack.cmd==EProtocol.NONE){
      on_ACK(ack);
    }else if(_sending!=null&&ack.cmd==_sending.getProtocol()){
      System.out.println("ACK "+_sending.getProtocol().toString());
      _sending = null;
      _timerAck.stop();
      on_ACK(ack);
    }
  }

  private void callOn_SEND_COLLECT(final TSEND_COLLECT collect){
    on_SEND_COLLECT(collect);
  }

  private void callOn_GET_COLLECT(final TGET_COLLECT collect){
    on_GET_COLLECT(collect);
  }

  private void callOn_PACK_TRANSFER(final TPACK_TRANSFER p){
    on_PACK_TRANSFER(p);
  }

  private void callOn_PING(final TPING ping){
    on_PING(ping);
  }

  private void callOn_PACK_SENT(final TPACK_SENT psent){
    on_PACK_SENT(psent);
  }

  private void callOn_SEND_SET_ID(final TSEND_SET_ID setid){
    on_SEND_SET_ID(setid);
  }

  private void callOn_HUB_IDENTIFICATION(final THUB_IDENTIFICATION hid){
    on_HUB_IDENTIFICATION(hid);
  }

  private void callOn_UPDATE_MISSING(final TUPDATE_MISSING pupm){
    on_UPDATE_MISSING(pupm);
  }

  private void callOnInactive(final int seconds){
    onInactive(seconds);
  }

  protected void callOnException(final String msg, final Exception ex){
    try{
      onException(msg, ex);
    }catch(final Exception ex2){
      lib.unhandledException(ex2);
    }
  }

  public void send_GIGA_COMMAND(final int sn, final EGigaCommand cmd, final int[] p_, final boolean wait_ack) throws Exception{
    final int p[] = new int[]{0, 0, 0, 0, 0};
    if(p_!=null){
      System.arraycopy(p_, 0, p, 0, Math.min(p.length, p_.length));
    }
    send(new TGIGA_COMMAND(sn, cmd, p));
    if(!wait_ack){
      on_ACK(new TACK(EProtocol.GIGA_COMMAND));
    }
  }

  public void send_HUB_CONFIG(int section, int id) throws Exception{
    send(new THUB_CONFIG(section, id));
  }

  public void send_HUB_IDENTIFICATION(final int section, final int id, final TCoordinates coo, final boolean only1hub) throws Exception{
    send(new THUB_IDENTIFICATION(section, id, coo, only1hub));
  }

  public void send_REQ_REQ_ID() throws Exception{
    send(new TREQ_REQ_ID());
  }

  public void send_SEND_RELEASE(final long sn, final int cmd, final long ullat, final long ullon, final int v, final int utc_hour, final int utc_min, final int utc_sec) throws Exception{
    send(new TSEND_RELEASE(sn, cmd, ullat, ullon, v, utc_hour, utc_min, utc_sec));
  }

  public void send_SEND_SECTION(final long sn, final int section) throws Exception{
    send(new TSEND_SECTION(sn, section));
  }

  public void send_SEND_SET_ID(final int sn, final int id, final byte[] order, final boolean propagate) throws Exception{
    send(new TSEND_SET_ID(sn, id, order, propagate));
  }

  public void send_SET_GPS(final TCoordinates coo) throws Exception{
    send(new TSET_GPS(coo));
  }

  public void send_SET_MODE(final int mode) throws Exception{
    send(new TSET_MODE(mode));
  }

  public void send_SET_PRESENCE(final boolean presence) throws Exception{
    send(new TSET_PRESENCE(presence));
  }

  public void send_SET_SN(final long sn) throws Exception{
    send(new TSET_SN(sn));
  }

  public void send_SET_RTC(final int h, final int m, final int s) throws Exception{
    send(new TSET_RTC(h, m, s));
  }

  public void send_SET_SECTION(final int section) throws Exception{
    send(new TSET_SECTION(section));
  }

  public void send(final TSEND p){
    if(p.getProtocol()!=EProtocol.ACK&&p.getProtocol()!=EProtocol.PING){
      _sending = p;
      _timerAck.start();
    }
    send(p.getByteArray());
  }

  public void cancelSend(){
    _sending = null;
    _timerAck.stop();
  }

  public boolean isSending(){
    return _sending!=null;
  }

  protected void on_PING(final TPING ping){
  }

  abstract protected void send(final byte[] data);

  abstract protected void on_INFO(final TINFO info);

  abstract protected void on_LISTENER_INFO(final TLISTENER_INFO info);

  abstract protected void on_LISTENER_NET_MESH(final TLISTENER_NET_MESH nm);

  abstract protected void on_MEASURE(final TMEASURE measure);

  abstract protected void on_INSTALL_INFO(final TINSTALL_INFO info);

  abstract protected void on_REQ_ID(final TREQ_ID reqid);

  abstract protected void on_DIRECTION_I(final TDIRECTION_I direction_i);

  abstract protected void on_PRESENCE(final TPRESENCE presence);

  abstract protected void on_PRESENCE_AFUND(final TPRESENCE_AFUND p);

  abstract protected void on_ALARM(final TALARM alarm);

  abstract protected void on_STUCK(final TSTUCK stuck);

  abstract protected void on_ACK(final TACK ack);

  abstract protected void on_SEND_COLLECT(final TSEND_COLLECT collect);

  abstract protected void on_GET_COLLECT(final TGET_COLLECT collect);

  abstract protected void on_PACK_TRANSFER(final TPACK_TRANSFER p);

  abstract protected void on_SEND_SET_ID(final TSEND_SET_ID setid);

  abstract protected void on_HUB_IDENTIFICATION(final THUB_IDENTIFICATION hid);

  abstract protected void on_PACK_SENT(final TPACK_SENT psent);

  abstract protected void on_UPDATE_MISSING(final TUPDATE_MISSING pupm);

  abstract protected void onInactive(final int seconds);

  abstract protected void onException(final String msg, final Exception ex);

  static private enum EReceiveState {
    NONE,
    HEADER,
    DATA
  }
}
